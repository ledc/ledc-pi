If you want to make a contribution, here is how you can help:


## Adding properties

The more components and properties that are described in a JSON file, the more powerful ledc-pi becomes.
Do you have a property for which you have defined a measure, convertor or scanner?
Then it would be nice to add to the registries folder.
When doing so, keep a few guidelines in mind:

- When defining measures, try to be complete in the tests you add.
That is, inform yourself about how the property needs to be formatted or which constraints it should meet.
Keep in mind that tests are always done in the order of specification, so think well about this order.

- Choose the URI to describe the property well. It must be unique and sufficiently informative.

- Always add a *source URL* about where you found information about a property.
This makes it easier for others to verify the structure and implementation of predicates.
This is especially important with complicated properties like ISBN numbers.

## Extending the operators

The definition of predicates and convertors strongly hinges on grexes, which come with a wide range of operators.
If you identify a certain operator that is missing and you think this operator can be useful for multiple properties, then you can think of adding the operator
as a native one.
If you want to do that, have a look at the PostfixParser and use the dedicated methods to add native operators.
Don't forget to add new native operators to the Wiki and provide a short description of what they do.


