# Introduction

The purpose of ledc-pi is to provide a generic, simple and extendible system for validation of Persistent Identifiers (PIs).
To do so, PIs are identified by means of a URIs (Uniform Resource Identifiers) in order to standardize their naming.
Each URI is composed of a *domain*, a *type*, a *name* and a *locale*.
The domain and type can be seen as a namespace for the property, in which the name of the property is unique.
The locale identifier can be used to indicate for example a specific language or country for which a property is relevant.

*Example* The URI `ledc.ugent.be/identifier#ssn:BE` identifies the Belgian Social Security Number (SSN). 
In this URI, the domain is "ledc.ugent.be" and the type is "identifier".
The name of the property is "ssn" and the locale indicates it is applicable to Belgium (BE).

If there is no specific locale, the wildcard `*` can be used.
We note that URIs are used to standardize property naming and to provide information about the context in which properties apply, but we do *not* seek to enforce that these URIs are resolvable.
If one wish to do so, then it is advisable that URIs resolve to a [JSON file](#portability) containing a description of the components for the property.

## Overview
The power and expressiveness of ledc-pi lies in the fact all components needed for validation are defined in terms of a simple addition to regular expressions which we call group expressions or [grexes](grexes.md).
It is advisable to first understand what these expression are.
The process of validation hereby relies on four central components
- A [quality measure](#quality-measures) evaluates the quality of a PI instance on an ordinal scale. It comes with a quality *threshold* that indicates the level at which instances are regarded as valid.
- A [convertor](#convertors) basically takes valid instances and transforms them into a *prefered* instance. This is one for which the quality is maximal. It follows that a convertor is always attached to a quality measure. The combination of a quality measure and a convertor is called a *validation scheme*.
- A [scanners](#scanners) finds all valid instances of a PI in a text.
- A [transformator](#transformation) transforms valid instances of one property into valid instances of another property. We note that this is of course limited to certain combinations of PIs only.

All components atteached to a PI can be collected in a *registry* and this registry can be persisted in a simple JSON file(#portability).
If you want to skip all this, you can go straight [getting started](#getting-started).

![architecture.svg](uploads/032cd4584d85c76fc6d5d021875015f4/architecture.svg)

## Quality measures

A quality measure for a PI assesses the quality of instances (i.e., string literals) on an ordinal scale.
Each quality measure consists of a list of *predicates*.
In order to compute the quality of an instance, the predicates of a quality measure are applied in *the order in which they are given*.
If a predicate succeeds, the next predicate is applied.
If a predicate fails, the measurement procedure stops and the index of the failing predicate is returned.
This means that the level of quality is always a number between 0 and the number of predicates that quality measure has.

*Example* For property <tt>ledc.ugent.be/identifier#ssn:BE</tt>, we can use the following five predicates:

- Check if a given value is a NULL value or not.
- Check the the basic pattern of a Belgian SSN (11 digits with possible separators).
- Check if the first six digits define a valid date.
- Check if the control digit is correct.
- Check if all separators are present and in the correct position.

All these checks can be defined as *grex predicates* which are basically a [grexes](grexes.md) that resolves to a Boolean (true or false).
There are two exceptions where we divert from this:

- An *instance predicate* is a special type of grex predicate where a grex expression is used to extract a value from a piece of data and test
if that value is a valid instance of another PI.
We treat this case separately for the sake of being able to easy detect validation dependencies between PIs.

- A predicate can also be Boolean combinations of other predicates.

Each quality measure comes with a *validity threshold*.
This is a level of quality at which instances can be considered valid.

## Convertors

A convertor takes a *valid* instance of a PI and converts it into the preferred instance (i.e., the one with maximal quality).
Currently, two types of convertors are available:

- A **grex conversion** uses a simple [grex](grexes.md) to define the conversion.

- A **conditional conversion** is a generalization of grex conversion, where multiple grex conversions are defined.
Each of them is combined with a grex predicate to identify is application condition.
During conversion, each test will be verified in order of declaration.
The convertor of the *first* test that succeeds is selected to perform the conversion.

*Example* In the case of Belgian SSNs, if we use our prior system of five predicates and choose the validity threshold as 4, then the the regex `(\d{2})\.?(\d{2})\.?(\d{2})\-?(\d{3})\.?(\d{2})`
and the grex formula `@1.@2.@3-@4.@5` together define a grex conversion for valid SSN instances.

We note here that since conversion can only be done for valid instances, a convertor is only useful in the context of a quality measure.
Therefore, we always consider a combination of (i) a quality measure, (ii) a validity threshold and (iii) a convertor together.
Such a combination is called a **validation scheme**.


## Scanners

In order to try to recognize valid instances of PIs in *free text*, scanners can be used.
Given a piece of text, a scanner finds all valid instances of a certain PI.
To support this, we can use a **measure scanner** that uses a regex pattern to search candidate instances.
Each candidate instance found by the regex pattern will then be validated with an available quality measure for the target property.
Only those instances that are valid in terms of this quality measure, are preserved.
In the case there is no quality measure for the target property, all instances found by the regex will be returned as a possible result.
In other words, if no quality measure is available for the target property, the pattern scanner reduces to a simple regex search in the given text.

## Transformation

In some cases, it can be useful or necessary to transform valid instances of some source PI to some target PI.
This can be done by using a transformation.
Transformations are very similar to conversions in the sense that they internally rely on grex conversions or conditional conversions to compute instances.
However, there are some differences.
First of all, a transformation needs two PIs: an input PI and an output PI.
Second, a transformation always assumes that instances at the input side are of the highest quality.
This is to avoid that conversions are repeated in the definition of a transformation.
Third, if there is a transformation from PI X to Y and there is one from Y to Z, then it is not necesaary to explicity define transformation from X to Z.
That latter transformation will be constructed as needed by composition of the first two transformations.
For that purpose, transformations are stored in a *transformation graph*.
If there exists a path between two PIs, the transformation steps along this path will be combined transitively.

## Getting started

To use ledc-pi, clone the code from the repository and build it with Maven.
When that is done, there basically two ways to get started.

The first option is to import one or more existing JSON files.
The registry is then populated with all components described in the files and you can use them immediately.
Example JSON files can be found at an instance of a [ledc-server](https://ledc.ugent.be:8828).

The second option is to compose the necessary components directly in Java.
You can then persist those components in a JSON file and share it with others.
The following example illustrates how to build a validation scheme and a scanner for Belgian SSNs.


```java
public static void buildSsn() throws URISyntaxException, PropertyParseException, IOException, PiException
{
    List<Predicate> predicates = new ArrayList<>();
    
    String regex1 = "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})";
    String regex2 = "(\\d{2})\\.(\\d{2})\\.(\\d{2})\\-(\\d{3})\\.(\\d{2})";
    
    predicates.add(new NotNullPredicate<>());
    predicates.add(new PatternPredicate(regex1, "Basic pattern mismatch"));
    predicates.add(new GrexPredicate(
        new Grex("::int 97 @1@2@3@4 97 % - @5 = 97 2@1@2@3@4 97 % - @5 = ::bool or"),
        regex1,
        "Invalid checksum"
    ));
    predicates.add(new GrexPredicate(
        new Grex("@1 / || @2 || / || @3 || yy/MM/dd isdatetime"),
        regex1,
        "First six digits do not correspond to a date with pattern yy.MM.dd"
    ));
    predicates.add(new PatternPredicate(regex2, "Separators are missing or not positioned correctly"));
    
    //Build the measure composed of the predicates
    Measure measure = new Measure(predicates);
    
    //Build a convertor
    Convertor convertor = new GrexStringConvertor(new Grex("@1.@2.@3-@4.@5"), regex1);
    
    //Register a new validation schema
    Registry
        .getInstance()
        .register(
            new ValidationScheme(
                Property.parseProperty("ledc.ugent.be/identifier#ssn:BE"),
                measure,
                convertor,
                new URI("https://www.ibz.rrn.fgov.be/"), //Source on which this validation scheme is based
                4 // Instaces are valid if the level is 4 or higher
            ));

    //Register a scanner
    Registry
        .getInstance()
        .register(new MeasureScanner(Properties.SSN_BE.getProperty(), regex1));
    
    //Dump
    Registry
        .getInstance()
        .dump(new File("ssn.json"));
} 
```
## Portability

Components defined for a given PI can be exported to a JSON file.
This JSON file has a root object that contains four arrays as shown below.
The operator array is used to support extensibility of grex operator tables (see below).

```json
{
    "ValidationSchemes":[],
    "Scanners":[],
    "Transformators": [],
    "Operators": []
}
```

The example code provide above to define components for Belgian SSNs will produce the following JSON file.

```json
{
    "Operators": [],
    "Scanners": [{
        "Property": "ledc.ugent.be/identifier#ssn:BE",
        "Pattern": "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})",
        "Type": "Measure"
    }],
    "Transformators": [],
    "ValidationSchemes": [{
        "Measure": {"Predicates": [
            {"Type": "NotNull"},
            {
                "Pattern": "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})",
                "Type": "Pattern",
                "Explanation": "Basic pattern mismatch"
            },
            {
                "Pattern": "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})",
                "Type": "Grex",
                "Explanation": "Invalid checksum",
                "Grex": "::int 97 @1@2@3@4 97 % - @5 = 97 2@1@2@3@4 97 % - @5 = ::bool or"
            },
            {
                "Pattern": "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})",
                "Type": "Grex",
                "Explanation": "First six digits do not correspond to a date with pattern yy.MM.dd",
                "Grex": "@1 / || @2 || / || @3 || yy/MM/dd isdatetime"
            },
            {
                "Pattern": "(\\d{2})\\.(\\d{2})\\.(\\d{2})\\-(\\d{3})\\.(\\d{2})",
                "Type": "Pattern",
                "Explanation": "Separators are missing or not positioned correctly"
            }
        ]},
        "SufficiencyThreshold": 4,
        "Property": "ledc.ugent.be/identifier#ssn:BE",
        "Source": "https://www.ibz.rrn.fgov.be/",
        "Convertor": {
            "Pattern": "(\\d{2})\\.?(\\d{2})\\.?(\\d{2})\\-?(\\d{3})\\.?(\\d{2})",
            "Type": "GrexString",
            "Grex": "@1.@2.@3-@4.@5"
        }
    }]
}
```
To support *extensibility* of grex operator tables, the following types of operators are persistable and can be customized:

- **Character mappings** Character mappings are binary scope operators in the string scope.
With this operator, you specify a string to which the operator is applied (first argument) and a mapping of characters (second argument).
The operator will replace characters from the given string according to the given map.
The mapping maps specific characters to *strings* rather than other characters.
Note that the behavior of character mappings is entirely determined by the fact that each character is visited in a sequential order, from start to end, and that the mapping is applied on each character independently.
The default [Caesar codes](grexes.md#the-string-scope) are examples of character mappings.
To add a character mapping, you must specify a "Type", "Symbol" and "Map".
The "Type" must be given the value `CharMap` and the "Symbol" should hold the name of your operator in accordance to the [naming conventions](grexes.md#operators-and-tests).
The "Symbol" value can then be used in any grex defined in the same JSON config file.
The value for "Map" will hold the actual mapping of characters formatted as a string.
The string should follow the format `<char-in 1>=<string-out 1>,...,<char-in k >=<string-out k>`

- **Integer permutations** As they are heavily used in check digit schemes, permutations of the 10 decimal digits can be customized as well.
The operators `luhn` and `verhoeff` are examples of such permutations.
To add an integer permutation, you must specify a "Type", "Symbol" and "Cycles".
The "Type" must be given the value `IntPerm` and the "Symbol" should hold the name of your operator in accordance to the [naming conventions](grexes.md#operators-and-tests).
The "Cycles" should specify the actual permutation in cycle notation as is exemplified [here](grexes.md#the-integer-scope).
For permutations, adding a `*` at the end of the name converts the operator into a binary operator where the second argument is the power of the permutation.

- **String mappings** String mappings are very similar to character mappings, except that now we will map strings to other strings, rather than characters to strings.
The required parts are again "Type", "Symbol" and "Map", but the "Type" should now be equal to string `StringMap`.
The formatting of the `Map` is the same as with character mappings, but the keys can now be full strings rather than single characters.
Important to note is that, in order to make the behavior deterministic, the mapping will be applied in the order in which string maps are specified in the `Map' variable.
More precisely, when applied, the operator will consider all string mappings in the order in which they are given in the "Map" string.
Each time, it will locate all places where it can find the key string and replace that with the value string.

## Assertions

A **property assertion** is a statement that links an attribute from a dataset to a property.
Each such statement is a triple consisting of an attribute name, an operator and a property name.
- The **attribute name** is an attribute that appears in the targeted dataset.

- The **property name** is a standardized name (i.e., a URI) for which validation components can be defined.

- The **operator** describes how values for the attribute in the targeted dataset should relate to the property.
Currently, there are three operators available as summarized in the table below.


| Operator name | Meaning                                                     | Validation                                                                               | Repair                                                                                                                        |
|---------------|:-------------------------------------------------------------:|:------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------------------------------------------------:|
| `instanceof`  | A value must be **equal to** a valid instance of the property | A [quality measure](#quality-measures) is used to verify if values have sufficient quality | Values are [converted](#convertors) to a standardized format, if possible |
| `contains single` | A value must **contain exactly one** valid instance of the property | A [scanner](#canners) is used to find instances of the property in the attribute and check if there is exactly one valid instance | A single instance will be extracted and then, if possible, standardized by use of a [convertor](#convertors)|
| `contains multiple`| A value must **contain at least one** valid instance of the property | A [scanner](#scanners) is used to find instances of the property | All valid instances will be extracted and then, if possible, standardized by use of a [convertor](#convertors) |


Consider now a snippet of data taken from [clinicaltrials.gov](http://clinicaltrials.gov) that connects clinical trials to secondary identifiers of that trial.

```csv
"nct_id","secondary_id"
"NCT00043134","SUPERGEN-EORTC-06011"
"NCT00043134","GMDSG-EORTC-06011"
"NCT00043134","EudraCT-2005-002830"
```
We can now provide the following statements.

```txt
secondary_id contains single ledc.ugent.be/identifier#eudract:*
secondary_id contains single ledc.ugent.be/identifier#utn:*
nct_id instanceof ledc.ugent.be/identifier#nct:*
```
This expresses that the attribute `nct_id` must be a valid instance of property `ledc.ugent.be/identifier#nct:*`, while secondary_id should contain valid instances of `ledc.ugent.be/identifier#eudract:*` or `ledc.ugent.be/identifier#utn:*`.
