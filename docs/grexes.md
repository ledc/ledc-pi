# Group expressions

The components used in ledc-pi strongly build on a new concept called *group expressions* or *grexes* for short.
In this part, we explain their definition, usages and possibilities.

## The basic idea
The current modalities to check the quality of properties rely heavily on some form of pattern matching.
In most cases, the underlying mechanism is that of *regular expressions* or *regexes*.
If you are not familiar with regexes, it is advisable to first catch up with that.

Simply put, regexes are patterns that allow to check if some string of characters from some alphabet is accepted by some language.
The main issue with regexes is however that they do not offer much semantics on the sequences of characters they check.
To solve this problem, ledc-pi uses a simple extension to regexes by making use of *grexes*.
In a regex $`\Sigma`$, it is possible to "capture" several groups by means of round brackets.
When a character string `s` matches the pattern $`\Sigma`$, then all groups in $`\Sigma`$ are aligned with the corresponding substrings in `s`.

*Example* Belgian SSN numbers consist of 11 digits with possible separators.
A regex that checks this pattern is the following:

`(\d{2})\.?(\d{2})\.?(\d{2})\-?(\d{3})\.?(\d{2})`

The string `s=11.22.55-286.36` matches the pattern and we denote this by $`\Sigma\models s`$.
The pattern $`\Sigma`$ uses round brackets several times to create groups.
Groups are indexed in order of opening brackets and this index can be used to dereference groups after matching.
By default, even if no groups are explicitly defined, the entire string corresponds to the default group with index `0`.
In our example, after the match, the first group of $`\Sigma`$ matches the string `11`, the second group matches `22` and so on.
It is precisely this feature on which grexes are built.
A grex `G` is always defined in the scope of some regex $`\Sigma`$ in which groups are defined.
In simple terms, a grex `G` can be seen as a formula or an expression in which variables refer to captured groups of $`\Sigma`$.

## Tokens and token classes
Assume now we have a regex $`\Sigma`$.
A grex `G` is then defined as a sequence of tokens where tokens are separated by the whitespace character.
A token always belongs to one of three classes: it is either a *value* token, an *operator* token or an *operator scope* token.

- **Value tokens** Value tokens represent literal values that can be used as arguments for operators.
In the simplest case, we just provide some constant value like `Hello` pr `3`.
Besides that, we can decide to add variables.
A variable always starts with an `@` symbol followed by an integer number.
The integer number is used as a reference to one of the groups defined in the regex.
Variables will *always* be literally substituted by their corresponding value.
When multiple variables are defined in a single value token (e.g. `@1@2`) then both variables will be replaced by their value and these values will be concatenated.
When a token is surrounded by double quotes (i.e., "), tokens are always treated as values.

- **Operator tokens** An operator token indicates the usage of some operator that takes a fixed number of value tokens as arguments and returns a single value as a result.
Each operator has a symbol with which it can be called.
Operator symbols cannot be empty, they cannot start or end with a double quote character, they cannot contain whitespace characters and they must be unique within their scope.
The data types of inputs and outputs of operators are bounded by the operator scope (see below). 
Hereby, we make a distinction between a scope operator and a test:
  * A *scope operator* takes a predetermined number of arguments and returns a value of which the type is determined by the operator scope.
It is not necessary that all arguments are of the type determined by the scope but the output is always bounded.
 
  * A *test* always takes arguments of the type determined by the operator scope and returns a Boolean value.

- **Operator scope tokens** Operator scope tokens are used to change the scope of the operators.
Currently, there are five scope tokens allowed: 
* `::string`: the string scope (the default)
* `::int`: the integer scope
* `::bigint`: the big integer scope
* `::double`: the double scope
* `::bool`: the Boolean scope
* `::datetime`: the datetime scope

The scope of the operators has two primary functions: (i) it enforces data types of inputs and outputs and (ii) it sets the scope in which operator names must be unique.

*Note: there is an exception to the rule of fixed arguments.
If the number of arguments of an operator equals `-1`, the operator takes **all** arguments currently on the stack as input.*

## Postfix notation and parsing

To evaluate a grex `G` on a string `s` in the context of a regex $`\Sigma`$, the following steps are taken.

- **Step 1** It is verified whether or not `s` matches the regex pattern $`\Sigma`$.
If it does, then all capture groups present in $`\Sigma`$ with their corresponding substrings in `s` are put in map and we proceed to step 2.
If not, then the evaluation stops.
Within a grex predicate, failing to match $`\Sigma`$ implies that the predicate evaluates to false for input s.

- **Step 2** The stream of tokens present in `G` is now parsed and processed by a postfix parser.
Postfix notation implies that the arguments of an operator are written *before* the operator itself (e.g., `3 5 +`).
Postfix notation offers some important advantages.
It does not require use of precedence rules and hence brackets are not necessary.
Moreover, the parsing of an expression is extremely simple.
It sequentially visits all tokens in the stream and for each token it will apply the algorithm visualized in the diagram below.
In particular, when a variable is observed, the mapping constructed in step 1 is used to replace variables with constants.
A variable starts with the symbol `@` and is followed by an integer number `n`.
That integer refers to the $`n^{th}`$ capture group of the regex.
Note that a single token can contain a **sequence** of variables.
In that case, the result is the concatenation of the variable values.
During parsing, a stack is used to store *value* tokens that either have been observed in the previous or have been computed by an operator.

```mermaid
graph LR
  A((Token))-->B[Operator Scope?];
  B--Y-->C(Change Scope);
  B--N-->D[Variables?];
  D--Y-->E(Dereference and push to stack);
  D--N-->G[Operator?];
  G--Y-->H[Branching?];
  G--N-->I[Push token to stack];
  H--Y-->J[Start branch mode];
  H--N-->K[Pull args from stack, execute and push result];

style B fill:#ffffff, stroke:#2bbde9
style D fill:#ffffff, stroke:#2bbde9
style G fill:#ffffff, stroke:#2bbde9
style H fill:#ffffff, stroke:#2bbde9
```

If a token is an operator scope token, the parser simply switches to the scope indicated by the token.
If a token is a variable or sequence of variables, those variables are dereferenced and the resulting token is pushed to the stack.
If the token is an operator token, there are two options.
If the operator is a branching operator, it will start branching mode.
This is a kind of terminal mode that is further explained in [branching operators](grexes.md#branching-operators).
If the operator is a regular operator, the parser will inspect how many arguments that operator takes.
It will then pop the necessary number of arguments from the stack, apply the operator and push the result back to the stack.
Finally, if the token is a *value* token, it is pushed to the stack.

*Example* For Belgian SSN numbers, suppose we use the regex pattern $`\Sigma`$ equal to

`(\d{2\})\.?(\d{2})\.?(\d{2})\-?(\d{3})\.?(\d{2})`

and now define a grex predicate that verifies the check digit.
For people born before the year 2000, this can be done by defining a grex predicate that uses the pattern $`\Sigma`$ and the following simple grex

`::int 97 @1@2@3@4 97 % - @5 =`

Let's see now how this works for the string `s = 11.22.55-286.36`.

First, `s` matches $`\Sigma`$ so we continue with the actual parsing.
The sequence of tokens is then sequentially processed as follows:
  - **Observe** `::int` This token is an operator scope token and sets the scope to the integer scope.
In particular, that means all operators that follow, are restricted to integer operators only.

  - **Observe** `97` This token is a value token and is pushed to the stack, which now has state `S = [97]`.

  - **Observe** `@1@2@3@4` This token is a sequence of variables. Each variable is then replaced with the corresponding content of the capture group. That means `@1` is replaced by the contents of group 1 and thus by the string `11`, `@2` is replaced by the contents of group 2 and thus by the string `22` and so on. All tokens are then concatenated and the result is pushed to the stack, which now has state `S = [97, 112255286]`.

  - **Observe** `97` This token is a value token and is pushed to the stack, which now has state `S = [97, 112255286, 97]`.

  - **Observe** `%` This token is an operator token that represent the modulo operation (see [Operators](#operators) for an overview).
It has two arguments, so the parser pops two arguments from the stack and applies the operator to it.
This results in the computation of `112255286 mod 97` which is equal to `96`.
This result is then pushed to the stack, which now has the state `S = [97, 96]`.

  - **Observe** `-` This token is again an operator token for a binary operator so two arguments are popped from the stack and the operator is applied to them.
This results in the computation of `97-96` which is equal to `1` and this result is pushed to the stack that now has the state `S = [1]`.

  - **Observe** `36` This token is a value token and is pushed to the stack, which now has state `S = [1, 36]`.

  - **Observe** `=` This token is again an operator token for a binary operator so two arguments are popped from the stack.
The operator token is now a test operator and will test equality of both arguments.
Hence, this results in `1 = 36` which is false.
This result is pushed to the stack, which now has it's final state `S = [false]`.

A few things can be noted after seeing this example.
First, the arguments are always passed to an operator in the order in which they appear in the stream, which is important to compose correct grexes.
Second, the stack contains strings that can be cast into other data types as determined by the operator scope.
In the scope of a grex predicate, the stack should in the end contain a Boolean value.

## Operators

### The integer scope

In the integer scope, the operators shown below are available by default.
The standard arithmetic operators next to some basic bitwise operators are available.
There are also a few integer operators that assume the input argument is a datetime and extract a field from that datetime.
Finally, there are some specialized operators.
More information on those special operators can be found below the table.

<div class="tg-wrap"><table class="tg">
<caption>Default operators in the integer scope</caption>
<thead>
  <tr>
    <th class="tg-0pky"></th>
    <th class="tg-c3ow">Symbol</th>
    <th class="tg-c3ow">Args</th>
    <th class="tg-c3ow">Description</th>
    <th class="tg-c3ow">Example</th>
    <th class="tg-c3ow">Result</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky" rowspan="8">Regular<br>arithmetic</td>
    <td class="tg-0pky">+</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Sum</td>
    <td class="tg-0pky">2 3 +</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">-</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Subtraction</td>
    <td class="tg-0pky">10 5 -</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">*</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Multiplication</td>
    <td class="tg-0pky">2 2 *</td>
    <td class="tg-0pky">4</td>
  </tr>
  <tr>
    <td class="tg-0pky">/</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Division</td>
    <td class="tg-0pky">8 4 /</td>
    <td class="tg-0pky">2</td>
  </tr>
  <tr>
    <td class="tg-0pky">%</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Modulo</td>
    <td class="tg-0pky">3 5 %</td>
    <td class="tg-0pky">2</td>
  </tr>
  <tr>
    <td class="tg-0pky">abs</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Absolute value</td>
    <td class="tg-0pky">-3 abs</td>
    <td class="tg-0pky">3</td>
  </tr>
  <tr>
    <td class="tg-0pky">!</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Faculty</td>
    <td class="tg-0pky">3!</td>
    <td class="tg-0pky">6</td>
  </tr>
  <tr>
    <td class="tg-0pky">^</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Power</td>
    <td class="tg-0pky">2 3 ^</td>
    <td class="tg-0pky">8</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="5">Bitwise<br>operators</td>
    <td class="tg-0pky">&gt;&gt;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitshift right</td>
    <td class="tg-0pky">20 2 &gt;&gt;</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">&lt;&lt;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitshift left</td>
    <td class="tg-0pky">20 2 &gt;&gt;</td>
    <td class="tg-0pky">80</td>
  </tr>
  <tr>
    <td class="tg-0pky">&amp;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitwise AND</td>
    <td class="tg-0pky">20 5 &amp;</td>
    <td class="tg-0pky">4</td>
  </tr>
  <tr>
    <td class="tg-0pky">|</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitwise OR</td>
    <td class="tg-0pky">20 5 |</td>
    <td class="tg-0pky">21</td>
  </tr>
  <tr>
    <td class="tg-0pky">~</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Bitwise NOT</td>
    <td class="tg-0pky">20 ~</td>
    <td class="tg-0pky">-21</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="7">Datetime<br>Extraction</td>
    <td class="tg-0pky">year</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract year</td>
    <td class="tg-0pky">2007-12-03T10:15:30 year</td>
    <td class="tg-0pky">2007</td>
  </tr>
  <tr>
    <td class="tg-0pky">month</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract month</td>
    <td class="tg-0pky">2007-12-03T10:15:30 month</td>
    <td class="tg-0pky">12</td>
  </tr>
  <tr>
    <td class="tg-0pky">dayofmonth</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract day of month</td>
    <td class="tg-0pky">2007-12-03T10:15:30 dayofmonth</td>
    <td class="tg-0pky">3</td>
  </tr>
  <tr>
    <td class="tg-0pky">dayofyear</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract day of year</td>
    <td class="tg-0pky">2007-12-03T10:15:30 dayofyear</td>
    <td class="tg-0pky">337</td>
  </tr>
  <tr>
    <td class="tg-0pky">hour</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract hours</td>
    <td class="tg-0pky">2007-12-03T10:15:30 hour</td>
    <td class="tg-0pky">10</td>
  </tr>  
  <tr>
    <td class="tg-0pky">minute</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract minutes</td>
    <td class="tg-0pky">2007-12-03T10:15:30 minute</td>
    <td class="tg-0pky">15</td>
  </tr>
  <tr>
    <td class="tg-0pky">second</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Extract seconds</td>
    <td class="tg-0pky">2007-12-03T10:15:30 second</td>
    <td class="tg-0pky">30</td>
  </tr>  
  <tr>
    <td class="tg-0pky" rowspan="5">Special<br>Operators</td>
    <td class="tg-0pky">D5</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Dihedral Group 5</td>
    <td class="tg-0pky">3 4 D5</td>
    <td class="tg-0pky">2</td>
  </tr>
  <tr>
    <td class="tg-0pky">w+</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Weighted digit sum:<br>second argument indicates<br>weighting schema</td>
    <td class="tg-0pky">156 1,3 w+</td>
    <td class="tg-0pky">22</td>
  </tr>
  <tr>
    <td class="tg-0pky">luhn</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Luhn permutation (0)(9)(3 6)(1 2 4 8 7 5)</td>
    <td class="tg-0pky">4 luhn</td>
    <td class="tg-0pky">8</td>
  </tr>
  <tr>
    <td class="tg-0pky">verhoeff</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Verhoeff permutation (1 5 8 9 4 2 7 0)(3 6)</td>
    <td class="tg-0pky">8 verhoeff</td>
    <td class="tg-0pky">9</td>
  </tr>
  <tr>
    <td class="tg-0pky">verhoeff*</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Verhoeff permutation power: <br>second argument indicates power</td>
    <td class="tg-0pky">8 3 verhoeff*</td>
    <td class="tg-0pky">2</td>
  </tr>
</tbody>
</table></div>

The **dihedral group** operator `D5` forms the basis of the Verhoeff check digit schema.
The implementation uses the Cayley table as proposed by Verhoeff in his PhD thesis and can be consulted [here](https://en.wikipedia.org/wiki/Verhoeff_algorithm).

The **weighted digit sum** `w+` is extremely useful to verify many check digit schemas.
This operator takes two arguments, of which the first needs to be an integer.
The second argument determines the weight schema.
The operator `w+` takes the first argument, multiplies each digit from that number with a weight determined by the weight schema and then sums all weighted digits into a final result.
The number of weight is automatically derived from the number of digits to which weights must be applied.
The following weight schemata are available:

- `1->2^n` Powers of two, ranging up and starting at power `1`. For example, the grex `1469 1->2^ n w+` will generate four weights `2`, `4`, `8` and `16` and will evaluate to:
`1*2 + 4 * 4 + 6 * 8 + 9 * 16 = 210`

- `0->2^n` Powers of two, ranging up and starting at power `0`. For example, the grex `1469 0->2^ n w+` will generate four weights `1`, `2`, `4` and `8` and will evaluate to:
`1*1 + 4 * 2 + 6 * 4 + 9 * 8 = 105`

- `2^ n<-1` Powers of two, ranging down and ending at power `1`. For example, the grex `1469 2^ n<-1 w+` will generate four weights `16`, `8`, `4` and `2` and will evaluate to:
`1*16 + 4 * 8 + 6 * 4 + 9 * 2 = 90`

- `2^n<-0` Powers of two, ranging down and ending at power `0`. For example, the grex `1469 2^ n<-0 w+` will generate four weights `8`, `4`, `2` and `1` and will evaluate to:
`1*8 + 4 * 4 + 6 * 2 + 9 * 1 = 45`

- `d->n` Incrementally ranging up, starting at d.
Here, d is a parameter that can be set to an integer number.
For example, the grex `1469 3->n w+` will generate four weights `3`, `4`, `5` and `6` and will evaluate to:
`1*3 + 4 * 4 + 6 * 5 + 9 * 6 = 103`

- `d<-n` Incrementally ranging down, starting from d.
Here, d is a parameter that can be set to an integer number.
For example, the grex `1469 3<-n w+` will generate four weights `3`, `2`, `1` and `0` and will evaluate to:
`1*3 + 4 * 2 + 6 * 1 + 9 * 0 = 17`

- `n->d` Incrementally ranging up, ending at d.
Here, d is a parameter that can be set to an integer number.
For example, the grex `1469 n->3 w+` will generate four weights `0`, `1`, `2` and `3` and will evaluate to:
`1*0 + 4 * 1 + 6 * 2 + 9 * 3 = 43`

- `n<-d` Incrementally ranging down, ending at d.
Here, d is a parameter that can be set to an integer number.
For example, the grex `1469 n<-3 w+` will generate four weights `6`, `5`, `4` and `3` and will evaluate to:
`1*6 + 4 * 5 + 6 * 4 + 9 * 3 = 77`

- <code>w<sub>1</sub>,w<sub>2</sub>...</code> A repetitive weight pattern that is cyclically applied and starts at w<sub>1</sub>. 
For example, the grex `1469 1,3 w+` will generate four weights `1`, `3`, `1` and `3` and will evaluate to:
`1*1 + 4 * 3 + 6 * 1 + 9 * 3 = 46`

Lastly, there two often used decimal permutations defined here: the **Luhn** permutation `luhn` used in the IBM check digit schema and the **Verhoeff** permutation `verhoeff`.
For this latter permutation, we have also predefined the power operator `verhoeff*`, where the second argument of the operator determines the power, i.e., the number of times the permutation needs to be applied (see also [Customizing operators](index.md#portability)).
For example,`9 3 verhoeff*` starts with argument `9` and applies the verhoeff permutation 3 times, leading to the result `7`.

Next to the scope operators, the integer scope comes with a set standard tests summarized in the table below.

<div class="tg-wrap"><table class="tg">
<caption>Default tests in the integer scope</caption>
<thead>
  <tr>
    <th class="tg-0lax">Symbol</th>
    <th class="tg-0lax">Args</th>
    <th class="tg-0lax">Description</th>
    <th class="tg-0lax">Example</th>
    <th class="tg-0lax">Result</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">&lt;</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Strictly smaller than</td>
    <td class="tg-0lax">2 2 &lt;</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">&lt;=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Smaller than or equal to</td>
    <td class="tg-0lax">2 2 &lt;=</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Equal to</td>
    <td class="tg-0lax">2 2 =</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">!=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Not equal to</td>
    <td class="tg-0lax">2 2 !=</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">&gt;=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Greater than or equal to</td>
    <td class="tg-0lax">2 2 &gt;=</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">&gt;</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Strictly greater than</td>
    <td class="tg-0lax">2 2 &gt;</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">between</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">First argument between bounds</td>
    <td class="tg-0lax">2 2 5 beween</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">betweenx</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">First argument between bounds, bounds exclusive</td>
    <td class="tg-0lax">2 2 5 betweenx</td>
    <td class="tg-0lax">false</td>
  </tr>
</tbody>
</table></div>

### The big integer scope

The big integer scope serves the purpose to deal with very large integer numbers beyond the reach of the regular 4 or 8 bytes encodings.
This scope should only be used if the regular integer scope would imply overflow errors.
For example, this would be the case with modulo arithmetic for verification of bank accounts like IBAN numbers.
The operators shown below are available by default.

<div class="tg-wrap"><table class="tg">
<caption>Default operators in the big integer scope</caption>
<thead>
  <tr>
    <th class="tg-0pky"></th>
    <th class="tg-c3ow">Symbol</th>
    <th class="tg-c3ow">Args</th>
    <th class="tg-c3ow">Description</th>
    <th class="tg-c3ow">Example</th>
    <th class="tg-c3ow">Result</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky" rowspan="8">Regular<br>arithmetic</td>
    <td class="tg-0pky">+</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Sum</td>
    <td class="tg-0pky">2 3 +</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">-</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Subtraction</td>
    <td class="tg-0pky">10 5 -</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">*</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Multiplication</td>
    <td class="tg-0pky">2 2 *</td>
    <td class="tg-0pky">4</td>
  </tr>
  <tr>
    <td class="tg-0pky">/</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Division</td>
    <td class="tg-0pky">8 4 /</td>
    <td class="tg-0pky">2</td>
  </tr>
  <tr>
    <td class="tg-0pky">%</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Modulo</td>
    <td class="tg-0pky">3 5 %</td>
    <td class="tg-0pky">2</td>
  </tr>
  <tr>
    <td class="tg-0pky">abs</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Absolute value</td>
    <td class="tg-0pky">-3 abs</td>
    <td class="tg-0pky">3</td>
  </tr>
  <tr>
    <td class="tg-0pky">!</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Faculty</td>
    <td class="tg-0pky">3!</td>
    <td class="tg-0pky">6</td>
  </tr>
  <tr>
    <td class="tg-0pky">^</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Power</td>
    <td class="tg-0pky">2 3 ^</td>
    <td class="tg-0pky">8</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="5">Bitwise<br>operators</td>
    <td class="tg-0pky">&gt;&gt;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitshift right</td>
    <td class="tg-0pky">20 2 &gt;&gt;</td>
    <td class="tg-0pky">5</td>
  </tr>
  <tr>
    <td class="tg-0pky">&lt;&lt;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitshift left</td>
    <td class="tg-0pky">20 2 &gt;&gt;</td>
    <td class="tg-0pky">80</td>
  </tr>
  <tr>
    <td class="tg-0pky">&amp;</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitwise AND</td>
    <td class="tg-0pky">20 5 &amp;</td>
    <td class="tg-0pky">4</td>
  </tr>
  <tr>
    <td class="tg-0pky">|</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Bitwise OR</td>
    <td class="tg-0pky">20 5 |</td>
    <td class="tg-0pky">21</td>
  </tr>
  <tr>
    <td class="tg-0pky">~</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Bitwise NOT</td>
    <td class="tg-0pky">20 ~</td>
    <td class="tg-0pky">-21</td>
  </tr>

</tbody>
</table></div>

Next to the scope operators, the big integer scope has exactly the same tests as the integer scope.
### The string scope

In the string scope, the operators shown below are available by default.
These operators include some basic operators like concatenation, trimming, conversion to lower or upper case and character reversion.
Some additional information on string operators is given below the table.

<div class="tg-wrap"><table class="tg">
<caption>Default operators in the string scope</caption>
<thead>
  <tr>
    <th></th>
    <th class="tg-0lax">Symbol</th>
    <th class="tg-0lax">Args</th>
    <th class="tg-0lax">Description</th>
    <th class="tg-0lax">Example</th>
    <th class="tg-0lax">Result</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky" rowspan="13">Basic<br>Operators</td>
    <td class="tg-0lax">||</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Concatenate two strings</td>
    <td class="tg-0lax">Hello World ||</td>
    <td class="tg-0lax">HelloWorld</td>
  </tr>
  <tr>
    <td class="tg-0lax">concat</td>
    <td class="tg-0lax">-1</td>
    <td class="tg-0lax">Concatenate all tokens on the stack</td>
    <td class="tg-0lax">10 . 10 / 40 concat</td>
    <td class="tg-0lax">10.10/40</td>
  </tr>
  <tr>
    <td class="tg-0lax">trim</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Trim leading and trailing whitespaces</td>
    <td class="tg-0lax">" Hello " trim</td>
    <td class="tg-0lax">Hello</td>
  </tr>
  <tr>
    <td class="tg-0lax">lowercase</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Convert to lowercase</td>
    <td class="tg-0lax">Hello lowercase</td>
    <td class="tg-0lax">hello</td>
  </tr>
  <tr>
    <td class="tg-0lax">uppercase</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Concert to uppercase</td>
    <td class="tg-0lax">Hello uppercase</td>
    <td class="tg-0lax">HELLO</td>
  </tr>
  <tr>
    <td class="tg-0lax">reverse</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Reverse order of characters</td>
    <td class="tg-0lax">Hello reverse</td>
    <td class="tg-0lax">olleH</td>
  </tr>
    <tr>
    <td class="tg-0lax">keep</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Filter characters from the first argument</td>
    <td class="tg-0lax">a2bc4 0..9 keep</td>
    <td class="tg-0lax">24</td>
  </tr>
  <tr>
    <td class="tg-0lax">substring</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">Returns a substring from the first argument</td>
    <td class="tg-0lax">Hello 1 3 substring</td>
    <td class="tg-0lax">el</td>
  </tr>
  <tr>
    <td class="tg-0lax">nbspremove</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Replaces non-breaking spaces with regular whitespaces</td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
  <tr>
    <td class="tg-0lax">spacecomp</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Replaces multiple consecutive white spaces with a single whitespace</td>
    <td class="tg-0lax">"Hi    John" spacecomp</td>
    <td class="tg-0lax">Hi John</td>
  </tr>
  <tr>
    <td class="tg-0lax">lpad</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">Pads a string to the left until a minimum length is obtained</td>
    <td class="tg-0lax">AB X 5 lpad</td>
    <td class="tg-0lax">XXXAB</td>
  </tr>
  <tr>
    <td class="tg-0lax">rpad</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">Pads a string to the right until a minimum length is obtained</td>
    <td class="tg-0lax">AB X 5 rpad</td>
    <td class="tg-0lax">ABXXX</td>
  </tr>
  <tr>
    <td class="tg-0lax">split</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">Splits a string according to a split pattern and inserts a given character</td>
    <td class="tg-0lax">128963 2,3,1 - split</td>
    <td class="tg-0lax">12-896-3</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="5">Encoding/<br>Decoding</td>
    <td class="tg-0lax">cc:a-&gt;0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Caesar coding with 'a' mapped to 0</td>
    <td class="tg-0lax">abc cc:a-&gt;0</td>
    <td class="tg-0lax">012</td>
  </tr>
  <tr>
    <td class="tg-0lax">cc:a-&gt;1</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Caesar coding with 'a' mapped to 1</td>
    <td class="tg-0lax">abc cc:a-&gt;1</td>
    <td class="tg-0lax">123</td>
  </tr>
  <tr>
    <td class="tg-0lax">cc:a-&gt;10</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Caesar coding with 'a' mapped to 10</td>
    <td class="tg-0lax">abc cc:a-&gt;10</td>
    <td class="tg-0lax">101112</td>
  </tr>
  <tr>
    <td class="tg-0lax">crc32</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Computes a crc-32 checksum of the input string</td>
    <td class="tg-0lax">2016000869 crc32</td>
    <td class="tg-0lax">2323174389</td>
  </tr>
  <tr>
    <td class="tg-0lax">crockford32decode</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Performs a Crockford Base 32 decode of a string</td>
    <td class="tg-0lax">031rekg crockford32decode</td>
    <td class="tg-0lax">102513264</td>
  </tr>
  <tr>
    <td class="tg-0pky" rowspan="2">Datetime <br> extraction</td>
    <td class="tg-0lax">dayofweek</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Extract day of week</td>
    <td class="tg-0lax">2007-12-03T10:15:30 dayofweek</td>
    <td class="tg-0lax">MONDAY</td>
  </tr>
  <tr>
    <td class="tg-0lax">month</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Extract month name</td>
    <td class="tg-0lax">2007-12-03T10:15:30 month</td>
    <td class="tg-0lax">DECEMBER</td>
  </tr>
</tbody>
</table></div>

The **substring operator** takes three arguments: a string and two indices.
The result is a substring of the first argument, starting at the index given as the second argument (inclusive) and ending at the index given as the third argument (exclusive).
Character count starts at zero.
For both of the index arguments of the substring operator, the wildcard `*` can be specified.
If the starting index is given a wildcard, it will be defaulted to 0.
If the ending index is given a wildcard, it will be defaulted to the length of the first argument string.

The **crc32** operator computes a CRC-32 checksum of the input string.
It uses the native implementation available in [Java](https://docs.oracle.com/javase/8/docs/api/java/util/zip/CRC32.html).
This can come in handy as some check digit schemes for identifiers use this CRC32 code.

The **crockford32decode** operator computes an integer number from a string based on the [Crockford Base32](http://www.crockford.com/base32.html) schema.
In the implementation, Crockford check symbols were not included.

The **keep** operator is a binary operator that takes a string as its first argument and a filter schema as its second argument.
The operator will apply a character filter to the first argument and retain only those characters that pass the filter.
Characters *not* passing the filter are removed rather than replaced by e.g. white space characters.

- <tt>digits</tt> Only characters that qualify as a digit, in any alphabet, are kept.
This includes Latin digits, Devanagari and (extended) Arabic-Indic digits.
- <tt>0..9</tt> Only Latin digits are kept.
- <tt>letters</tt> Only characters that qualify as letters are kept.
- <tt>letters+spacing</tt> Only characters that qualify as letters or whitespaces are kept.
- <tt>alphanum</tt> Only characters that qualify either as letters, digits (in any alphabet) are kept.
- <tt>alphanum+spacing</tt> Only characters that qualify either as letters, digits (in any alphabet) or whitespaces are kept.

The standard operators contain three predefined **Caesar codes** (`cc:a->0`, `cc:a->1`, `cc:a->10`), where characters from the alphabet are mapped to integer numbers.
This is for example useful in check digit schemes where non-numerical characters are used.
An example of such a schema is the IBAN check digit schema.
Note that if different (Caesar) character mappings are necessary, this is possible via defining customized operators (see [Portability](portability)).

Besides the basic operators, the string scope comes with the default tests summarized in the table below.
More information on these tests can be found below the table.

<div class="tg-wrap"><table class="tg">
<caption>Default tests in the string scope</caption>
<thead>
  <tr>
    <th class="tg-0lax">Symbol</th>
    <th class="tg-0lax">Args</th>
    <th class="tg-0lax">Description</th>
    <th class="tg-0lax">Example</th>
    <th class="tg-0lax">Result</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">contains</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Superstring test</td>
    <td class="tg-0lax">hello ello contains</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Equality test</td>
    <td class="tg-0lax">hello ello =</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">!=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Inequality test</td>
    <td class="tg-0lax">hello ello !=</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">&lt;</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Before test in lexicographical order</td>
    <td class="tg-0lax">hello ello &lt;</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">&lt;=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Before or equal test in lexicographical order</td>
    <td class="tg-0lax">hello ello &lt;=</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">&gt;</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">After test in lexicographical order</td>
    <td class="tg-0lax">hello ello &gt;</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">&gt;=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">After or equal test in lexicographical order</td>
    <td class="tg-0lax">hello ello &gt;=</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">shorter</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Shorter than test</td>
    <td class="tg-0lax">hello ello shorter</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">shortereq</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Shorter than or same length test</td>
    <td class="tg-0lax">hello ello shortereq</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">longer</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Longer than test</td>
    <td class="tg-0lax">hello ello longer</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">longereq</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Longer than or same length test</td>
    <td class="tg-0lax">hello ello longereq</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">eqlong</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Equal length test</td>
    <td class="tg-0lax">hello ello eqlong</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">~=</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Regex match test</td>
    <td class="tg-0lax">hello he.* ~=</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">startswith</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Prefix test</td>
    <td class="tg-0lax">hello hel startswith</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">endswith</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Suffix test</td>
    <td class="tg-0lax">hello ello endswith</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">notstartswith</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Negated prefix test</td>
    <td class="tg-0lax">hello hel notstartswith</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">notendswith</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Negated suffix test</td>
    <td class="tg-0lax">hello ello notendswith</td>
    <td class="tg-0lax">false</td>
  </tr>
  <tr>
    <td class="tg-0lax">httpstatus</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">URL resolution test</td>
    <td class="tg-0lax">http://ledc.ugent.be 200 httpstatus</td>
    <td class="tg-0lax">true</td>
  </tr>
  <tr>
    <td class="tg-0lax">isdatetime</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Date time test</td>
    <td class="tg-0lax">"10/10/2012" "dd/MM/yyyy" isdatetime</td>
    <td class="tg-0lax">true</td>
  </tr>
</tbody>
</table></div>

The **regex match** operator `~=`can be used to match certain subgroups of the first regex against a new regex.
In many cases, it is possible to include this regex test in the initial regex in the scope of which the grex is defined.
However, allowing regex matching inside a grex can be useful when the additional constraint on a group should only hold in some conditions.

The **httpstatus** test takes two arguments.
The first is a string that represents and URL that needs testing.
The second argument represents a white-space-separated list of acceptable HTTP status codes.
The test will resolve the URL and see if the status code is among the acceptable ones.
The second argument can be given a wildcard `*`.
In that case, the list of acceptable codes consists of 200 (`OK`), 201 (`Created`) and 202 (`Accepted`).

Temporal information can be verified by the **isdatetime** operator.
With this operator, the first argument is a string that is matched against a date-time pattern specified in the second argument.
The test will return true if the given string (first argument) is a valid instance of the requested pattern (second argument).
Patterns must be encoded with the symbols and rules used by the  [Java DateTime formatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html).

### The double scope
In the double scope, the basic arithmetic operators shown in the table below can be used.

<div class="tg-wrap"><table class="tg">
<caption>Default operators in the double scope</caption>
<thead>
  <tr>
    <th class="tg-0lax">Symbol</th>
    <th class="tg-0lax">Args</th>
    <th class="tg-0lax">Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">+</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Sum</td>
  </tr>
  <tr>
    <td class="tg-0lax">-</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Subtraction</td>
  </tr>
  <tr>
    <td class="tg-0lax">*</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Multiplication</td>
  </tr>
  <tr>
    <td class="tg-0lax">/</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Division</td>
  </tr>
  <tr>
    <td class="tg-0lax">^</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Power</td>
  </tr>
  <tr>
    <td class="tg-0lax">abs</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Absolute value</td>
  </tr>
  <tr>
    <td class="tg-0lax">sin</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Sine</td>
  </tr>
  <tr>
    <td class="tg-0lax">cos</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Cosine</td>
  </tr>
  <tr>
    <td class="tg-0lax">tan</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Tangent</td>
  </tr>
  <tr>
    <td class="tg-0lax">exp</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Exponential Function</td>
  </tr>
  <tr>
    <td class="tg-0lax">log</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Logarithm base 10</td>
  </tr>
  <tr>
    <td class="tg-0lax">ln</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Natural logarithm</td>
  </tr>
  <tr>
    <td class="tg-0lax">sqrt</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">Square root</td>
  </tr>
</tbody>
</table></div>
For tests, the double scope offers exactly the same tests as the integer scope.

### The datetime scope
The datetime scope provides a few basic operators to introduce temporal information in ledc-pi.
The scope operators are shown in the table below.
Although there are currently only two operators in the datetime scope, more can be done by producing a date and using other scope operators to extract information from these dates.

<div class="tg-wrap"><table class="tg">
<caption>Default operators in the datetime scope</caption>
<thead>
  <tr>
    <th class="tg-0lax">Symbol</th>
    <th class="tg-0lax">Args</th>
    <th class="tg-0lax">Description</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">now</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">Produces a timestamp that reflects the current local time</td>
  </tr>
  <tr>
    <td class="tg-0lax">parse</td>
    <td class="tg-0lax">2</td>
    <td class="tg-0lax">Parses the first argument with the second as pattern</td>
  </tr>
</tbody>
</table></div>

### The Boolean scope
In the Boolean scope, the usual Boolean connectives can be used to combine the outcome of tests.
Available operators are `or`, `and`, `xor` and `not`.

### Creating new variables
A special operator is reserved for the creation of new variables.
The operator `mkvar` pops a token from the stack and assigns this value to a new variable.
The variable is treated like the variables created by regex groups and the indexing just continues in the same way.
For example, if the regex underlying the grex has three capture groups, this will create four variables which can be referenced by `@0`, `@1`, `@2` and `@3`.
The first occurrence of `mkvar` will then create a new variable that can be referenced by `@4`.
Any next occurrence creates again a new variable and so on.
The creation of new variables is very useful to avoid repeated computations.

### Branching operators

In some scenarios where you define a grex, you can find yourself in the position where you need to do a *large* amount of different tests on the same situation.
One good example of such a situation, is in the verification of ISBN numbers or ISMN numbers.
Here you typically want to match a group in a regex and the content of that group will be a number that can be in a set of different *ranges*.

For example, you would want to check if some number is either equal to 2 or between 5 and 9 or between 11 and 15.
You could then use different grex predicates (one for each range) and combine them with a disjunctive predicate.
Suppose for the sake of example that the number we want to test is produced by capture group `@1` from some regex $`\Sigma`$, then the following three grex predicates could be defined.

- `::int @1 2 =`
- `::int @1 5 9 between`
- `::int @1 11 15 between`

Then, we could combine these three predicates disjunctively.
The problem with this approach is that for *each* grex predicate, we need to evaluate and test the same regex $`\Sigma`$ over and over again.
If the number of cases becomes high (and it does in the case of ISBN numbers) this will be highly inefficient.

In such a scenario, the use of branching operators is advised.
Currently, there are two branching operators `branch&` and `branch|`.
By using these operators, it is possible to specify a collection of tests and combine the outcomes in a *conjunctive* way (with `branch&`) or a *disjunctive* way (with `branch|`).
When using branching operators, the rules of [Postfix parsing](grexes.md#postfix-notation-and-parsing) slightly alter:

- In the current version of ledc-pi, a grex can contain *at most* one branching operator.

- Tokens that come *before* the branching operator, follow the usual rules of Postfix parsing (see [Postfix parsing](grexes.md#postfix-notation-and-parsing)).

- After the branching operator, the scope can not be changed anymore.
This is because all branches do testing in a specific scope.

- After the branching started, each sequence of tokens that *ends* with a test operator is called a *branch*. 

- The state of the stack as it was, just before the branching started, will be passed to each of the branches.
So each branch is actually treated as a separate grex that starts with a fixed stack that is potentially non-empty.

- The outcomes of all branches are combined with a Boolean connective: AND in case of `branch&` and OR in case of `branch|`.
During testing of branches, if we see an outcome `true` in the case of `branch|` or `false` in the case of `branch&`, no further branches are tested.

In the example of range testing, we can now use branching to combine all tests in the following single grex:

`::int @1 branch| 2 = 5 9 between 11 15 between`

The part `::int @1` is only written once.
When executing this part, the stack will contain the value of group 1.
That stack is then fed to each of the branches, where the first one tests equality to 2, the second one tests the range 5 to 9 and the third one tests the range 11 to 15.
This is  visualized below

```mermaid
graph LR
  A("::int @1")-->B[OR];
  B--branch 1-->C(2 =);
  B--branch 2-->D(5 9 between);
  B--branch 3-->E(11 15 between);

  style B fill:#ffffff, stroke:#2bbde9
```

Branching can be a powerful tool.
Not only is the notation of this grex shorted, the parsing and matching of the regex and the determination of the capture groups is done only once.
So this latter expression is superior in efficiency.

