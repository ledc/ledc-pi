# ledc-pi

## Intro

Suppose you need to verify or standardize the format of some identifier like an ISBN number or an ORCID code.
You then go to Google to look for the format of that identifier and two things can happen.
You can find a nice description in PDF and then you start coding.
Or, you find a ton of websites that say they can validate it for you.
In the first case, it can be very time consuming to read, understand and code the desired constraints on the format.
In the second case, you don't really know what's been checked behind the scenes and standardizing is often not possible.

Those days will be over soon, we hope.
Ledc-pi offers a *transparant* and *easy* way to build and exchange the knowledge to validate, convert and search for instances of attributes.

## What

This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.
Here, you can find ledc-pi which has a focus on validating individual attributes, 
Ledc-pi is a simple yet highly expressive module of ledc to define ordinal-scaled *quality measures* for individual columns in your dataset. It's main features are:

* **Expressiveness**: the usage of *group expressions* (Grexes) on top of regexes enables a wide range of predicates that can be defined for properties.

* **Standardization**: rather than working with column names, ledc-pi connects several components like quality measures, convertors and scanners with properties that are identified via URIs.

* **Conversion**: easy conversion between different properties. Among other things, this allows standardization of values for properties and increases referential integrity across databases.

* **Scanning**: if data is not in a nicely structured database, scanners can help to search in free text fields to localized instances of properties.
It will use measures to avoid false positives as much as possible.

* **Portability**: quality measures, convertors and scanners are stored in registries that can be dumped to [JSON files](registries) and ported to other servers that run ledc-pi as a property checking agent.
On the long term, this makes it possible to setup a network of web-connected servers that can communicate and exchange these registries. 
 
## Getting started

As all current and future components of ledc, ledc-pi is written in Java.
To run it, as of version 1.2, you require Java 17 (or higher) and Maven.
Just pull the code from this repository and build it with Maven.
To use ledc-pi, you can either [read registries from a JSON file](docs/introduction.md#reading-an-existing-registry-from-a-json-file) our [start building your own components](docs/introduction.md#building-your-own-components-in-java).
Click on the hyperlinks to see example code.

## References

Below are listed some of the publications where the core ideas of ledc-pi where conceived.
The basic ideas on group expression are described in the 2016 paper.

* Yoram Timmerman and Antoon Bronselaer, Measuring data quality in information systems research, *Decision Support Systems*, p. 1-7, (**2019**) 

* Antoon Bronselaer, Robin De Mol and Guy De Tré , A measure-theoretic foundation for data quality, *IEEE Transactions on Fuzzy Systems*, 26(2), p.627-639 (**2018**)

* Antoon Bronselaer, Joachim Nielandt, Robin De Mol and Guy De Tré , Ordinal assessment of data consistency based on regular expressions, *Proceedings of the IPMU Conference*, p.317-328 (**2016**)
