package be.ugent.ledc.pi.grex.operator;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.Function;

public class Between<T extends Comparable<T>> implements Test<T>
{
    private final Function<String, T> convertor;
    
    private final boolean boundsInclusive;
    
    public Between(Function<String, T> convertor, boolean boundsInclusive)
    {
        this.convertor = convertor;
        this.boundsInclusive = boundsInclusive;
    }
    
    @Override
    public Boolean apply(String... arguments) throws GrexParseException
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("Operator " + getSymbol() + " requires 3 arguments");
        
        if(arguments[0] == null || arguments[1] == null || arguments[2] == null)
            return false;
        
        T operand = convertor.apply(arguments[0]);
        T lower = convertor.apply(arguments[1]);
        T upper = convertor.apply(arguments[2]);
        
        return boundsInclusive
            ? operand.compareTo(lower) >= 0 && operand.compareTo(upper) <= 0
            : operand.compareTo(lower) > 0 && operand.compareTo(upper) < 0;
    }

    @Override
    public String getSymbol()
    {
        return boundsInclusive ? "between" : "betweenx";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 3;
    }
    
}
