package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractDecimal implements Operator<String>
{
    private final boolean trim;

    public ExtractDecimal(boolean trim)
    {
        this.trim = trim;
    }
    
    @Override
    public String getSymbol()
    {
        return trim ? "exdectrim" : "exdec";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException(
                getSymbol()
                    +   " requires "
                    + getNumberOfArguments()
                    + " arguments"
            );
        
        String s = arguments[0];
        String format = arguments[1];
        
        String fPattern = "([,.])\\{(\\d{1,2})\\}(c|f|r)?";
        
        Matcher fMatcher = Pattern.compile(fPattern).matcher(format);
        
        if(fMatcher.matches())
        {
            String separator = fMatcher.group(1);
            int decimals = Integer.parseInt(fMatcher.group(2));
            
            RoundingMode rMode = RoundingMode.HALF_UP;
            
            if(fMatcher.group(3) != null)
            {
                switch(fMatcher.group(3))
                {
                    case "c":
                        rMode = RoundingMode.CEILING;
                        break;
                    case "f":
                        rMode = RoundingMode.FLOOR;
                        break;
                    case "r":
                        //Do nothing
                }            
            }
            
            Pattern p = Pattern.compile(
                "^[^\\d\\-.,]*"
                + "((-?[\\d,.]+([.,]\\d+)?)|((-?[\\d,.]+)?[.,]\\d+))"
                + "[^\\d]*$");
            
            Matcher m = p.matcher(s);

            if(m.matches())
            {
                String v = m.group(1).replace(',', '.');
                
                while(v.indexOf(".") != v.lastIndexOf("."))
                {
                    v = v.replaceFirst("\\.", "");
                }
                
                String vRounded = new BigDecimal(v)
                    .setScale(decimals, rMode)
                    .toPlainString();
                
                String sep = separator.equals(",")
                    ? vRounded.replace('.', ',')
                    : vRounded;
                
                return trim && sep.contains(separator)
                    ? trimZeros(sep)
                    : sep;
            }

            return "";
        }
        else
            throw new GrexParseException(
                "Second parameter of exdec must be a format string of type"
                + " ,|.{n}x where n is an integer between 0 and 99 (number of desired decimals)"
                + " and x is an optional character indicating the rounding mode (c=ceiling, f=floor, r=rounding)."
            );
    }
    
    private String trimZeros(String s)
    {
        int idx  = s.length()-1;
        
        while(idx >= 0 && s.charAt(idx) == '0')
            idx--;
        
        return Character.isDigit(s.charAt(idx))
            ? s.substring(0, idx+1)
            : s.substring(0, idx);
        
        
    }
}
