package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.BiPredicate;

public enum BinaryStringTests implements StringTest
{
    CONTAINS("contains", (s,t) -> s.contains(t)),
    EQUALS("=", (s,t) -> s.equals(t)),
    NOT_EQUALS("!=", (s,t) -> !s.equals(t)),
    BEFORE("<", (s,t) -> s.compareTo(t) < 0),
    BEFORE_OR_EQUALS("<=", (s,t) -> s.compareTo(t) <= 0),
    AFTER(">", (s,t) -> s.compareTo(t) > 0),
    AFTER_OR_EQUALS(">=", (s,t) -> s.compareTo(t) >= 0),
    SHORTER_THAN("shorter", (s,t) -> s.length() < t.length()),
    SHORTER_THAN_OR_AS_LONG("shortereq", (s,t) -> s.length() <= t.length()),
    LONGER_THAN("longer", (s,t) -> s.length() > t.length()),
    LONGER_THAN_OR_AS_LONG("longereq", (s,t) -> s.length() >= t.length()),
    AS_LONG("eqlong", (s,t) -> s.length() == t.length()),
    MATCHES("~=", (s,t) -> s.matches(t)),
    BEGINS_WITH("startswith", (s,t) -> s.startsWith(t)),
    ENDS_WITH("endswith", (s,t) -> s.endsWith(t)),
    NOT_BEGINS_WITH("notstartswith", (s,t) -> !s.startsWith(t)),
    NOT_ENDS_WITH("notendswith", (s,t) -> !s.endsWith(t));

    private final String symbol;
    private final BiPredicate<String, String> operator;
    
    private BinaryStringTests(String symbol, BiPredicate<String, String> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public Boolean apply(String... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.test(arguments[0], arguments[1]);
    }
}
