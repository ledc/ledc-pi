package be.ugent.ledc.pi.grex.operator.real;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.BinaryOperator;

public enum BinaryDoubleOperators implements DoubleOperator
{
    ADDITION("+", (a,b) -> a + b),
    SUBTRACTION("-", (a,b) -> a - b),
    MULTIPLICATION("*", (a,b) -> a * b),
    DIVISION("/", (a,b) -> a / b),
    POWER("^", (a,b) -> Math.pow(a,b))  
    ;

    private final String symbol;
    private final BinaryOperator<Double> operator;
    
    private BinaryDoubleOperators(String symbol, BinaryOperator<Double> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public double apply(double... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.apply(arguments[0], arguments[1]);
    }
}
