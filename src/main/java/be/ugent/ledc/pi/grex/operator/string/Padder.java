package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;

public class Padder implements Operator<String>
{
    private final boolean left;

    public Padder(boolean left)
    {
        this.left = left;
    }
    
    @Override
    public String getSymbol()
    {
        return left ? "lpad" : "rpad";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 3;
    }

    @Override
    public String apply(String... arguments)
    {
        String s = arguments[0];
        String p = arguments[1];
        
        if(p.length() == 0)
            throw new GrexParseException("Padding strings cannot be empty");
        
        if(!arguments[2].matches("\\d+"))
            throw new GrexParseException("Size must be an integer number");
        
        int size = Integer.parseInt(arguments[2]);
        
        while(s.length() < size)
        {
            s = left ? p+s : s+p;
        }
        
        return s;   
    }
    
}
