package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.operator.string.StringTest;
import be.ugent.ledc.pi.grex.GrexParseException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UriTest implements StringTest
{
    public static final String RESPONSE_CODE_WILDCARD = "*";
    
    @Override
    public String getSymbol()
    {
        return "httpstatus";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public Boolean apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("Operator " + getSymbol() + " requires " + getNumberOfArguments() +  " arguments.");
        
        String uriToTest = arguments[0];
        
        Set<Integer> accepted = arguments[1].equals(RESPONSE_CODE_WILDCARD)
            ? Stream.of(HttpURLConnection.HTTP_OK, HttpURLConnection.HTTP_CREATED, HttpURLConnection.HTTP_ACCEPTED).collect(Collectors.toSet())
            : Stream.of(arguments[1].split(" ")).map(s -> Integer.parseInt(s)).collect(Collectors.toSet());
        
        try
        {
            HttpURLConnection urlConnection = (HttpURLConnection) new URL(uriToTest).openConnection();
            urlConnection.connect();
            
            for(Integer code: accepted)
            {
                if(code.equals(urlConnection.getResponseCode()))
                    return true;
            }
            return false;
            
        }
        catch(IOException ex)
        {
            return false;
        }
    }
}
