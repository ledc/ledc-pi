package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.function.UnaryOperator;

public enum UnaryStringOperators implements Operator<String>
{
    TRIM("trim", (s) -> s.trim()),
    LOWERCASE("lowercase", (s) -> s.toLowerCase()),
    UPPERCASE("uppercase", (s) -> s.toUpperCase()),
    REVERT("reverse", (s) -> new StringBuilder(s).reverse().toString()),
    LENGTH("length", (s) -> String.valueOf(s.length())),
    NBSP("nbspremove", (s) -> s.replaceAll("\\xA0", " ")),
    SPACECOMP("spacecomp", (s) -> s.replaceAll("\\s+", " "));

    private final String symbol;
    private final UnaryOperator<String> operator;
    
    private UnaryStringOperators(String symbol, UnaryOperator<String> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(arguments[0]);
    }
}
