package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import be.ugent.ledc.pi.grex.operator.OperatorFeature;
import be.ugent.ledc.pi.grex.operator.OperatorType;
import be.ugent.ledc.pi.grex.operator.PersistableOperator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class IntegerPermutation implements Operator<Long>, PersistableOperator<Long>
{   
    private final String symbol;
    
    /**
     * The power marker allows to indicate that the Operator needs to be treated as a power of permutations.
     * To do so, let the symbol of the permutation operator end with the power marker. 
     * In that case, the operator will be treated as a binary operator with first argument the integer to which we apply the permutation
     * and the second argument the power of the permutation. This is the number of times the permutation will be applied.
     * The power needs to be a positive, non-zero integer.
     * 
     * Example: choosing symbol "P*" makes the operator a power of permutations.
     * If the single argument alternative is wanted, then choose "P" as symbol.
     */
    public static final String POWER_MARKER = "*";
    
    private final Map<Long, Long> permutationMap;
    
    public IntegerPermutation(String cycleNotation, String symbol)
    {
        this.symbol = symbol;
        this.permutationMap = new HashMap<>();
        this.parse(cycleNotation);
    }
    
    @Override
    public Long apply(String... arguments) throws GrexParseException
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("Wrong number of arguments for operator " + symbol);
        
        //Find the target
        Long target = permutationMap.get(Long.parseLong(arguments[0]));
        
        if(target == null)
            throw new GrexParseException(symbol + " does not contain a map for integer " + arguments[0] + ".");
        
        if(getNumberOfArguments() == 1)
            return target;
        
        long power = Long.parseLong(arguments[1]);
        
        if(power < 1)
            throw new GrexParseException("Cannot apply " + symbol + " with a negative or zero power.");
        
        if(power == 1)
            return target;
            
        return apply(target.toString(), Long.toString(power - 1));
    }

    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return symbol.endsWith(POWER_MARKER) ? 2 : 1;
    }
    
    /**
     * Parses the permutation map from the cycle notation in String form.
     * @param cycleNotation 
     */
    private void parse(String cycleNotation)
    {
        //Clear the permutation
        this.permutationMap.clear();
        
        //First parse into different cycles
        List<String> cycles = Collections.list(
            new StringTokenizer(cycleNotation, "\\(|\\)\\(|\\)"))
            .stream()
            .map(t -> (String)t)
            .collect(Collectors.toList());

        for(String cycle: cycles)
        {
            String[] cycleMembers = cycle.split("\\s");
            
            for(int i=0; i<cycleMembers.length; i++)
            {
                Long currentMember = Long.parseLong(cycleMembers[i]);
                
                if(this.permutationMap.containsKey(currentMember))
                    throw new GrexParseException("A permutation must be a bijection, but " + currentMember + " is mapped to multiple items");
                
                //Compute the target: the next item, of the first if the current item is the last in the cycle
                Long target = i == cycleMembers.length-1? Long.parseLong(cycleMembers[0]) : Long.parseLong(cycleMembers[i+1]);
                
                this.permutationMap.put(currentMember, target);
            }
        }
    }
    
    public String buildCycleNotation()
    {
        //Keep track of visited members
        Set<Long> visited = new HashSet<>();
        
        //Init a StringBuilder
        StringBuilder cycleNotationBuilder = new StringBuilder();
        
        //Get the keys in sorted notation
        List<Long> sortedKeys = this.permutationMap.keySet().stream().sorted().collect(Collectors.toList());
        
        //A list in which we store all elements of a cycle in order.
        List<Long> cycle = new ArrayList<>();
        
        for(Long key:sortedKeys)
        {
            //Was this key already involved in some cycle?
            if(visited.contains(key))
                continue;
            
            //Start a new cycle
            cycle.clear();
            
            Long next = append(key, cycle, visited);

            //Extend cycle
            while(!visited.contains(next))
            {
                next = append(next, cycle, visited);
            }
            
            //Convert the cycle to string representation and append it
            cycleNotationBuilder.append(cycle.stream().map(l->l.toString()).collect(Collectors.joining(" ", "(", ")")));
        }
        
        return cycleNotationBuilder.toString();
    }
    
    private Long append(Long key, List<Long> cycle, Set<Long> visited)
    {
        //Add to the cycle
        cycle.add(key);
        
        //Mark as visited
        visited.add(key);
        
        //Return the target of this key under the mapping
        return this.permutationMap.get(key);
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(OperatorFeature.TYPE.getName(), OperatorType.INTEGER_PERMUTATION.getName())
            .addFeature(OperatorFeature.SYMBOL.getName(), getSymbol())
            .addFeature(OperatorFeature.CYCLES.getName(), buildCycleNotation());
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.symbol);
        hash = 41 * hash + Objects.hashCode(this.permutationMap);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final IntegerPermutation other = (IntegerPermutation) obj;
        if (!Objects.equals(this.symbol, other.symbol))
        {
            return false;
        }
        if (!Objects.equals(this.permutationMap, other.permutationMap))
        {
            return false;
        }
        return true;
    }
    
    
}
