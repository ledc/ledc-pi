package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.function.BinaryOperator;

public enum BinaryStringOperators implements Operator<String>
{
    CONCAT("||", (s,t) -> s.concat(t));

    private final String symbol;
    private final BinaryOperator<String> operator;
    
    private BinaryStringOperators(String symbol, BinaryOperator<String> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.apply(arguments[0], arguments[1]);
    }
}
