package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Test;
import java.util.stream.Stream;

public interface IntegerTest extends Test<Long>
{
    @Override
    public default Boolean apply(String... arguments)
    {
        return test(Stream.of(arguments).mapToLong(arg -> Long.parseLong(arg)).toArray());
    }
            
    public boolean test(long ... arguments) throws GrexParseException;
}

