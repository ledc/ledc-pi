package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.Objects;
import java.util.function.BiPredicate;

public enum BinaryIntegerTests implements IntegerTest
{
    LOWER_THAN("<", (a,b) -> a < b),
    LOWER_THAN_EQUALS("<=", (a,b) -> a <= b),
    EQUALS("=", (a,b) -> Objects.equals(a, b)),
    NOT_EQUALS("!=", (a,b) -> !Objects.equals(a, b)),
    GREATER_THAN_EQUALS(">=", (a,b) -> a >= b),
    GREATER_THAN(">", (a,b) -> a > b);

    private final String symbol;
    private final BiPredicate<Long, Long> operator;
    
    private BinaryIntegerTests(String symbol, BiPredicate<Long, Long> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public boolean test(long... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.test(arguments[0], arguments[1]);
    }
}
