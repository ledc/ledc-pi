package be.ugent.ledc.pi.grex.operator;

public enum OperatorFeature
{
    TYPE("Type"),
    SYMBOL("Symbol"),
    MAP("Map"),
    CYCLES("Cycles"),
    OFFSET("Offset");
    
    private final String name;
    
    private OperatorFeature(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
