package be.ugent.ledc.pi.grex;

import java.util.Map;

public class Grex extends Grexable
{
    public static final char GROUP_REFERENCE = '@';
    
    private final String expression;

    public Grex(String expression)
    {
        this.expression = expression;
    }

    public String getExpression()
    {
        return expression;
    }
    
    public String resolveAsString(String pattern, String data) throws InterpretationException
    {
        return resolveAsString(pattern, data, false);
    }
    
    public String resolveAsString(String pattern, String data, boolean verbose) throws InterpretationException
    {
        try
        {
            Map<Integer, String> groupIndexMap = this.getGroupIndexMap(data, pattern);
            
            if(groupIndexMap == null)
            {
                if(verbose)
                    System.out.println("Group index map is null");
                return null;
            }
            
            return PostfixParser
                .getInstance()
                .resolve(groupIndexMap, expression, verbose);
        }
        catch (GrexParseException ex)
        {
            throw new InterpretationException(ex);
        }
    }
    
    public Long resolveAsInteger(String pattern, String data) throws InterpretationException
    {
        try
        {
            String s = resolveAsString(pattern, data);
            return s == null
                ? null
                : Long.valueOf(s);
        }
        catch (GrexParseException ex)
        {
            throw new InterpretationException(ex);
        }
    }
    
    public Double resolveAsDouble(String pattern, String data) throws InterpretationException
    {
        try
        {
            String s = resolveAsString(pattern, data);
            return s == null
                ? null
                : Double.valueOf(s);
        }
        catch (GrexParseException ex)
        {
            throw new InterpretationException(ex);
        }
    }
    
    public Boolean resolveAsBoolean(String pattern, String data) throws InterpretationException
    {
        try
        {
            String s = resolveAsString(pattern, data);
            return s == null
                ? null
                : Boolean.valueOf(s);
        }
        catch (GrexParseException ex)
        {
            throw new InterpretationException(ex);
        }
    }
       
    @Override
    public String toString()
    {
        return expression;
    }
}
