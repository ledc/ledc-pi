package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.time.LocalDateTime;
import java.util.function.Function;

public enum StringDateTimeExtraction implements Operator<String>
{
    DAY_OF_WEEK("dayofweek", (d) -> d.getDayOfWeek().toString()),
    MONTH("month", (d) -> d.getMonth().toString())
    ;

    private final String symbol;
    private final Function<LocalDateTime, String> operator;
    
    private StringDateTimeExtraction(String symbol, Function<LocalDateTime, String> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(LocalDateTime.parse(arguments[0]));
    }
}
    
