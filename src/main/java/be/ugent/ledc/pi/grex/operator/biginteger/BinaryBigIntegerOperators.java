package be.ugent.ledc.pi.grex.operator.biginteger;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.math.BigInteger;
import java.util.function.BinaryOperator;

public enum BinaryBigIntegerOperators implements BigIntegerOperator
{
    ADDITION("+", (a,b) -> a.add(b)),
    SUBTRACTION("-", (a,b) -> a.subtract(b)),
    MULTIPLICATION("*", (a,b) -> a.multiply(b)),
    DIVISION("/", (a,b) -> a.divide(b)),
    MODULO("%", (a,b) -> a.mod(b)),   
    POWER("^", (a,b) -> a.pow(b.intValue())),
    RIGHT_SHIFT(">>", (a,b) -> a.shiftRight(b.intValue())),
    LEFT_SHIFT("<<", (a,b) -> a.shiftLeft(b.intValue())),
    AND("&", (a,b) -> a.and(b)),
    OR("|", (a,b) -> a.or(b)),
    ;

    private final String symbol;
    private final BinaryOperator<BigInteger> operator;
    
    private BinaryBigIntegerOperators(String symbol, BinaryOperator<BigInteger> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public BigInteger apply(BigInteger... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.apply(arguments[0], arguments[1]);
    }
}