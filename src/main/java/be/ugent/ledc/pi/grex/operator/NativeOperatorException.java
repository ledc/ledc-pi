package be.ugent.ledc.pi.grex.operator;

public class NativeOperatorException extends RuntimeException
{
    public NativeOperatorException(String message)
    {
        super(message);
    }

    public NativeOperatorException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NativeOperatorException(Throwable cause)
    {
        super(cause);
    }

    public NativeOperatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
