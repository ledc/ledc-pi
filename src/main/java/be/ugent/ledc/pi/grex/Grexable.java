package be.ugent.ledc.pi.grex;

import java.util.Map;
import java.util.regex.Matcher;

public abstract class Grexable
{

    /**
     * A helper method that gets for some {@link java.lang.String} the substrings for each group indicated
     * in a regex pattern.
     * @param data
     * @param pattern
     * @return 
     */
    protected Map<Integer, String> getGroupIndexMap(String data, String pattern)
    {
        Matcher matcher = PatternManager
            .getInstance()
            .getMatcher(pattern, data);
        
        if(matcher.matches())
        {
            return PatternManager
                .getInstance()
                .getGroupIndexMap(matcher);
        }
        
        return null;
    }
}
