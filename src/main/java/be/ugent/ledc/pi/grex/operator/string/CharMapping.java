package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.OperatorFeature;
import be.ugent.ledc.pi.grex.operator.OperatorType;
import be.ugent.ledc.pi.grex.operator.PersistableOperator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class CharMapping implements PersistableOperator<String>
{   
    private final Map<Character, String> characterMap;
    
    private final String symbol;

    public CharMapping(Map<Character, String> characterMap, String symbol)
    {
        this.characterMap = characterMap;
        this.symbol = symbol;
    }
            
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    public final Map<Character, String> getCharacterMap()
    {
        return characterMap;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return arguments[0]
            .chars()
            .mapToObj(i -> (char)i).map(c -> characterMap.containsKey(c) ? characterMap.get(c) : c.toString())
            .collect(Collectors.joining(""));
    }    
    
    public static Map<Character, String> parse(String s)
    {
        Map<Character, String> cMap = new HashMap<>();
        
        String[] parts = s.split(",");
        
        for(String part: parts)
        {
            String[] item = part.split("=");
            
            if(item.length != 2)
                throw new GrexParseException("Could not parse character mapping " + s);
            
            if(item[0] == null || item[1] == null || item[0].trim().isEmpty() || item[1].trim().isEmpty())
                throw new GrexParseException("Could not parse character mapping " + s);
            
            cMap.put(item[0].charAt(0), item[1]);
        }
        
        return cMap;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(OperatorFeature.TYPE.getName(), OperatorType.CHAR_MAPPING.getName())
            .addFeature(OperatorFeature.SYMBOL.getName(), getSymbol())
            .addFeature(OperatorFeature.MAP.getName(), getMapAsString());
    }

    private String getMapAsString()
    {
        return this.characterMap
            .entrySet()
            .stream().map(e -> e.getKey().toString().concat("=").concat(e.getValue()))
            .collect(Collectors.joining(","));
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.characterMap);
        hash = 89 * hash + Objects.hashCode(this.symbol);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final CharMapping other = (CharMapping) obj;
        if (!Objects.equals(this.symbol, other.symbol))
        {
            return false;
        }
        if (!Objects.equals(this.characterMap, other.characterMap))
        {
            return false;
        }
        return true;
    }
    
    
}
