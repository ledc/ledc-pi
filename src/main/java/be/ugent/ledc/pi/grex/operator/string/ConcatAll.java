package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A variant of the concatenation operator for strings that works on all 
 * arguments currently on the stack.
 * @author abronsel
 */
public class ConcatAll implements Operator<String>
{
    @Override
    public String getSymbol()
    {
        return "concat";
    }

    @Override
    public int getNumberOfArguments()
    {
        return -1;
    }

    @Override
    public String apply(String... arguments) {
        return Stream
            .of(arguments)
            .collect(Collectors.joining("")
        );
    }
    
}
