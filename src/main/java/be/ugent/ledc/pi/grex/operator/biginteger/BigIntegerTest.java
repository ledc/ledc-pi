package be.ugent.ledc.pi.grex.operator.biginteger;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Test;
import java.math.BigInteger;
import java.util.stream.Stream;

public interface BigIntegerTest extends Test<BigInteger>
{
    @Override
    public default Boolean apply(String... arguments)
    {
        return test(Stream
            .of(arguments)
            .map(arg -> new BigInteger(arg))
            .toArray(BigInteger[]::new));
    }
            
    public boolean test(BigInteger ... arguments) throws GrexParseException;
}
