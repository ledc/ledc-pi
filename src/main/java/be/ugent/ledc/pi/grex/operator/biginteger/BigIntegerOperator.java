package be.ugent.ledc.pi.grex.operator.biginteger;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.math.BigInteger;
import java.util.stream.Stream;

public interface BigIntegerOperator extends Operator<BigInteger>
{
    @Override
    public default BigInteger apply(String... arguments)
    {
        return apply(Stream
            .of(arguments)
            .map(arg -> new BigInteger(arg))
            .toArray(BigInteger[]::new)
        );
    }
    
    public BigInteger apply(BigInteger ... arguments) throws GrexParseException;
}

