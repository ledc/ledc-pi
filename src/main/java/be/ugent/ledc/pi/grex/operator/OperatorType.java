package be.ugent.ledc.pi.grex.operator;

import be.ugent.ledc.core.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum OperatorType
{
    CAESAR_CODE("CaesarCode", OperatorFeature.TYPE, OperatorFeature.SYMBOL, OperatorFeature.OFFSET),
    CHAR_MAPPING("CharMap", OperatorFeature.TYPE, OperatorFeature.SYMBOL, OperatorFeature.MAP),
    STRING_MAPPING("StringMap", OperatorFeature.TYPE, OperatorFeature.SYMBOL, OperatorFeature.MAP),
    INTEGER_PERMUTATION("IntPerm", OperatorFeature.TYPE, OperatorFeature.SYMBOL, OperatorFeature.CYCLES);
    
    private final String name;
    
    private final Set<OperatorFeature> requiredFeatures;
    private final Set<OperatorFeature> optionalFeatures;
    
    private OperatorType(String name, Set<OperatorFeature> requiredFeatures, Set<OperatorFeature> optionalFeatures)
    {
        this.name = name;
        this.requiredFeatures = requiredFeatures;
        this.optionalFeatures = optionalFeatures;
    }
    
    private OperatorType(String name, OperatorFeature... requiredFeatures)
    {
        this(name, Stream.of(requiredFeatures).collect(Collectors.toSet()), new HashSet<>());
    }

    public String getName()
    {
        return name;
    }
    
    public static OperatorType parse(String name) throws ParseException
    {
        if(Stream.of(values()).noneMatch(pt -> pt.getName().equals(name)))
            throw new ParseException("Operator type '" + name + "' is unknown or not supported for manual configuration.");
        
        return Stream.of(values()).filter(pt -> pt.getName().equals(name)).findFirst().get();
    }

    public Set<OperatorFeature> getRequiredFeatures()
    {
        return requiredFeatures;
    }

    public Set<OperatorFeature> getOptionalFeatures()
    {
        return optionalFeatures;
    }    
}
