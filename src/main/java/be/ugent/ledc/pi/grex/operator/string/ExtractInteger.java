
package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractInteger implements Operator<String>
{

    @Override
    public String getSymbol()
    {
        return "exint";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException(
                getSymbol()
                    +   " requires "
                    + getNumberOfArguments()
                    + " arguments"
            );
        
        String s = arguments[0];
        
        Pattern p = Pattern.compile("^[^\\d\\-.,]*(-?\\d+)[^\\d\\-.,]*$");
        Matcher m = p.matcher(s);
        
        
        if(m.matches())
            return Long.valueOf(m.group(1)).toString();
        
        return "";
    }
    
}
