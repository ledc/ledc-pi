package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class DigitWeightedSum implements Operator<Long>
{
    public static final String RANGE_UP = "->";
    public static final String RANGE_DOWN = "<-";
    public static final String REPEAT = ",";
    
    @Override
    public String getSymbol()
    {
        return "w+";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public Long apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException(getSymbol() + " requires " + getNumberOfArguments() + " arguments");
        
        String number = arguments[0];
        
        if(number.chars().map(i -> (char)i).anyMatch(c -> (int)c <48 || (int)c >57))
            throw new GrexParseException("Cannot apply operator " + getSymbol() + " token " + number); 
        
        //Verify this first string argument is an integer number
        List<Long> digits = number
            .chars()
            .mapToObj(i -> (char)i)
            .map(c -> Long.parseLong(c.toString()))
            .collect(Collectors.toList());
        
        String schema = arguments[1];
        
        List<Long> weights = getWeights(schema, digits.size()).boxed().collect(Collectors.toList());
        
        if(weights.size() != digits.size())
            throw new GrexParseException("Error during evaluation of operator " + getSymbol() + ": number of digits differs from numbers of weights.");
        
        //Multiply each digits with its weight and then sum.
        return IntStream.range(0, weights.size()).mapToLong(i -> digits.get(i) * weights.get(i)).sum();
    }
    
    public static LongStream getWeights(String schema, int length)
    {   
        if(schema.equals("1" + RANGE_UP + "2^n"))
            return IntStream.rangeClosed(1, length).mapToLong(i -> (long)Math.pow(2, i));
        
        if(schema.equals("0" + RANGE_UP + "2^n"))
            return IntStream.range(0, length).mapToLong(i -> (long)Math.pow(2, i));

        if(schema.equals("2^n" + RANGE_DOWN + "1"))
            return IntStream.rangeClosed(1, length).map(i -> length - i + 1).mapToLong(i -> (long)Math.pow(2, i));
        
        if(schema.equals("2^n" + RANGE_DOWN + "0"))
            return IntStream.range(0, length).map(i -> length - i - 1).mapToLong(i -> (long)Math.pow(2, i));
        
        
        if(schema.matches("\\d+" + RANGE_UP + "n"))
        {
            int lBound = Integer.parseInt(schema.replaceAll(RANGE_UP + "n", ""));
            return LongStream.range(lBound, lBound + length);
        }
        
        if(schema.matches("\\d+" + RANGE_DOWN + "n"))
        {
            int lBound = Integer.parseInt(schema.replaceAll(RANGE_DOWN + "n", ""));
            return LongStream.range(0, length).map(i -> lBound - i);
        }
        
        if(schema.matches("n" + RANGE_UP + "\\d+"))
        {
            int rBound = Integer.parseInt(schema.replaceAll("n" + RANGE_UP, ""));
            return LongStream.range(rBound - length + 1, rBound+1);
        }
        
        if(schema.matches("n" + RANGE_DOWN + "\\d+"))
        {
            int rBound = Integer.parseInt(schema.replaceAll("n" + RANGE_DOWN, ""));
            return LongStream.range(0, length).map(i -> rBound + length - 1 - i);
        }
        
        List<Long> pattern = Stream.of(schema.split(","))
        .map(s -> s.trim())
        .filter(s -> !s.isEmpty())
        .mapToLong(s -> Long.parseLong(s))
        .boxed()
        .collect(Collectors.toList());

        return IntStream.range(0, length).mapToLong(i -> pattern.get(i % pattern.size()));

    }
    
}
