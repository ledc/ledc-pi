package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.UnaryOperator;
import java.util.stream.LongStream;

public enum UnaryIntegerOperators implements IntegerOperator
{
    ABSOLUTE_VALUE("abs", (a) -> Math.abs(a)),
    FACTORIAL("!", (a) -> LongStream.rangeClosed(1, a).reduce(1, (long x, long y) -> x * y)),
    NOT("~", (a) -> ~a);

    private final String symbol;
    private final UnaryOperator<Long> operator;
    
    private UnaryIntegerOperators(String symbol, UnaryOperator<Long> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public long apply(long... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(arguments[0]);
    }
}
