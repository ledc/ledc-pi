package be.ugent.ledc.pi.grex;

public class GrexException extends RuntimeException
{
    public GrexException(String message)
    {
        super(message);
    }

    public GrexException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public GrexException(Throwable cause)
    {
        super(cause);
    }
}
