package be.ugent.ledc.pi.grex.operator.datetime;

import be.ugent.ledc.pi.grex.operator.Operator;
import java.time.LocalDateTime;

public class Now implements Operator<LocalDateTime>
{
    @Override
    public String getSymbol()
    {
        return "now";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 0;
    }

    @Override
    public LocalDateTime apply(String... arguments)
    {
        return LocalDateTime.now();
    }
}
