package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.OperatorFeature;
import be.ugent.ledc.pi.grex.operator.OperatorType;
import be.ugent.ledc.pi.grex.operator.PersistableOperator;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public class StringMapping implements PersistableOperator<String>
{   
    private final LinkedHashMap<String, String> stringMap;
    
    private final String symbol;

    public StringMapping(LinkedHashMap<String, String> stringMap, String symbol)
    {
        this.stringMap = stringMap;
        this.symbol = symbol;
    }
            
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    public final LinkedHashMap<String, String> getCharacterMap()
    {
        return stringMap;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        String original = arguments[0];
        
        for(String in: stringMap.keySet())
        {
            original = original.replaceAll(in, stringMap.get(in));
        }
        
        return original;
    }    
    
    public static LinkedHashMap<String, String> parse(String s)
    {
        LinkedHashMap<String, String> sortedMap = new LinkedHashMap<>();
        
        String[] parts = s.split(",");
        
        for(String part: parts)
        {
            String[] item = part.split("=");
            
            if(item.length != 2)
                throw new GrexParseException("Could not parse character mapping " + s);
            
            if(item[0] == null || item[1] == null || item[0].trim().isEmpty() || item[1].trim().isEmpty())
                throw new GrexParseException("Could not parse character mapping " + s);
            
            sortedMap.put(item[0], item[1]);
        }
        
        return sortedMap;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(OperatorFeature.TYPE.getName(), OperatorType.CHAR_MAPPING.getName())
            .addFeature(OperatorFeature.SYMBOL.getName(), getSymbol())
            .addFeature(OperatorFeature.MAP.getName(), getMapAsString());
    }
    
    private String getMapAsString()
    {
        return this.stringMap
            .entrySet()
            .stream().map(e -> e.getKey().concat("=").concat(e.getValue()))
            .collect(Collectors.joining(","));
    }
}
