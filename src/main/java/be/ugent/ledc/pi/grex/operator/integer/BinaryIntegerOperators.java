package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.BinaryOperator;

public enum BinaryIntegerOperators implements IntegerOperator
{
    ADDITION("+", (a,b) -> a + b),
    SUBTRACTION("-", (a,b) -> a - b),
    MULTIPLICATION("*", (a,b) -> a * b),
    DIVISION("/", (a,b) -> a / b),
    MODULO("%", (a,b) -> a % b),   
    POWER("^", (a,b) -> (long)Math.pow(a,b)),
    RIGHT_SHIFT(">>", (a,b) -> a >> b),
    LEFT_SHIFT("<<", (a,b) -> a << b),
    AND("&", (a,b) -> a & b),
    OR("|", (a,b) -> a | b),
    ;

    private final String symbol;
    private final BinaryOperator<Long> operator;
    
    private BinaryIntegerOperators(String symbol, BinaryOperator<Long> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public long apply(long... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.apply(arguments[0], arguments[1]);
    }
}
