package be.ugent.ledc.pi.grex;

import be.ugent.ledc.core.LedcException;

public class InterpretationException extends LedcException
{
    public InterpretationException(String message)
    {
        super(message);
    }

    public InterpretationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InterpretationException(Throwable cause)
    {
        super(cause);
    }
}
