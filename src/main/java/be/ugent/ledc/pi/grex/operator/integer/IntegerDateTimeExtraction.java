package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.time.LocalDateTime;
import java.util.function.Function;

public enum IntegerDateTimeExtraction implements Operator<Long>
{
    YEAR("year", (d) -> (long)d.getYear()),
    MONTH("month", (d) -> (long)d.getMonthValue()),
    DAY_OF_MONTH("dayofmonth", (d) -> (long)d.getDayOfMonth()),
    DAY_OF_YEAR("dayofyear", (d) -> (long)d.getDayOfYear()),
    HOUR("hour", (d) -> (long)d.getHour()),
    MINUTE("minute", (d) -> (long)d.getMinute()),
    SECOND("second", (d) -> (long)d.getSecond());

    private final String symbol;
    private final Function<LocalDateTime, Long> operator;
    
    private IntegerDateTimeExtraction(String symbol, Function<LocalDateTime, Long> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public Long apply(String... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(LocalDateTime.parse(arguments[0]));
    }
    
}
