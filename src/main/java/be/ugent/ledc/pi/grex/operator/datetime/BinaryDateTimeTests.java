package be.ugent.ledc.pi.grex.operator.datetime;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Test;
import java.time.LocalDateTime;
import java.util.function.BiPredicate;

public enum BinaryDateTimeTests implements Test<LocalDateTime> 
{
    BEFORE("before", (a,b) -> a.isBefore(b)),
    AFTER("after", (a,b) -> a.isAfter(b));

    private final String symbol;
    private final BiPredicate<LocalDateTime, LocalDateTime> operator;
    
    private BinaryDateTimeTests(String symbol, BiPredicate<LocalDateTime, LocalDateTime> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public Boolean apply(String... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.test(LocalDateTime.parse(arguments[0]), LocalDateTime.parse(arguments[1]));
    }
}
