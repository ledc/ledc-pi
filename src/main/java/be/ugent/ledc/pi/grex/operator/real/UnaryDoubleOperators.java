package be.ugent.ledc.pi.grex.operator.real;

import be.ugent.ledc.pi.grex.operator.real.DoubleOperator;
import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.function.UnaryOperator;

public enum UnaryDoubleOperators implements DoubleOperator, Operator<Double>
{
    ABSOLUTE_VALUE("abs", (a) -> Math.abs(a)),
    SINE("sin", (a) -> Math.sin(a)),
    COSINE("cos", (a) -> Math.cos(a)),
    TANGENT("tan", (a) -> Math.tan(a)),
    EXPONTIAL("exp", (a) -> Math.exp(a)),
    LOGARITHM("log", (a) -> Math.log10(a)),
    NATURAL_LOGARITHM("ln", (a) -> Math.log(a)),
    SQRT("sqrt", (a) -> Math.sqrt(a))
    ;

    private final String symbol;
    private final UnaryOperator<Double> operator;
    
    private UnaryDoubleOperators(String symbol, UnaryOperator<Double> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public double apply(double... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(arguments[0]);
    }
}
