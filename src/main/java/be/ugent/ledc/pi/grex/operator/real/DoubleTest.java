package be.ugent.ledc.pi.grex.operator.real;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Test;
import java.util.stream.Stream;

public interface DoubleTest extends Test
{
    @Override
    public default Boolean apply(String... arguments)
    {
        return test(Stream.of(arguments).mapToDouble(arg -> Double.parseDouble(arg)).toArray());
    }
            
    public boolean test(double ... arguments) throws GrexParseException;
}
