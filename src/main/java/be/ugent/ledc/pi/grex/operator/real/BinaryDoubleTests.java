package be.ugent.ledc.pi.grex.operator.real;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.util.function.BiPredicate;

public enum BinaryDoubleTests implements DoubleTest
{
    LOWER_THAN("<", (a,b) -> a < b),
    LOWER_THAN_EQUALS("<=", (a,b) -> a <= b),
    EQUALS("=", (a,b) -> Math.abs(a-b) < Double.MIN_VALUE),
    NOT_EQUALS("!=", (a,b) -> Math.abs(a-b) >= Double.MIN_VALUE),
    GREATER_THAN_EQUALS(">=", (a,b) -> a >= b),
    GREATER_THAN(">", (a,b) -> a > b);

    private final String symbol;
    private final BiPredicate<Double, Double> operator;
    
    private BinaryDoubleTests(String symbol, BiPredicate<Double, Double> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public boolean test(double... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.test(arguments[0], arguments[1]);
    }
}
