package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.operator.integer.IntegerOperator;
import be.ugent.ledc.pi.grex.GrexParseException;

public class DihedralGroup5 implements IntegerOperator
{  
    private final int[][] cayleyTable = new int[][]{
        {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
        {1, 2, 3, 4, 0, 6, 7, 8, 9, 5},
        {2, 3, 4, 0, 1, 7, 8, 9, 5, 6},
        {3, 4, 0, 1, 2, 8, 9, 5, 6, 7},
        {4, 0, 1, 2, 3, 9, 5, 6, 7, 8},
        {5, 9, 8, 7, 6, 0, 4, 3, 2, 1},
        {6, 5, 9, 8, 7, 1, 0, 4, 3, 2},
        {7, 6, 5, 9, 8, 2, 1, 0, 4, 3},
        {8, 7, 6, 5, 9, 3, 2, 1, 0, 4},
        {9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
    };
    @Override
    public long apply(long... arguments) throws GrexParseException
    {
        if(arguments.length != 2)
            throw new GrexParseException(getSymbol() + " requires 2 arguments");
        
        int a = (int) arguments[0];
        int b = (int) arguments[1];
        
        if(a<0 || a>9 || b<0 || b>9)
            throw new GrexParseException(getSymbol() + " must be applied to digits between 0 and 9, bounds inclusive.");
        
        return this.cayleyTable[a][b];
    }

    @Override
    public String getSymbol()
    {
        return "D5";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }
    
}
