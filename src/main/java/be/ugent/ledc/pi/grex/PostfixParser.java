package be.ugent.ledc.pi.grex;

import be.ugent.ledc.pi.grex.operator.Between;
import be.ugent.ledc.pi.grex.operator.datetime.BinaryDateTimeTests;
import be.ugent.ledc.pi.grex.operator.OperatorException;
import be.ugent.ledc.pi.grex.operator.real.BinaryDoubleOperators;
import be.ugent.ledc.pi.grex.operator.real.BinaryDoubleTests;
import be.ugent.ledc.pi.grex.operator.integer.BinaryIntegerOperators;
import be.ugent.ledc.pi.grex.operator.integer.BinaryIntegerTests;
import be.ugent.ledc.pi.grex.operator.string.BinaryStringOperators;
import be.ugent.ledc.pi.grex.operator.string.BinaryStringTests;
import be.ugent.ledc.pi.grex.operator.string.CaesarCode;
import be.ugent.ledc.pi.grex.operator.string.Crc32;
import be.ugent.ledc.pi.grex.operator.string.CrockfordBase32Decode;
import be.ugent.ledc.pi.grex.operator.string.DateTimeTest;
import be.ugent.ledc.pi.grex.operator.integer.DigitWeightedSum;
import be.ugent.ledc.pi.grex.operator.integer.DihedralGroup5;
import be.ugent.ledc.pi.grex.operator.integer.IntegerDateTimeExtraction;
import be.ugent.ledc.pi.grex.operator.integer.IntegerPermutation;
import be.ugent.ledc.pi.grex.operator.string.Keep;
import be.ugent.ledc.pi.grex.operator.NativeOperatorException;
import be.ugent.ledc.pi.grex.operator.datetime.Now;
import be.ugent.ledc.pi.grex.operator.Operator;
import be.ugent.ledc.pi.grex.operator.datetime.Parse;
import be.ugent.ledc.pi.grex.operator.PersistableOperator;
import be.ugent.ledc.pi.grex.operator.string.StringDateTimeExtraction;
import be.ugent.ledc.pi.grex.operator.string.Substring;
import be.ugent.ledc.pi.grex.operator.Test;
import be.ugent.ledc.pi.grex.operator.biginteger.BinaryBigIntegerOperators;
import be.ugent.ledc.pi.grex.operator.biginteger.BinaryBigIntegerTests;
import be.ugent.ledc.pi.grex.operator.biginteger.UnaryBigIntegerOperators;
import be.ugent.ledc.pi.grex.operator.real.UnaryDoubleOperators;
import be.ugent.ledc.pi.grex.operator.integer.UnaryIntegerOperators;
import be.ugent.ledc.pi.grex.operator.string.ConcatAll;
import be.ugent.ledc.pi.grex.operator.string.ExtractDecimal;
import be.ugent.ledc.pi.grex.operator.string.ExtractInteger;
import be.ugent.ledc.pi.grex.operator.string.Padder;
import be.ugent.ledc.pi.grex.operator.string.Splitter;
import be.ugent.ledc.pi.grex.operator.string.UnaryStringOperators;
import be.ugent.ledc.pi.grex.operator.string.UriTest;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class PostfixParser
{
    private static final PostfixParser INSTANCE = new PostfixParser();
    
    public static final String QUOTE_SYMBOL    = "\"";
    
    //Boolean operators
    public static final String BOOLEAN_OR   = "or";
    public static final String BOOLEAN_AND  = "and";
    public static final String BOOLEAN_NOT  = "not";
    public static final String BOOLEAN_XOR  = "xor";
    
    //Special operators, independent of the scope
    public static final String MKVAR        = "mkvar";
    public static final String BRANCH_OR    = "branch|";
    public static final String BRANCH_AND   = "branch&";

    
    //Operators
    private final Map<String, Operator> integerOperators;
    private final Map<String, Operator> bigIntegerOperators;
    private final Map<String, Operator> doubleOperators;
    private final Map<String, Operator> stringOperators;
    private final Map<String, Operator> datetimeOperators;
    public final Map<String, Operator> booleanOperators;
    
    private final Set<PersistableOperator> persistableOperators;
    
    private PostfixParser()
    {
        //Init
        this.persistableOperators = new HashSet<>();
        this.integerOperators = new HashMap<>();
        this.bigIntegerOperators = new HashMap<>();
        this.doubleOperators = new HashMap<>();
        this.stringOperators = new HashMap<>();
        this.datetimeOperators = new HashMap<>();
        this.booleanOperators = new HashMap<>();
        
        //We start by adding logical operators
        this.booleanOperators.put(BOOLEAN_OR, new Operator<Boolean>()
        {
            @Override
            public String getSymbol() {return BOOLEAN_OR;}

            @Override
            public int getNumberOfArguments() {return 2;}

            @Override
            public Boolean apply(String... arguments)
            {
                return Boolean.parseBoolean(arguments[0])
                    || Boolean.parseBoolean(arguments[1]);
            }
        });
        
        this.booleanOperators.put(BOOLEAN_XOR, new Operator<Boolean>()
        {
            @Override
            public String getSymbol() {return BOOLEAN_XOR;}

            @Override
            public int getNumberOfArguments() {return 2;}

            @Override
            public Boolean apply(String... arguments)
            {
                return Boolean.logicalXor(
                    Boolean.parseBoolean(arguments[0]),
                    Boolean.parseBoolean(arguments[1]));
            }
        });
        
        this.booleanOperators.put(BOOLEAN_AND, new Operator<Boolean>()
        {
            @Override
            public String getSymbol() {return BOOLEAN_AND;}

            @Override
            public int getNumberOfArguments() {return 2;}

            @Override
            public Boolean apply(String... arguments)
            {
                return Boolean.parseBoolean(arguments[0])
                    && Boolean.parseBoolean(arguments[1]);
            }
        });
        
        this.booleanOperators.put(BOOLEAN_NOT, new Operator<Boolean>()
        {
            @Override
            public String getSymbol() {return BOOLEAN_NOT;}

            @Override
            public int getNumberOfArguments() {return 1;}

            @Override
            public Boolean apply(String... arguments)
            {
                return !Boolean.valueOf(arguments[0]);
            }
        });
        
        //Add Integer binary and unary operators 
        Stream.of(BinaryIntegerOperators.values()).forEach(this::addNativeIntegerOperator);
        Stream.of(UnaryIntegerOperators.values()).forEach(this::addNativeIntegerOperator);
        Stream.of(IntegerDateTimeExtraction.values()).forEach(this::addNativeIntegerOperator);
        
        //Add Integer tests
        Stream.of(BinaryIntegerTests.values()).forEach(this::addNativeIntegerTest);
        
        //Add BigInteger binary and unary operators 
        Stream.of(BinaryBigIntegerOperators.values()).forEach(this::addNativeBigIntegerOperator);
        Stream.of(UnaryBigIntegerOperators.values()).forEach(this::addNativeBigIntegerOperator);
        
        //Add BigInteger tests
        Stream.of(BinaryBigIntegerTests.values()).forEach(this::addNativeBigIntegerTest);

        //Add Double binary and unary operators 
        Stream.of(BinaryDoubleOperators.values()).forEach(this::addNativeDoubleOperator);
        Stream.of(UnaryDoubleOperators.values()).forEach(this::addNativeDoubleOperator);
        
        //Add Double tests
        Stream.of(BinaryDoubleTests.values()).forEach(this::addNativeDoubleTest);

        //Add String binary and unary operators 
        Stream.of(BinaryStringOperators.values()).forEach(this::addNativeStringOperator);
        Stream.of(UnaryStringOperators.values()).forEach(this::addNativeStringOperator);
        Stream.of(StringDateTimeExtraction.values()).forEach(this::addNativeStringOperator);
        
        addNativeStringOperator(new ExtractInteger());
        addNativeStringOperator(new ExtractDecimal(false));
        addNativeStringOperator(new ExtractDecimal(true));
        addNativeStringOperator(new Padder(true));
        addNativeStringOperator(new Padder(false));
        addNativeStringOperator(new Splitter());
        
        //Add String tests
        Stream.of(BinaryStringTests.values()).forEach(this::addNativeStringTest);

        addNativeIntegerOperator(new DihedralGroup5());
        addNativeIntegerOperator(new DigitWeightedSum());
        
        addNativeIntegerOperator(new IntegerPermutation("(0)(9)(3 6)(1 2 4 8 7 5)", "luhn"));
        addNativeIntegerOperator(new IntegerPermutation("(1 5 8 9 4 2 7 0)(3 6)", "verhoeff"));
        addNativeIntegerOperator(new IntegerPermutation("(1 5 8 9 4 2 7 0)(3 6)", "verhoeff" + IntegerPermutation.POWER_MARKER));
        
        addNativeIntegerTest(new Between<>(s-> Long.valueOf(s), true));
        addNativeIntegerTest(new Between<>(s-> Long.valueOf(s), false));
        
        addNativeDoubleTest(new Between<>(s-> Double.valueOf(s), true));
        addNativeDoubleTest(new Between<>(s-> Double.valueOf(s), false));
        
        addNativeStringOperator(new CaesarCode(0, "cc:a->0"));
        addNativeStringOperator(new CaesarCode(1, "cc:a->1"));
        addNativeStringOperator(new CaesarCode(10, "cc:a->10"));
        addNativeStringOperator(new Keep());
        addNativeStringOperator(new Substring());
        addNativeStringOperator(new Crc32());
        addNativeStringOperator(new CrockfordBase32Decode());
        addNativeStringOperator(new ConcatAll());
        
        addNativeStringTest(new UriTest());
        addNativeStringTest(new DateTimeTest());
        
        //Add datetime operators
        addNativeDatetimeOperator(new Now());
        addNativeDatetimeOperator(new Parse());
        
        //Add DateTime tests
        Stream.of(BinaryDateTimeTests.values()).forEach(this::addNativeDateTimeTest);
    }
    
    public final void addIntegerOperator(PersistableOperator<Long> o) throws OperatorException
    {
        if(persistableOperators.contains(o))
            return;
        
        checkOperatorName(o.getSymbol(), OperatorScope.INTEGER_SCOPE);
            
        integerOperators.put(o.getSymbol(), o);
        persistableOperators.add(o);
    }
    
    public final void addDoubleOperator(PersistableOperator<Double> o) throws OperatorException
    {
        if(persistableOperators.contains(o))
            return;
        
        checkOperatorName(o.getSymbol(), OperatorScope.DOUBLE_SCOPE);
            
        doubleOperators.put(o.getSymbol(), o);
        persistableOperators.add(o);
    }
    
    public final void addStringOperator(PersistableOperator<String> o) throws OperatorException
    {
        if(persistableOperators.contains(o))
            return;
        
        checkOperatorName(o.getSymbol(), OperatorScope.STRING_SCOPE);
            
        stringOperators.put(o.getSymbol(), (Operator)o);
        persistableOperators.add(o);
    }
    
    private void checkOperatorName(String symbol, OperatorScope scope) throws OperatorException
    {
        if(symbol == null || symbol.trim().isEmpty())
            throw new OperatorException("An operator symbol cannot by null or empty");
        
        if(symbol.startsWith(QUOTE_SYMBOL) || symbol.endsWith(QUOTE_SYMBOL))
            throw new OperatorException("An operator symbol cannot start or end with the quote symbol '" + QUOTE_SYMBOL + "'. Violation by '" + symbol + "'");
        
        if(symbol.contains(" "))
            throw new OperatorException("An operator symbol cannot contain a white space character. Violation by '" + symbol + "'");
        
        if(symbol.equals(BRANCH_AND) || symbol.equals(BRANCH_OR) || symbol.equals(MKVAR))
            throw new OperatorException("There is already an operator defined for symbol " + symbol);

        switch(scope)
        {
            case INTEGER_SCOPE:
                if(integerOperators.containsKey(symbol))
                    throw new OperatorException("There is already an Integer operator defined for symbol " + symbol);
                break;
            case DOUBLE_SCOPE:
                if(doubleOperators.containsKey(symbol))
                    throw new OperatorException("There is already a Double operator defined for symbol " + symbol);
                break;
            case STRING_SCOPE:
                if(stringOperators.containsKey(symbol))
                    throw new OperatorException("There is already a String operator defined for symbol " + symbol);
                break;
        }
    }
    
    private void addNativeIntegerOperator(Operator<Long> o)
    {
        try
        {
            checkOperatorName(o.getSymbol(), OperatorScope.INTEGER_SCOPE);
        }
        catch(OperatorException ex)
        {
            throw new NativeOperatorException(ex);
        }
        
        integerOperators.put(o.getSymbol(), o);
    }
    
    private void addNativeBigIntegerOperator(Operator<BigInteger> o)
    {
        try
        {
            checkOperatorName(o.getSymbol(), OperatorScope.BIGINTEGER_SCOPE);
        }
        catch(OperatorException ex)
        {
            throw new NativeOperatorException(ex);
        }
        
        bigIntegerOperators.put(o.getSymbol(), o);
    }
    
    private void addNativeDoubleOperator(Operator<Double> o)
    {
        try
        {
            checkOperatorName(o.getSymbol(), OperatorScope.DOUBLE_SCOPE);
        }
        catch(OperatorException ex)
        {
            throw new NativeOperatorException(ex);
        }
        doubleOperators.put(o.getSymbol(), o);
    }
    
    private void addNativeStringOperator(Operator<String> o) 
    {
        try
        {
            checkOperatorName(o.getSymbol(), OperatorScope.STRING_SCOPE);
        }
        catch(OperatorException ex)
        {
            throw new NativeOperatorException(ex);
        }
            
        stringOperators.put(o.getSymbol(), o);
    }
    
    private void addNativeDatetimeOperator(Operator<LocalDateTime> o) 
    {
        try
        {
            checkOperatorName(o.getSymbol(), OperatorScope.DATETIME_SCOPE);
        }
        catch(OperatorException ex)
        {
            throw new NativeOperatorException(ex);
        }
            
        datetimeOperators.put(o.getSymbol(), o);
    }
    
    private void addNativeIntegerTest(Test<Long> test) 
    {
        if(integerOperators.containsKey(test.getSymbol()) || test.getSymbol().equals(BRANCH_AND) || test.getSymbol().equals(BRANCH_OR))
            throw new NativeOperatorException("There is already an Integer test defined for symbol " + test.getSymbol());
            
        integerOperators.put(test.getSymbol(), test);
    }
    
    private void addNativeBigIntegerTest(Test<BigInteger> test) 
    {
        if(bigIntegerOperators.containsKey(test.getSymbol()) || test.getSymbol().equals(BRANCH_AND) || test.getSymbol().equals(BRANCH_OR))
            throw new NativeOperatorException("There is already a BigInteger test defined for symbol " + test.getSymbol());
            
        bigIntegerOperators.put(test.getSymbol(), test);
    }
    
    private void addNativeDoubleTest(Test<Double> test) 
    {
        if(doubleOperators.containsKey(test.getSymbol()) || test.getSymbol().equals(BRANCH_AND) || test.getSymbol().equals(BRANCH_OR))
            throw new NativeOperatorException("There is already a Double test defined for symbol " + test.getSymbol());
            
        doubleOperators.put(test.getSymbol(), test);
    }
    
    private void addNativeStringTest(Test<String> test) 
    {
        if(stringOperators.containsKey(test.getSymbol()) || test.getSymbol().equals(BRANCH_AND) || test.getSymbol().equals(BRANCH_OR))
            throw new NativeOperatorException("There is already a String test defined for symbol " + test.getSymbol());
            
        stringOperators.put(test.getSymbol(), test);
    }
    
    private void addNativeDateTimeTest(Test<LocalDateTime> test) 
    {
        if(datetimeOperators.containsKey(test.getSymbol()) || test.getSymbol().equals(BRANCH_AND) || test.getSymbol().equals(BRANCH_OR))
            throw new NativeOperatorException("There is already an DateTime test defined for symbol " + test.getSymbol());
            
        datetimeOperators.put(test.getSymbol(), test);
    }

    private boolean isOperator(OperatorScope operatorScope, String token)
    {
        if(token.equals(BRANCH_AND) || token.equals(BRANCH_OR) || token.equals(MKVAR))
            return true;
        
        switch(operatorScope)
        {
            case INTEGER_SCOPE:
                return integerOperators.containsKey(token);
            case DOUBLE_SCOPE:
                return doubleOperators.containsKey(token);
            case STRING_SCOPE:
                return stringOperators.containsKey(token);
            case DATETIME_SCOPE:
                return datetimeOperators.containsKey(token);
            case BIGINTEGER_SCOPE:
                return bigIntegerOperators.containsKey(token);
            case BOOLEAN_SCOPE:
                return booleanOperators.containsKey(token);
            default:
                throw new GrexParseException("Unknown operator scope defined!");
        }
    }
    
    private Operator getOperator(OperatorScope operatorScope, String token)
    {
        if(!isOperator(operatorScope, token))
            return null;
        
        switch(operatorScope)
        {
            case INTEGER_SCOPE:
                return integerOperators.get(token);
            case DOUBLE_SCOPE:
                return doubleOperators.get(token);
            case STRING_SCOPE:
                return stringOperators.get(token);
            case DATETIME_SCOPE:
                return datetimeOperators.get(token);
            case BIGINTEGER_SCOPE:
                return bigIntegerOperators.get(token);
            case BOOLEAN_SCOPE:
                return booleanOperators.get(token);
            default:
                throw new GrexParseException("Unknown operator scope defined!");
        }
    }
    
    public static PostfixParser getInstance()
    {
        return INSTANCE;
    }

    public Set<PersistableOperator> getPersistableOperators()
    {
        return persistableOperators;
    }
         
    /**
     * Resolution of a formula with no variables.
     * @param formula
     * @return
     * @throws GrexParseException 
     */
    public String resolve(String formula) throws GrexParseException
    {
        return resolve(new HashMap<>(), formula, false);
    }
    
    public String resolve(Map<Integer, String> groupIndexMap, String formula) throws GrexParseException
    {
        return resolve(groupIndexMap, formula, false);
    }
    
    /**
     * Resolution of a formula with indexed variables.
     * @param groupIndexMap The map of variable indices to values that is used
     * during resolution.
     * @param formula The grex formula that is being resolved
     * @param verbose Verbose output
     * @return
     * @throws GrexParseException 
     */
    public String resolve(Map<Integer, String> groupIndexMap, String formula, boolean verbose) throws GrexParseException
    {
        if(verbose)
        {
            System.out.println("-------");
            System.out.println("Formula: " + formula);
            System.out.println("Index map: " + groupIndexMap);
        }
        //Initialize stack
        Stack<String> stack = new Stack<>();
        
        //Set current scope
        OperatorScope currentScope = OperatorScope.STRING_SCOPE;
        
        //Tokenize the formula
        String[] tokens = formula.split(" (?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");
        
        for(int i=0; i<tokens.length; i++)
        {
            //Get the next token
            String token = tokens[i].trim();
            
            //Is the token a variable or sequence of variables
            if(token.matches("(" + Grex.GROUP_REFERENCE+"\\d+)+"))
            {
                stack.push(dereference(token, groupIndexMap));
                continue;
            }
            
            //If the token is quoted, we treat it as a literal
            if(token.startsWith(QUOTE_SYMBOL) && token.endsWith(QUOTE_SYMBOL))
            {
                stack.push(token.substring(1, token.length()-1));
                continue;
            }
            
            //If the token declares a new operator scope, change it
            if(OperatorScope.isScopeName(token))
            {
                currentScope = OperatorScope.parseByScopeName(token);
                continue;
            }
            
            //Is the token an operator?
            if(isOperator(currentScope, token))
            {   
                if(token.equals(BRANCH_AND) || token.equals(BRANCH_OR))
                {
                    if(i == tokens.length - 1)
                        return Boolean.FALSE.toString();
                    
                    String[] remainingTokens = Arrays.copyOfRange(tokens, i + 1, tokens.length);
                    
                    return Boolean.toString(branch(currentScope, remainingTokens, token, stack));
                }
                else if(token.equals(MKVAR))
                {
                    if(stack.isEmpty())
                    {
                        throw new GrexParseException(
                            "Invalid formula: insufficient operands for operator "
                            + MKVAR
                            + ". Token number "
                            + i
                            + ".");
                    }
                    else
                    {
                        groupIndexMap.put(
                            groupIndexMap.size(),
                            stack.pop()
                        );
                    }                    
                }
                else
                {
                    //Get the operator
                    Operator o = getOperator(currentScope, token);

                    //Number of arguments to be taken from the stack
                    int numArgs = o.getNumberOfArguments() == -1
                        ? stack.size()
                        : o.getNumberOfArguments();

                    //Does the stack contain sufficient operands?
                    if(stack.size() < numArgs)
                    {
                        throw new GrexParseException(
                            "Invalid formula: insufficient operands for operator "
                            + o.getSymbol()
                            + ". Token number "
                            + i
                            + ".");
                    }

                    //Make a list of operands
                    String[] operands = new String[numArgs];

                    //Pop as many symbols from the stack as the operator requires
                    for(int j=0; j<numArgs; j++)
                    {
                        operands[numArgs - j - 1] = stack.pop();
                    }

                    //Apply the operator and put the result on the stack
                    stack.push(o.apply(operands).toString());                    
                }
            }
            else
            {
                stack.push(token);
            }
            
            if(verbose)
            {
                System.out.println("Token: " + token + " Stack: " + stack);
            }
        }
        
        if(stack.size() == 1)
        {
            return stack.pop();
        }
        else if(stack.size() > 1)
        {
            throw new GrexParseException(
                "Invalid formula. "
                + "Processed all tokens but stack contains multiple values.");
        }
        else
        {
            throw new GrexParseException(
                "Invalid formula. "
                + "Processed all tokens but stack is empty.");
        }
    }
    
    private boolean branch(OperatorScope currentScope, String[] branchedTokens, String branchOperator, Stack<String> stack)
    {
        Stack<String> localStack = new Stack<>();
        
        localStack.addAll(stack);
        
        for(String next: branchedTokens)
        {            
            //If the token is quoted, we treat it as a literal
            if(next.startsWith(QUOTE_SYMBOL) && next.endsWith(QUOTE_SYMBOL))
            {
                localStack.push(next);
                continue;
            }
            
            //Check for operator scope
            if(OperatorScope.isScopeName(next))
            {
                throw new GrexParseException(
                    "Switching operator scope is not permitted during branching..."
                );
            }
            
            //Is the token an operator?
            if(isOperator(currentScope, next))
            {   
                if(next.equals(BRANCH_OR) || next.equals(BRANCH_AND))
                    throw new GrexParseException("Branching can be used only once...");
                
                //Get the operator
                Operator o = getOperator(currentScope, next);
                
                if(localStack.size() < o.getNumberOfArguments())
                    throw new GrexParseException(
                        "Invalid formula: insufficient operands for operator " + o.getSymbol()
                    );
                
                //Make a list of operands
                String[] operands = new String[o.getNumberOfArguments()];
                
                //Pop as many symbols from the stack as the operator requires
                for(int j=0; j<o.getNumberOfArguments(); j++)
                {
                    operands[o.getNumberOfArguments() - j - 1] = localStack.pop();
                }
                
                if(o instanceof Test)
                {
                    Test<?> t = (Test)o;

                    //Do the branch test 
                    Boolean branchTest = t.apply(operands);
                
                    if(branchOperator.equals(BRANCH_OR) && branchTest)
                        return true;

                    if(branchOperator.equals(BRANCH_AND) && !branchTest)
                        return false;
                
                    //Reset the local stack for the next branch
                    localStack.clear();
                    localStack.addAll(stack);
                }
                else
                    localStack.push(o.apply(operands).toString());
            }
            else
            {
                localStack.push(next);
            }
        }
        
        //If we could not early discard, we return true in case of branch and and false otherwise.
        return branchOperator.equals(BRANCH_AND);
    }

    private String dereference(String token, Map<Integer, String> groupIndexMap)
    {
        Pattern p = Pattern.compile(Grex.GROUP_REFERENCE +"(\\d+)");
        
        Matcher m = p.matcher(token);
        
        String result = "";
        
        while(m.find())
        {
            result += groupIndexMap.get(Integer.valueOf(m.group(1)));
        }
        
        return result;
    }
}
