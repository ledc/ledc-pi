package be.ugent.ledc.pi.grex;

public class GrexParseException extends RuntimeException
{
    public GrexParseException(String message)
    {
        super(message);
    }

    public GrexParseException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public GrexParseException(Throwable cause)
    {
        super(cause);
    }
}
