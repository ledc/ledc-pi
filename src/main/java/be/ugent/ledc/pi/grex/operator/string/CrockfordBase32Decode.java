package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;

public class CrockfordBase32Decode implements Operator<String>
{
    private static final char[] CHARACTER_TABLE =
    {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', '*', '~', '$', '=', 'U',
    };
    
    @Override
    public String apply(String... arguments) throws GrexParseException
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");

        return decode(arguments[0]);
    }

    @Override
    public String getSymbol()
    {
        return "crockford32decode";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }
    
    private String decode(String string) throws GrexParseException
    {
        if (!string.matches("^[0-9a-zA-Z\\-]+$"))
        {
            throw new GrexParseException("Invalid symbol detected");
        }

        String processedString = string.replaceAll("-", "");

        long result = 0;

        //go through the characters
        for (int i = 0; i < processedString.length(); i++)
        {            
            result = result * 32 + decode(processedString.charAt(i));
        }

        return Long.toString(result);
    }

    private long decode(char digit)
    {
        switch (digit)
        {
            case '0':
            case 'O':
            case 'o':
                return 0;

            case '1':
            case 'I':
            case 'i':
            case 'L':
            case 'l':
                return 1;

            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;

            case 'A':
            case 'a':
                return 10;

            case 'B':
            case 'b':
                return 11;

            case 'C':
            case 'c':
                return 12;

            case 'D':
            case 'd':
                return 13;

            case 'E':
            case 'e':
                return 14;

            case 'F':
            case 'f':
                return 15;

            case 'G':
            case 'g':
                return 16;

            case 'H':
            case 'h':
                return 17;

            case 'J':
            case 'j':
                return 18;

            case 'K':
            case 'k':
                return 19;

            case 'M':
            case 'm':
                return 20;

            case 'N':
            case 'n':
                return 21;

            case 'P':
            case 'p':
                return 22;

            case 'Q':
            case 'q':
                return 23;

            case 'R':
            case 'r':
                return 24;

            case 'S':
            case 's':
                return 25;

            case 'T':
            case 't':
                return 26;
            case 'U':
            case 'u':
            case 'V':
            case 'v':
                return 27;

            case 'W':
            case 'w':
                return 28;

            case 'X':
            case 'x':
                return 29;

            case 'Y':
            case 'y':
                return 30;

            case 'Z':
            case 'z':
                return 31;

            default:
                return -1;
        }
    }
}
    
