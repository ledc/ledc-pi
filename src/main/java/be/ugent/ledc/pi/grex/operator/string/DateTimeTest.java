package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateTimeTest implements StringTest
{
    @Override
    public String getSymbol()
    {
        return "isdatetime";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public Boolean apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("Operator " + getSymbol() + " requires " + getNumberOfArguments() + " arguments");
        
        String s = arguments[0];
        String pattern = arguments[1];
        
        try
        {
            //Try to match the pattern
            DateTimeFormatter.ofPattern(pattern).parse(s);
        }
        catch (DateTimeParseException ex)
        {
            //If an expection occurs, the test fails
            return false;
        }

        //Else, valid
        return true;
    }
}
