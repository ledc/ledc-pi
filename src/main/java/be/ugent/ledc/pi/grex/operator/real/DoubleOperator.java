package be.ugent.ledc.pi.grex.operator.real;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.stream.Stream;

public interface DoubleOperator extends Operator<Double>
{
    @Override
    public default Double apply(String... arguments)
    {
        return apply(Stream.of(arguments).mapToDouble(arg -> Double.parseDouble(arg)).toArray());
    }
            
    public double apply(double ... arguments) throws GrexParseException;
}
