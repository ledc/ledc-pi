package be.ugent.ledc.pi.grex.operator;

public interface Operator<T>
{    
    public String getSymbol();
    
    public int getNumberOfArguments();
    
    public T apply(String ... arguments);
}
