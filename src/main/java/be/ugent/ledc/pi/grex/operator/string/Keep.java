package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Keep implements Operator<String>
{
    public static final String DIGITS            = "digits";
    public static final String LATIN_DIGITS      = "0..9";
    public static final String ALPHANUMERIC      = "alphanum";
    public static final String LETTERS           = "letters";
    public static final String LETTERS_AND_SPACE = "letters+spacing";
    public static final String ALPHANUMERIC_SPACE= "alphanum+spacing";
    
    @Override
    public String getSymbol()
    {
        return "keep";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException(getSymbol() + " requires 2 arguments");
        
        Stream<Character> charStream = arguments[0].chars().mapToObj(i-> (char) i);
        String mode = arguments[1];
        
        switch(mode)
        {
            case LETTERS:
                return charStream.filter(c -> Character.isLetter(c)).map(c -> c.toString()).collect(Collectors.joining(""));
            case LETTERS_AND_SPACE:
                return charStream.filter(c -> Character.isLetter(c) || Character.isWhitespace(c)).map(c -> c.toString()).collect(Collectors.joining(""));
            case DIGITS:
                return charStream.filter(c -> Character.isDigit(c)).map(c -> c.toString()).collect(Collectors.joining(""));
            case LATIN_DIGITS:
                return charStream.filter(c -> 48 <= (int)c && (int)c <= 57).map(c -> c.toString()).collect(Collectors.joining(""));
            case ALPHANUMERIC:
                return charStream
                    .filter(
                        c -> 
                            Character.isLetter(c)
                        ||  Character.isDigit(c))
                    .map(c -> c.toString()).collect(Collectors.joining(""));
            case ALPHANUMERIC_SPACE:
                return charStream
                    .filter(
                        c -> 
                            Character.isLetter(c)
                        ||  Character.isDigit(c)
                        ||  Character.isWhitespace(c)
                    )
                    .map(c -> c.toString()).collect(Collectors.joining(""));
        }
        
        return arguments[0];
    }
    
}
