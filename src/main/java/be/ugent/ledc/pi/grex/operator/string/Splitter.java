package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A splitter splits a given strings into substrings of fixed length according
 * to a split pattern. The substrings are separated by a fixed symbol.
 * 
 * Example: 7894569865 3,5,2 - split results in 789-45698-65
 * @author abronsel
 */
public class Splitter implements Operator<String>{

    @Override
    public String getSymbol()
    {
        return "split";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 3;
    }

    @Override
    public String apply(String... arguments)
    {
        String s = arguments[0];
        String pattern = arguments[1];
        String separator = arguments[2];
        
        //Turn pattern into substring length
        String[] subs = pattern.split(",");
        
        if(Stream.of(subs).anyMatch(sub -> !sub.matches("\\d+")))
        {
            throw new GrexParseException("Error: cannot parse split pattern " + pattern);
        }
        
        //Get lengths
        int[] sizes = Stream
            .of(subs)
            .mapToInt(Integer::parseInt)
            .toArray();
        
        //Check sum
        if(IntStream.of(sizes).sum() != s.length())
            throw new GrexParseException("Error: cannot apply split pattern " + pattern + " to string " + s + ". Cause: sum of split lengths does not equal total length.");

        String[] parts = new String[sizes.length];
        
        int offset = 0;
        for(int i=0;i<sizes.length;i++)
        {
            parts[i] = s.substring(offset, offset + sizes[i]);
            offset += sizes[i];
        }
        
        return Stream
            .of(parts)
            .collect(Collectors.joining(separator));
        
    }
    
}
