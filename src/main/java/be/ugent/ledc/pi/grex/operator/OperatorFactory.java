package be.ugent.ledc.pi.grex.operator;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.pi.grex.operator.string.CaesarCode;
import be.ugent.ledc.pi.grex.operator.string.CharMapping;
import be.ugent.ledc.pi.grex.operator.string.StringMapping;
import be.ugent.ledc.pi.grex.operator.integer.IntegerPermutation;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.PostfixParser;

public class OperatorFactory
{
    public static void registerOperator(FeatureMap featureMap) throws ParseException
    {
        if(featureMap.getString(OperatorFeature.TYPE.getName()) == null)
            throw new ParseException("Cannot determine the type of Operator. Operators must include a " + OperatorFeature.TYPE.getName() + ".");
        
        //Get the operator type
        OperatorType operatorType = OperatorType.parse(featureMap.getString(OperatorFeature.TYPE.getName()));
        
        if(operatorType.getRequiredFeatures()
            .stream()
            .anyMatch(feature -> !featureMap.getFeatures().containsKey(feature.getName()))
        )
            throw new ParseException("Could not reconstruct operator type " + operatorType.getName() + ". Required fields are missing...");
        
        try
        {
            switch(operatorType)
            {
                case CAESAR_CODE:
                    CaesarCode op1 = new CaesarCode(
                        featureMap.getInteger(OperatorFeature.OFFSET.getName()),
                        featureMap.getString(OperatorFeature.SYMBOL.getName())
                    );
                    PostfixParser.getInstance().addStringOperator(op1);

                    break;
                
                case INTEGER_PERMUTATION:
                    IntegerPermutation op2 = new IntegerPermutation(
                        featureMap.getString(OperatorFeature.CYCLES.getName()),
                        featureMap.getString(OperatorFeature.SYMBOL.getName())
                    );
                    PostfixParser.getInstance().addIntegerOperator(op2);

                    break;
                
                case CHAR_MAPPING:
                    CharMapping op3 = new CharMapping(
                        CharMapping.parse(featureMap.getString(OperatorFeature.MAP.getName())),
                        featureMap.getString(OperatorFeature.SYMBOL.getName()));
                    PostfixParser.getInstance().addStringOperator(op3);

                    break;
                case STRING_MAPPING:
                    StringMapping op4 = new StringMapping(
                        StringMapping.parse(featureMap.getString(OperatorFeature.MAP.getName())),
                        featureMap.getString(OperatorFeature.SYMBOL.getName()));
                    PostfixParser.getInstance().addStringOperator(op4);

                    break;
            }
        }
        catch(OperatorException ex)
        {
            throw new ParseException(ex);
        }
    }
    
}
