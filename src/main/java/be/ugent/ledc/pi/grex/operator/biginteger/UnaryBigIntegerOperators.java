package be.ugent.ledc.pi.grex.operator.biginteger;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.math.BigInteger;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;

public enum UnaryBigIntegerOperators implements BigIntegerOperator
{
    ABSOLUTE_VALUE("abs", (a) -> a.abs()),
    FACTORIAL("!", (a) -> IntStream
        .rangeClosed(1, a.intValue())
        .mapToObj(i -> BigInteger.valueOf(i))
        .reduce(BigInteger.ONE, (x, y) -> x.multiply(y))),
    NOT("~", (a) -> a.not());

    private final String symbol;
    private final UnaryOperator<BigInteger> operator;
    
    private UnaryBigIntegerOperators(String symbol, UnaryOperator<BigInteger> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }

    @Override
    public BigInteger apply(BigInteger... arguments)
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        return operator.apply(arguments[0]);
    }
}
