package be.ugent.ledc.pi.grex.operator.integer;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.stream.Stream;

public interface IntegerOperator extends Operator<Long>
{
    @Override
    public default Long apply(String... arguments)
    {
        return apply(Stream.of(arguments).mapToLong(arg -> Long.parseLong(arg)).toArray());
    }
    
    public long apply(long ... arguments) throws GrexParseException;
}
