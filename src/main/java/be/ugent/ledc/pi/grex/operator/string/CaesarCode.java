package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.operator.OperatorFeature;
import be.ugent.ledc.pi.grex.operator.OperatorType;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class CaesarCode extends CharMapping
{
    private final long offset;
    
    public CaesarCode(long offset, String symbol)
    {
        super(new HashMap<Character, String>(), symbol);
     
        this.offset = offset;
        getCharacterMap().putAll(LongStream.rangeClosed(0, 25).boxed()
            .collect(
                Collectors.toMap(
                    i -> (char)(i + 65),
                    i -> Long.toString(i + offset)
            )));
        
        getCharacterMap().putAll(LongStream.rangeClosed(0, 25).boxed()
            .collect(
                Collectors.toMap(
                    i -> (char)(i + 97),
                    i -> Long.toString(i + offset)
            )));
    }
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(OperatorFeature.TYPE.getName(), OperatorType.CHAR_MAPPING.getName())
            .addFeature(OperatorFeature.SYMBOL.getName(), getSymbol())
            .addFeature(OperatorFeature.OFFSET.getName(), offset);
    }
}
