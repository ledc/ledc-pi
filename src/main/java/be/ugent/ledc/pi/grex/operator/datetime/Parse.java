package be.ugent.ledc.pi.grex.operator.datetime;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Parse implements Operator<LocalDateTime>
{
    @Override
    public String getSymbol()
    {
        return "parse";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public LocalDateTime apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException("Operator " + getSymbol() + " requires " + getNumberOfArguments() + " arguments");
        
        String s = arguments[0];
        String pattern = arguments[1];
        
        try
        {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
                    
            return LocalDateTime.parse(s, formatter);
        }
        catch (DateTimeParseException ex)
        {
            System.err.println(ex);
            //If an expection occurs, the test fails
            throw new GrexParseException(ex);
        }
    }
}
