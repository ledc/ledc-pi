package be.ugent.ledc.pi.grex.operator;

import be.ugent.ledc.core.config.Persistable;

public interface PersistableOperator<T> extends Operator<T>, Persistable
{
    
}
