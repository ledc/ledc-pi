package be.ugent.ledc.pi.grex;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RegexUtility
{
    public static final String DASH = "[\\u002D\\u2010\\u2012\\u2013\\u2014\\u2015]";
    
    public static String getPattern(List<String> options)
    {
        options.sort(Collections.reverseOrder());
        
        return options.stream().collect(Collectors.joining("|"));
    }
}
