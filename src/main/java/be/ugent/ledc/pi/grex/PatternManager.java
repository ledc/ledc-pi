package be.ugent.ledc.pi.grex;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternManager
{
    private static final PatternManager INSTANCE = new PatternManager();
    
    private final Map<String, Pattern> patternRegistry;
    
    private PatternManager()
    {
        this.patternRegistry = new HashMap<>();
    }
    
    public synchronized static PatternManager getInstance()
    {
        return INSTANCE;
    }
    
    public boolean registerPattern(String pattern, int flags)
    {
        return registerPattern(pattern, flags, false);
    }
    
    public boolean registerPattern(String pattern, int flags, boolean overwrite)
    {
        //Is the pattern not yet registered?
        if(!patternRegistry.containsKey(pattern) || overwrite)
        {
            //Register the pattern
            patternRegistry.put(pattern, Pattern.compile(pattern, flags));
            
            //Signal new pattern registration
            return true;
        }
        
        //Signal pattern was already registered
        return false;
    }
    
    public synchronized boolean registerPattern(String pattern)
    {
        return registerPattern(pattern, 0);
    }
    
    public Pattern getPattern(String pattern)
    {
        return patternRegistry.get(pattern);
    }
    
    public synchronized Matcher getMatcher(String pattern, String value)
    {
        if(patternRegistry.get(pattern) == null)
        {
            registerPattern(pattern);
        }
        return patternRegistry
            .get(pattern)
            .matcher(value);
    }
    
    public Map<Integer, String> getGroupIndexMap(Matcher matcher)
    {
        //Initialize
        Map<Integer, String> groupIndexMap = new HashMap<>();
        
        for(int index=0; index <= matcher.groupCount(); index++)
        {
            //Link the group index to the group instance
            String group = matcher.group(index);
            groupIndexMap.put(index, group == null ? "" : group);
        }
        
        //Return
        return groupIndexMap;
    }
}
