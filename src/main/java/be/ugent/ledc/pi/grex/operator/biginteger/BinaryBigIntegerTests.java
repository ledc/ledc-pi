package be.ugent.ledc.pi.grex.operator.biginteger;

import be.ugent.ledc.pi.grex.GrexParseException;
import java.math.BigInteger;
import java.util.Objects;
import java.util.function.BiPredicate;

public enum BinaryBigIntegerTests implements BigIntegerTest
{
    LOWER_THAN("<", (a,b) -> a.compareTo(b) < 0),
    LOWER_THAN_EQUALS("<=", (a,b) -> a.compareTo(b) <= 0),
    EQUALS("=", (a,b) -> Objects.equals(a, b)),
    NOT_EQUALS("!=", (a,b) -> !Objects.equals(a, b)),
    GREATER_THAN_EQUALS(">=", (a,b) -> a.compareTo(b) >= 0),
    GREATER_THAN(">", (a,b) -> a.compareTo(b) > 0);

    private final String symbol;
    private final BiPredicate<BigInteger, BigInteger> operator;
    
    private BinaryBigIntegerTests(String symbol, BiPredicate<BigInteger, BigInteger> operator)
    {
        this.symbol = symbol;
        this.operator = operator;
    }
    
    @Override
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public int getNumberOfArguments()
    {
        return 2;
    }

    @Override
    public boolean test(BigInteger... arguments)
    {
        if(arguments.length != 2)
            throw new GrexParseException("A BinaryOperator requires 2 arguments");
        
        return operator.test(arguments[0], arguments[1]);
    }
}
