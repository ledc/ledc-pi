package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;
import java.util.zip.CRC32;

public class Crc32 implements Operator<String>
{

    @Override
    public String apply(String... arguments) throws GrexParseException
    {
        if(arguments.length != 1)
            throw new GrexParseException("A UnaryOperator requires 1 argument");
        
        //Get the input value
        String input = arguments[0];        
        
        //Create the native CRC32 implementation
        CRC32 checksum = new CRC32();
        
        //Compute the checksum
        checksum.update(input.getBytes());
        
        return Long.toString(checksum.getValue());
    }

    @Override
    public String getSymbol()
    {
        return "crc32";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 1;
    }
    
}
