package be.ugent.ledc.pi.grex;

import java.util.stream.Stream;

public enum OperatorScope
{
    INTEGER_SCOPE("::int"),
    BIGINTEGER_SCOPE("::bigint"),
    BOOLEAN_SCOPE("::bool"),
    DOUBLE_SCOPE("::double"),
    STRING_SCOPE("::string"),
    DATETIME_SCOPE("::datetime");

    private final String scopeName;

    private OperatorScope(String scopeName)
    {
        this.scopeName = scopeName;
    }

    public String getScopeName()
    {
        return scopeName;
    }
    
    public static boolean isScopeName(String token)
    {
        return Stream
            .of(values())
            .map(os -> os
                .getScopeName())
                .anyMatch((sn) -> sn.equals(token));
    }
    
    public static OperatorScope parseByScopeName(String scopeName)
    {
        if(scopeName.equals(INTEGER_SCOPE.getScopeName()))
            return INTEGER_SCOPE;
        if(scopeName.equals(DOUBLE_SCOPE.getScopeName()))
            return DOUBLE_SCOPE;
        if(scopeName.equals(STRING_SCOPE.getScopeName()))
            return STRING_SCOPE;
        if(scopeName.equals(DATETIME_SCOPE.getScopeName()))
            return DATETIME_SCOPE;
        if(scopeName.equals(BIGINTEGER_SCOPE.getScopeName()))
            return BIGINTEGER_SCOPE;
        if(scopeName.equals(BOOLEAN_SCOPE.getScopeName()))
            return BOOLEAN_SCOPE;
        
        throw new GrexParseException("Unknown scope name.");
    }
}
