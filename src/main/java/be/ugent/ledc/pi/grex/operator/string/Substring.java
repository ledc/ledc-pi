package be.ugent.ledc.pi.grex.operator.string;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.operator.Operator;

public class Substring implements Operator<String>
{
    public static final String SUBSTRING_WILDCARD = "*";
    
    @Override
    public String getSymbol()
    {
        return "substring";
    }

    @Override
    public int getNumberOfArguments()
    {
        return 3;
    }

    @Override
    public String apply(String... arguments)
    {
        if(arguments.length != getNumberOfArguments())
            throw new GrexParseException(getSymbol() + " requires " + getNumberOfArguments() + " arguments");
        
        String s = arguments[0];
        
        int start = arguments[1].equals(SUBSTRING_WILDCARD) ? 0 : Math.max(0, Integer.parseInt(arguments[1]));
        int end = arguments[2].equals(SUBSTRING_WILDCARD) ? s.length() : Math.min(Integer.parseInt(arguments[2]), s.length());
        
        return s.substring(start, end);
    }
    
}
