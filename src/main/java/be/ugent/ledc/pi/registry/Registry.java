package be.ugent.ledc.pi.registry;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.DependencyGraph;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.conversion.ConversionFactory;
import be.ugent.ledc.pi.conversion.PropertyTransform;
import be.ugent.ledc.pi.conversion.grex.GrexStringConvertor;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.PostfixParser;
import be.ugent.ledc.pi.grex.operator.OperatorFactory;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.PropertyScanner;
import be.ugent.ledc.pi.scanner.ScannerFactory;
import be.ugent.ledc.pi.scanner.execution.ScannerExecutor;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.validation.ValidationScheme;
import be.ugent.ledc.pi.validation.ValidationSchemeFactory;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Registry
{
    public static final String OPERATOR_ARRAY   = "Operators";
    
    public static final String VALIDATION_SCHEME_ARRAY  = "ValidationSchemes";
    public static final String SCANNER_ARRAY            = "Scanners";
    public static final String TRANSFORMATOR_ARRAY      = "Transformators";

    private static Registry INSTANCE;

    private final Map<Property, ValidationScheme> validationSchemes;
    
    private final Map<Property, Map<Property, PropertyTransform>> transformators;
    
    private final Map<Property, List<PropertyScanner>> scanners;
    
    private Registry()
    {
        validationSchemes = new HashMap<>();
        transformators = new HashMap<>();
        scanners = new HashMap<>();
    }

    public static Registry getInstance()
    {
        if (INSTANCE == null)
        {
            INSTANCE = new Registry();
        }
        return INSTANCE;
    }

    public final void register(ValidationScheme vScheme) throws PiException
    {   
        validationSchemes.put(vScheme.getProperty(), vScheme);
    }
    
    public final void register(PropertyTransform transform) throws PiException
    {   
        Property in = transform.getInputProperty();
        Property out = transform.getOutputProperty();
        
        if(!hasValidationScheme(in))
        {
            throw new PiException("Cannot register transform from "
                + in 
                + " to "
                + out
                + ". Cause: no validation scheme for "
                + in + " is present.");
        }

        if(!transformators.containsKey(in))
            transformators.put(in, new HashMap<>());

        transformators
            .get(in)
            .put(out, transform);
    }

    public final void register(PropertyScanner propertyScanner)
    {
        Property property = propertyScanner.getScannableProperty();

        if (!scanners.containsKey(property))
        {
            scanners.put(property, new ArrayList<>());
        }
        scanners.get(property).add(propertyScanner);
    }
    
    public boolean hasValidationScheme(Property property)
    {
        return validationSchemes.containsKey(property);
    }

    public ValidationScheme getValidationScheme(Property property)
    {
        return validationSchemes.get(property);
    }

    public List<PropertyTransform> buildTransform(Property in, Property out)
    {
        List<PropertyTransform> transformSequence = new ArrayList<>();
        
        //Dummy case
        if(in.equals(out))
        {
            transformSequence.add(new PropertyTransform(
                in,
                out,
                new GrexStringConvertor(new Grex("@0"), ".+")));
        }
        else if(canDirectlyTransform(in, out))
        {
            transformSequence.add(transformators.get(in).get(out));
        }
        else if(canTransform(in, out))
        {
            //
            //Dijkstra's shortest path
            //
            
            Set<Property> memory = new HashSet<>();
            LinkedList<Property> frontier = new LinkedList<>();

            Map<Property, Integer> distanceMap = new HashMap<>();
            Map<Property, Property> previousMap = new HashMap<>();
            
            for(Property requestable: transformators.keySet())
            {
                distanceMap.put(
                    requestable,
                    requestable.equals(in) ? 0 : Integer.MAX_VALUE
                );
                
                for(Property p: transformators.get(requestable).keySet())
                {
                    if(p.equals(in))
                    {
                        distanceMap.put(p, 0);
                    }
                    else
                    {
                        distanceMap.put(p, Integer.MAX_VALUE);
                    }
                }
            }
            
            frontier.add(in);

            while(!frontier.isEmpty())
            {
                //Poll the next property to inspect
                Property toInspect = frontier.poll();

                if(transformators.containsKey(toInspect))
                {
                    //Get all property from which this can be inferred
                    Set<Property> newCandidates = transformators.get(toInspect).keySet();

                    for(Property newCandidate: newCandidates)
                    {
                        //If we have not yet treated this candidate property
                        if(!memory.contains(newCandidate))
                        {
                            //Check if we found a shorter distance
                            int alternateDistance = distanceMap.get(toInspect) + 1;

                            if(alternateDistance < distanceMap.get(newCandidate))
                            {
                                distanceMap.put(newCandidate, alternateDistance);
                                previousMap.put(newCandidate, toInspect);
                            }

                            frontier.add(newCandidate);
                        }
                    }
                }

                //Register the to inspect property as done treated
                memory.add(toInspect);
            }

//            LOGGER.log(Level.FINER, "Shortest inference path has {0} hops", distanceMap.get(inputProperty));
            
            //Use a flag to signal if we have reached the end of the path
            boolean stop = false;
            
            //Keep an intermediate property
            Property current = out;
            
            while(!stop)
            {
                if(current.equals(out))
                {
                    //Stop
                    stop = true;
                }
                else
                {
                    //Retrieve the inferencer to go from the current property to its preceding property and add it to the path
                    transformSequence.add(0, transformators.get(previousMap.get(current)).get(current));
                    
                    //Update the current property
                    current = previousMap.get(current);
                }
            }
        }

        return transformSequence;
    }
    
    public boolean canDirectlyTransform(Property in, Property out)
    {
        return transformators.containsKey(in)
            && transformators.get(in).containsKey(out);
    }
    
    public boolean canTransform(Property in, Property out)
    {
        return transformReach(in).contains(out);
    }
    
    public Set<Property> transformReach(Property in)
    {
        Set<Property> reach = new HashSet<>();
        LinkedList<Property> frontier = new LinkedList<>();
        
        frontier.add(in);
        
        while(!frontier.isEmpty())
        {
            //Poll the next property to inspect
            Property toInspect = frontier.poll();
            
            if(transformators.containsKey(toInspect))
            {
                //Get all property from which this can be inferred
                Set<Property> newCandidates = transformators.get(toInspect).keySet();

                for(Property newCandidate: newCandidates)
                {
                    if(!reach.contains(newCandidate))
                    {
                        frontier.add(newCandidate);
                    }
                }
            }
            
            //Add to memory
            reach.add(toInspect);
        }
        
        return reach;
    }

    public List<Couple<Property>> getTranformCouples()
    {
        return transformators
            .entrySet()
            .stream()
            .flatMap(e -> e
                .getValue()
                .keySet()
                .stream()
                .map(pp -> new Couple<>(e.getKey(), pp)))
            .distinct()
            .toList();
    }
    
    public List<PropertyScanner> getScanners(Property property)
    {
        return hasScanners(property) ? scanners.get(property) : new ArrayList<>();
    }

    public boolean hasScanners(Property property)
    {
        return scanners.containsKey(property);
    }

    public List<Property> getPropertiesWithValidation()
    {
        return scanners.keySet().stream().collect(Collectors.toList());
    }
    
    public List<Property> getPropertiesWithScanner()
    {
        return scanners.keySet().stream().collect(Collectors.toList());
    }

    public DependencyGraph<PropertyScanner> createDependencyGraph(Property... requiredProperties)
    {
        return createDependencyGraph(Stream
            .of(requiredProperties)
            .collect(Collectors.toList())
        );
    }
    
    public DependencyGraph<PropertyScanner> createDependencyGraph(Collection<Property> requiredProperties)
    {
        //Initialize an empty graph
        DependencyGraph<PropertyScanner> dependencyGraph = new DependencyGraph<>();

        if (requiredProperties != null)
        {
            List<Property> expanded = expand(requiredProperties);

            for (Property requiredProperty : expanded)
            {
                addProperty(dependencyGraph, requiredProperty);
            }
        }

        return dependencyGraph;
    }

    public Function<String, ResultBuffer> createSequentialTaskFor(Consumer<PropertyScanException> exceptionHandler, Property... properties) throws DependencyGraphException
    {
        List<PropertyScanner> propertyScanners = createDependencyGraph(properties).toList();

        return text ->
        {
            ResultBuffer resultBuffer = new ResultBuffer();
            for (PropertyScanner propertyScanner : propertyScanners)
            {
                try
                {
                    propertyScanner.scan(text, resultBuffer);
                }
                catch (PropertyScanException e)
                {
                    exceptionHandler.accept(e);
                }
            }
            return resultBuffer;
        };
    }

    public Consumer<String> createParallellizableTaskFor(Consumer<ResultBuffer> resultBufferHandler, Consumer<PropertyScanException> exceptionHandler, Property... properties) throws DependencyGraphException
    {
        List<PropertyScanner> propertyScanners = createDependencyGraph(properties).toList();

        return text ->
        {
            ResultBuffer resultBuffer = new ResultBuffer();
            for (PropertyScanner propertyScanner : propertyScanners)
            {
                try
                {
                    propertyScanner.scan(text, resultBuffer);
                }
                catch (PropertyScanException e)
                {
                    exceptionHandler.accept(e);
                }
            }
            resultBufferHandler.accept(resultBuffer);
        };
    }

    private List<Property> expand(Collection<Property> requiredProperties)
    {
        List<Property> expanded = new ArrayList<>();

        for(Property requiredProperty: requiredProperties)
        {
            if(hasScanners(requiredProperty))
            {
                expanded.add(requiredProperty);
            }
            else
            {
                for(Property p: scanners.keySet())
                {
                    if(requiredProperty.isWeakCompatibleWith(p))
                    {
                        expanded.add(p);
                    }
                }
            }
        }

        return expanded;
    }

    private void addProperty(DependencyGraph<PropertyScanner> dependencyGraph, Property property)
    {
        //Get the scanners
        List<PropertyScanner> localScanners = getScanners(property);

        if (localScanners != null)
        {
            for(PropertyScanner scanner: localScanners)
            {
                if(scanner != null)
                {
                    for (Property dependee : scanner.getScannerDependencies())
                    {
                        addProperty(dependencyGraph, dependee);
                    }

                    List<PropertyScanner> dependeeScannersList = new ArrayList<>();

                    for(Property dependee : scanner.getScannerDependencies())
                    {
                        dependeeScannersList.addAll(getScanners(dependee));
                    }

                    dependencyGraph.addDependent(scanner, dependeeScannersList.toArray(new PropertyScanner[dependeeScannersList.size()]));
                }
            }
        }
    }

    public DependencyGraph<Property> createValidationDependencyGraph(Property ... properties) throws DependencyGraphException
    {
        return createValidationDependencyGraph(Stream
            .of(properties)
            .collect(Collectors.toList())
        );
    }
    
    public DependencyGraph<Property> createValidationDependencyGraph(Collection<Property> properties) throws DependencyGraphException
    {
        //Initialize an empty graph
        DependencyGraph<Property> dependencyGraph = new DependencyGraph<>();

        if (properties != null)
        {
            for (Property p : properties)
            {
                addValidationProperty(dependencyGraph, p);
            }
        }

        return dependencyGraph;
    }

    private void addValidationProperty(DependencyGraph<Property> dependencyGraph, Property property) throws DependencyGraphException
    {
        ValidationScheme scheme = validationSchemes.get(property);
        
        if(scheme != null)
        {
            if(!scheme.getMeasure().getMeasureDependencies().isEmpty())
            {
                dependencyGraph.addDependent(
                    property,
                    scheme
                        .getMeasure()
                        .getMeasureDependencies()
                        .toArray(new Property[scheme.getMeasure().getMeasureDependencies().size()])
                );

                dependencyGraph.verifyValidity();

                for(Property dependee: scheme.getMeasure().getMeasureDependencies())
                {
                    addValidationProperty(dependencyGraph, dependee);
                }
            }
            
        }
        else
        {
            throw new DependencyGraphException("Can not find a measure for " + property);
        }
    }
    
    public void dump(File outputFile) throws IOException
    {
        //Array to hold any operators
        JSONArray opArray = new JSONArray(
            PostfixParser
                .getInstance()
                .getPersistableOperators()
                .stream()
                .map(o -> new JSONObject(o.buildFeatureMap().getFeatures()))
                .collect(Collectors.toList())
        );
        
        //Array to hold the validation schemes
        JSONArray vArray = new JSONArray(
            validationSchemes
            .values()
            .stream()
            .map(s -> new JSONObject(s.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList())
        );
        
        //Array to hold the transformators
        JSONArray tArray = new JSONArray(
            transformators
            .values()
            .stream()
            .flatMap(m -> m.values().stream())
            .map(t -> new JSONObject(t.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList()));
        
        //Array to hold the scanners
        JSONArray sArray = new JSONArray(
            scanners
            .values()
            .stream()
            .flatMap(s -> s.stream())
            .map(s -> new JSONObject(s.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList()));
        

        
        JSONObject mainObject = new JSONObject();
        
        mainObject.put(OPERATOR_ARRAY, opArray);
        mainObject.put(VALIDATION_SCHEME_ARRAY, vArray);
        mainObject.put(SCANNER_ARRAY, sArray);
        mainObject.put(TRANSFORMATOR_ARRAY, tArray);
        
        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                        new FileOutputStream(outputFile),
                        StandardCharsets.UTF_8)
        ))
        {
            writer.write(mainObject.toString(4));
            writer.flush();
        }
 
    }
    
    public void dumpOnly(File outputFile, Property p) throws IOException
    {
        JSONArray opArray = new JSONArray(
        PostfixParser
            .getInstance()
            .getPersistableOperators()
            .stream()
            .map(o -> new JSONObject(o.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList()));
        
        //Array to hold the validation schemes
        JSONArray vArray = new JSONArray(
            validationSchemes
            .values()
            .stream()
            .filter(s -> s.getProperty().equals(p))
            .map(s -> new JSONObject(s.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList())
        );
        
        //Array to hold the scanners
        JSONArray sArray = new JSONArray(
            scanners
            .values()
            .stream()
            .flatMap(s -> s.stream())
            .filter(s -> s.getScannableProperty().equals(p))
            .map(s -> new JSONObject(s.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList()));
        
        Set<Property> iReach = transformReach(p);
        
        //Array to hold the transformators
        JSONArray tArray = new JSONArray(
            getTranformCouples()
            .stream()
            .filter(couple -> iReach.contains(couple.getSecond()))
            .map(couple -> buildTransform(p, couple.getSecond()).get(0))
            .map(t -> new JSONObject(t.buildFeatureMap().getFeatures()))
            .collect(Collectors.toList()));
        
        
        JSONObject mainObject = new JSONObject();
        
        mainObject.put(OPERATOR_ARRAY, opArray);
        mainObject.put(VALIDATION_SCHEME_ARRAY, vArray);
        mainObject.put(SCANNER_ARRAY, sArray);
        mainObject.put(TRANSFORMATOR_ARRAY, tArray);
        
        try (BufferedWriter writer = new BufferedWriter(
            new OutputStreamWriter(
                new FileOutputStream(outputFile),
                StandardCharsets.UTF_8)
        ))
        {
            writer.write(mainObject.toString(4));
            writer.flush();
        }
 
    }

    public void restore(File inputFile) throws FileNotFoundException, LedcException, URISyntaxException
    {
        JSONObject topObject = new JSONObject(
            new JSONTokener(
                new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(inputFile),
                        StandardCharsets.UTF_8))));
        
        //Get array of persistable operators
        JSONArray operators = topObject.getJSONArray(OPERATOR_ARRAY);

        for(int i=0; i<operators.length(); i++)
        {
            OperatorFactory.registerOperator(convertToFeatureMap(operators.getJSONObject(i).toMap()));
        }
        
        //Get array of validation schemes
        JSONArray vArray = topObject.getJSONArray(VALIDATION_SCHEME_ARRAY);

        for(int i=0; i<vArray.length(); i++)
        {
            register(ValidationSchemeFactory.createValidationScheme(
                convertToFeatureMap(vArray.getJSONObject(i).toMap()))
            );
        }
        
        //Get array of scanners
        JSONArray sArray = topObject.getJSONArray(SCANNER_ARRAY);
        
        for(int i=0; i<sArray.length(); i++)
        {
            register(ScannerFactory.createPropertyScanner(
                convertToFeatureMap(sArray.getJSONObject(i).toMap()))
            );
        }
        
        //Get array of transformators
        JSONArray tArray = topObject.getJSONArray(TRANSFORMATOR_ARRAY);

        for(int i=0; i<tArray.length(); i++)
        {
            register(ConversionFactory.createPropertyTransform(
                convertToFeatureMap(tArray.getJSONObject(i).toMap())
            ));
        }
    }
    
    private static FeatureMap convertToFeatureMap(Map<String, Object> mapping)
    {
        FeatureMap fm = new FeatureMap();
        
        for(String key: mapping.keySet())
        {
            if(mapping.get(key) instanceof Map)
                fm.addFeature(key, convertToFeatureMap((Map) mapping.get(key)));
            else if (mapping.get(key) instanceof List)
                fm.addFeature(key, convertList((List) mapping.get(key)));
            else
                fm.addFeature(key, mapping.get(key));
        }
        
        return fm;
    }
    
    private static List<FeatureMap> convertList(List list)
    {
        List featureList = new ArrayList();
        
        for(int i=0; i<list.size(); i++)
        {
            if(list.get(i) instanceof Map)
                featureList.add(convertToFeatureMap((Map) list.get(i)));
            else if (list.get(i) instanceof List)
                featureList.add(convertList((List) list.get(i)));
            else
                featureList.add(list.get(i));
        }
        
        return featureList;
    }
    
    //
    //Below are convenience method that directly access the validation techniques
    //
    
    public String transform(Property in, Property out, String value)
    {
        if(!hasValidationScheme(in))
            return null;

        String output = getValidationScheme(in)
            .getConvertor()
            .convert(value);
        
        if(out == null || in.equals(out))
            return output;
        
        if(canTransform(in, out))
        {
            List<PropertyTransform> sequence = buildTransform(in, out);
            
            for(PropertyTransform pt: sequence)
            {
                output = pt.transform(output);
            }
            
            return output;
        }
        
        return null;
    
    }
    
    public int measure(Property p, String value)
    {
        if(!hasValidationScheme(p))
            return 0;

        return this.validationSchemes.get(p).getMeasure().measure(value);
    }
    
    public String explain(Property p, String value)
    {
        if(!hasValidationScheme(p))
            return "No validation scheme found for property " + p;

        return this.validationSchemes.get(p).getMeasure().explain(value);
    }
    
    public boolean isValid(Property p, String value)
    {
        if(!hasValidationScheme(p))
            return false;

        return this.validationSchemes.get(p).validate(value);
    }
    
    public String convert(Property p, String value)
    {
        if(!hasValidationScheme(p))
            return null;

        return this.validationSchemes.get(p).getConvertor().convert(value);
    }
    
    public List<String> scan(Property p, String text) throws DependencyGraphException, PropertyParseException, PropertyScanException
    {
        //Build an executor to search for the desired properties.
        ScannerExecutor executor = new ScannerExecutor(p);
        
        if(text == null)
            return new ArrayList<>();

        //Execute the scan and store results.
        return executor
            .execute(text)
            .getScanResults()
            .stream()
            .map(scr -> scr.getInstance())
            .toList();
    }
}
