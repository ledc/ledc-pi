package be.ugent.ledc.pi.io;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.pi.assertion.AssertionParser;
import be.ugent.ledc.pi.assertion.PropertyAssertions;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AssertionIo
{
    public static final String COMMENT_PREFIX   = "--";
    
    public static void writeAssertions(PropertyAssertions assertions, File file, Charset charset) throws FileNotFoundException
    {
        writeAssertions(assertions, file, charset, false);
    }
    
    public static void writeAssertions(PropertyAssertions assertions, File file, Charset charset, boolean append) throws FileNotFoundException
    {
        try (
            PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(
                        new FileOutputStream(file, append),
                        charset))
        )
        {
            assertions
                .getRules()
                .stream()
                .map(a -> a.toString())
                .forEach(writer::println);
            
            writer.flush();
        }
    }
    
    public static void writeAssertions(PropertyAssertions assertions, File file) throws FileNotFoundException
    {
        writeAssertions(assertions, file, StandardCharsets.UTF_8);
    }
    
    public static PropertyAssertions readAssertions(File file) throws FileNotFoundException, LedcException
    {
        return readAssertions(file, StandardCharsets.UTF_8);
    }
    
    public static PropertyAssertions readAssertions(File file, Charset charset) throws FileNotFoundException, LedcException {

        Scanner scanner = new Scanner(file, charset.name());

        List<String> lines = new ArrayList<>();
        
        while (scanner.hasNextLine())
        {
            //Read the line, ignore leading and trailing spaces
            lines.add(scanner.nextLine().trim());
        }

        return AssertionParser.parse(lines);

    }
}
