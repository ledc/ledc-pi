package be.ugent.ledc.pi.property;

import be.ugent.ledc.core.ParseException;

public class PropertyParseException extends ParseException
{
    public PropertyParseException(String message)
    {
        super(message);
    }

    public PropertyParseException(Throwable cause)
    {
        super(cause);
    }
}
