package be.ugent.ledc.pi.property;

import java.util.Objects;

public class Property
{
    private String domain;
    
    private String type;
    
    private String name;
    
    private String locale;
    
    public static final String LOCALE_WILDCARD = "*";
    public static final String NAME_WILDCARD = "*";

    public Property(String domain, String type, String name, String locale)
    {
        //TODO solve funky situation with type==null and type=="" not being equal (see isWeakCompatibleWith())
        this.domain = domain;
        this.type = type;
        this.name = name;
        this.locale = locale;
    }

    public Property(){}

    public static Property parseProperty(String canonicalName) throws PropertyParseException
    {
        if(canonicalName == null) {
            throw new PropertyParseException("Unparseable canonical name for property, it's null.");
        }
        else if(canonicalName.startsWith("http://"))
        {
            canonicalName = canonicalName.replaceAll("http://", "");
        }
        
        int first   = canonicalName.indexOf("/");
        int last    = canonicalName.indexOf("#");
        int colon   = canonicalName.lastIndexOf(":");
        
        if(first == -1 || last == -1)
        {
            throw new PropertyParseException("Unparseable canonical name for property: " + canonicalName);
        }
        
        String domain   = canonicalName.substring(0, first).trim();
        String type     = first == last ? null : canonicalName.substring(first + 1, last).trim();
        String name     = colon == -1 ? canonicalName.substring(last + 1) : canonicalName.substring(last + 1, colon);
        String locale   = colon == -1 ? LOCALE_WILDCARD : canonicalName.substring(colon + 1);

        return new Property(domain, type, name, locale);
    }

    public String getCanonicalName()
    {
        return domain + "/" + (type == null || type.isEmpty() ? "": type) + "#" + name + ":" + locale;
    }

    @Override
    public String toString()
    {
        return getCanonicalName();
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.domain);
        hash = 97 * hash + Objects.hashCode(this.type);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.locale);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Property other = (Property) obj;
        if (!Objects.equals(this.domain, other.domain))
        {
            return false;
        }
        if (!Objects.equals(this.type, other.type))
        {
            return false;
        }
        if (!Objects.equals(this.name, other.name))
        {
            return false;
        }
        if (!Objects.equals(this.locale, other.locale))
        {
            return false;
        }
        return true;
    }
    
    /**
     * Returns true if this property is strong compatible with the given {@see Property}. Strong compatibility requires the domains and types to be the same.
     * The names must be equal to each other, unless one of them equals the name wildcard.
     * Locales are compared in the same way as names.
     * @param property
     * @return 
     */
    public boolean isStrongCompatibleWith(Property property)
    {
        if(property != null)
        {
            //Check domain
            if((domain == null && property.getDomain() != null) || (domain != null && property.getDomain() == null) || !domain.equals(property.getDomain()))
            {
                return false;
            }
            
            //Check type
            if((type == null && property.getType() != null) || (type != null && property.getType() == null) || !type.equals(property.getType()))
            {
                return false;
            }
            
            //Check name
            if((name == null && property.getName() != null) || (name != null && property.getName() == null))
            {
                return false;
            }
            if(name != null && property.getName() != null && !name.equals(NAME_WILDCARD) && !property.getName().equals(NAME_WILDCARD) && !name.equals(property.getName()))
            {
                return false;
            }
            
            //Check locale
            if((locale == null && property.getLocale() != null) || (locale != null && property.getLocale()== null))
            {
                return false;
            }
            if(locale != null && property.getLocale() != null && !locale.equals(LOCALE_WILDCARD) && !property.getLocale().equals(LOCALE_WILDCARD) && !locale.equals(property.getLocale()))
            {
                return false;
            }

            return true;
        }
        
        return false;
    }
    
    /**
     * Returns true if this property is weak compatible with the given {@see Property}.
     * Weak compatibility requires the same conditions as strong compatibility, except for type.
     * For type, it is sufficient that the type of the current property is a prefix of the given {@see Property}.
     * Because of this, weak compatibility is not a symmetric relation.
     * @param property
     * @return 
     */
    public boolean isWeakCompatibleWith(Property property)
    {
        if(property != null)
        {
            //Check domain
            if((domain == null && property.getDomain() != null) || (domain != null && property.getDomain() == null) || !domain.equals(property.getDomain()))
            {
                return false;
            }
            
            //Check type
            if((type == null && property.getType() != null) || (type != null && property.getType() == null) || !property.getType().startsWith(type))
            {
                return false;
            }
            
            //Check name
            if((name == null && property.getName() != null) || (name != null && property.getName() == null))
            {
                return false;
            }
            if(name != null && property.getName() != null && !name.equals(NAME_WILDCARD) && !property.getName().equals(NAME_WILDCARD) && !name.equals(property.getName()))
            {
                return false;
            }
            
            //Check locale
            if((locale == null && property.getLocale() != null) || (locale != null && property.getLocale()== null))
            {
                return false;
            }
            if(locale != null && property.getLocale() != null && !locale.equals(LOCALE_WILDCARD) && !property.getLocale().equals(LOCALE_WILDCARD) && !locale.equals(property.getLocale()))
            {
                return false;
            }

            return true;
        }
        
        return false;
    }
    
    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLocale()
    {
        return locale;
    }

    public void setLocale(String locale)
    {
        this.locale = locale;
    }
    
    public String getNameAndLocale()
    {
        return getName() + ":" + getLocale();
    }
}
