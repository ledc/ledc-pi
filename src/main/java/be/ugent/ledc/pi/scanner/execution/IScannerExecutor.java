package be.ugent.ledc.pi.scanner.execution;

import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;

public interface IScannerExecutor
{
    public ResultBuffer execute(String text) throws PropertyScanException;
}
