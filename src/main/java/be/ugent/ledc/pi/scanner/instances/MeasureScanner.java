package be.ugent.ledc.pi.scanner.instances;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.PropertyScanner;
import be.ugent.ledc.pi.scanner.ScannerFeature;
import be.ugent.ledc.pi.scanner.ScannerType;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.scanner.result.ScanResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MeasureScanner extends PropertyScanner
{
    private final boolean patternWrap;
    
    private final String pattern;
    
    public MeasureScanner(Property scannableProperty, String pattern, boolean patternWrap)
    {
        super(scannableProperty);
        this.pattern = pattern;
        this.patternWrap = patternWrap;
    }
    
    public MeasureScanner(Property scannableProperty, String pattern)
    {
        this(scannableProperty, pattern, true);
    }
    
    @Override
    public void scan(String text, ResultBuffer resultBuffer) throws PropertyScanException
    {

        //Get a matcher for the pattern (adapted to scanning)
        //Modify the before/after pattern to use lookbehind/lookahead ->
        //this allows for two patterns to follow each other and still match
        String adaptedPattern = patternWrap
            ? "(?<=^|\\W)(" + pattern + ")(?=\\W|$)"
            : pattern;
        
        Pattern p = Pattern.compile(adaptedPattern);
        Matcher matcher = p.matcher(text);
        

        while(matcher.find())
        {
            //it can be the full match -> lookbehind/lookahead ensures that the pattern is without the non-word chars
            String candidate = matcher.group(0);

            if(Registry
                .getInstance()
                .getValidationScheme(getScannableProperty())
                .validate(candidate))
            {
                resultBuffer.register(
                    new ScanResult(
                        getScannableProperty(),
                        candidate,
                        matcher.start(),
                        matcher.end())
                );
            }
        }

    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
        .addFeature(ScannerFeature.TYPE.getName(), ScannerType.MEASURE.getName())
        .addFeature(ScannerFeature.PROPERTY.getName(), getScannableProperty().getCanonicalName())
        .addFeature(ScannerFeature.PATTERN.getName(), pattern);
    }
    
}
