package be.ugent.ledc.pi.scanner;

import be.ugent.ledc.core.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ScannerType
{
    MEASURE("Measure", ScannerFeature.TYPE, ScannerFeature.PROPERTY),
    PATTERN("Pattern", ScannerFeature.TYPE, ScannerFeature.PROPERTY, ScannerFeature.PATTERN, ScannerFeature.PATTERN_WRAP),
    COMPOSED_PATTERN("ComposedPattern", ScannerFeature.TYPE, ScannerFeature.PROPERTY, ScannerFeature.SYMBOLS_INFO, ScannerFeature.PATTERN_ARRAY, ScannerFeature.PATTERN_WRAP);

    private final String name;
    
    private final Set<ScannerFeature> requiredFeatures;
    private final Set<ScannerFeature> optionalFeatures;
    
    private ScannerType(String name, Set<ScannerFeature> requiredFeatures, Set<ScannerFeature> optionalFeatures)
    {
        this.name = name;
        this.requiredFeatures = requiredFeatures;
        this.optionalFeatures = optionalFeatures;
    }
    
    private ScannerType(String name, ScannerFeature... requiredFeatures)
    {
        this(name, Stream.of(requiredFeatures).collect(Collectors.toSet()), new HashSet<>());
    }

    public String getName()
    {
        return name;
    }
    
    public static ScannerType parse(String name) throws ParseException
    {
        if(Stream.of(values()).noneMatch(pt -> pt.getName().equals(name)))
            throw new ParseException("Unknown scanner type.");
        
        return Stream.of(values()).filter(pt -> pt.getName().equals(name)).findFirst().get();
    }

    public Set<ScannerFeature> getRequiredFeatures()
    {
        return requiredFeatures;
    }

    public Set<ScannerFeature> getOptionalFeatures()
    {
        return optionalFeatures;
    }
}
