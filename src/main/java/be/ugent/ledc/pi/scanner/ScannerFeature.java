package be.ugent.ledc.pi.scanner;

public enum ScannerFeature
{
    TYPE("Type"),
    PROPERTY("Property"),
    PATTERN("Pattern"),
    PATTERN_ARRAY("PatternArray"),
    SYMBOLS_INFO("SymbolInfoMap"),
    PATTERN_WRAP("PatternWrapping"),
    ;
    
    private final String name;
    
    private ScannerFeature(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
