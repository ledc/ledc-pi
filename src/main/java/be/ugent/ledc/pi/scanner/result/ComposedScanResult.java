package be.ugent.ledc.pi.scanner.result;

import be.ugent.ledc.pi.property.Property;
import java.util.HashMap;
import java.util.Map;

public class ComposedScanResult extends ScanResult
{
    private final Map<String, ScanResult> subResults;

    public ComposedScanResult(Property property, String instance, int startIndex, int endIndex)
    {
        super(property, instance, startIndex, endIndex);
        this.subResults = new HashMap<>();
    }

    public Map<String, ScanResult> getSubResults()
    {
        return subResults;
    }

    public void addSubResult(String symbol, ScanResult subResult)
    {
        this.subResults.put(symbol, subResult);
    }
}
