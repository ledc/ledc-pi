package be.ugent.ledc.pi.scanner.instances;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.PatternManager;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.scanner.result.ComposedScanResult;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.ScannerFeature;
import be.ugent.ledc.pi.scanner.ScannerType;
import be.ugent.ledc.pi.scanner.result.ScanResult;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

public class ComposedPatternScanner extends ComposedScanner
{
    private static final Logger LOGGER = Logger.getLogger(ComposedPatternScanner.class.getName());

    private final String[] patterns;
    
    private final boolean patternWrap;

    public ComposedPatternScanner(List<SymbolPropertyInfo> symbolPropertyInfo, Property scannableProperty, boolean patternWrap, String ... patterns)
    {
        super(symbolPropertyInfo, scannableProperty);
        this.patterns = patterns;
        this.patternWrap = patternWrap;
    }
    
    public ComposedPatternScanner(List<SymbolPropertyInfo> symbolPropertyInfo, Property scannableProperty, String ... patterns)
    {
        this(symbolPropertyInfo, scannableProperty, true, patterns);
    }

    public ComposedPatternScanner(Property scannableProperty, String ... patterns)
    {
        super(scannableProperty);
        this.patterns = patterns;
        this.patternWrap = true;
    }

    public ComposedPatternScanner(String ... patterns)
    {
        this.patterns = patterns;
        patternWrap = true;
    }

    @Override
    public void scan(String text, ResultBuffer resultBuffer) throws PropertyScanException
    {
        //Force a clean
        resultBuffer.clean();
                
        List<ComposedScanResult> scanResults = new ArrayList<>();

        //Check if all dependees are found
        boolean preCheck = true;

        for(String symbol : getSymbols())
        {
            //Is this symbol required?
            if(isRequired(symbol))
            {
                //Is there at least one instance of a property for this symbol
                boolean symbolCheck = false;

                for(Property p: getAllPropertiesForSymbol(symbol))
                {
                    symbolCheck |= !resultBuffer.getScanResultsForProperty(p).isEmpty();
                }

                preCheck &= symbolCheck;
            }
        }
        
        Map<String, List<ScanResult>> symbolResultMap = new HashMap<>();

        for(String symbol: getSymbols())
        {
            List<ScanResult> symbolResults = new ArrayList<>();

            getAllPropertiesForSymbol(symbol).stream().forEach((p) ->
            {
                symbolResults.addAll(resultBuffer.getScanResultsForProperty(p));
            });

            //Clean results for this symbol
            for(int i=0; i<symbolResults.size(); i++)
            {
                for(int j=i+1; j<symbolResults.size(); j++)
                {
                    if(symbolResults.get(i).subsumedBy(symbolResults.get(j)))
                    {
                        LOGGER.log(
                                Level.FINEST,
                                "Removing '{0}'. Subsumed by {1}",
                                new Object[]{symbolResults.get(i), symbolResults.get(j)});
                        symbolResults.remove(i);
                        i--;
                        break;
                    }
                }
            }

            symbolResultMap.put(symbol, symbolResults);
        }

        if(preCheck && !symbolResultMap.isEmpty())
        {
            for(String pattern: patterns)
            {
                //Replace symbols in pattern with occurrences
                String modifiedPattern = pattern;

                for(String symbol: symbolResultMap.keySet())
                {
                    //StringJoiner joiner = new StringJoiner("|", "(?<" + symbol + ">", ")");
                    
                    List<String> listOfInstances = symbolResultMap.get(symbol).stream().map(r -> r.getInstance()).collect(Collectors.toList());
                    String symbolPattern = "(?<" + symbol + ">" + getPatternFromList(listOfInstances) + ")";

                    modifiedPattern = modifiedPattern.replace(symbol, isRequired(symbol) ? symbolPattern : symbolPattern + "?");
                }

                LOGGER.log(Level.FINER, "Composed scanning with pattern (?<=^|\\W)({0})(?=\\W|$)", modifiedPattern);

                //Find possible composed properties
                Matcher matcher = PatternManager.getInstance().getMatcher( patternWrap ? "(?<=^|\\W)(" + modifiedPattern + ")(?=\\W|$)" : modifiedPattern, text);

                while(matcher.find())
                {
                    String instance = matcher.group(0);

                    //Supress empty results
                    if(instance != null && !instance.trim().isEmpty())
                    {
                        ComposedScanResult composedScanResult = new ComposedScanResult(getScannableProperty(), instance, matcher.start(), matcher.end());

                        for(String symbol: getSymbols())
                        {
                            String subMatch = matcher.group(symbol);

                            for(ScanResult scanResult: symbolResultMap.get(symbol))
                            {
                                if(scanResult.getInstance().equals(subMatch))
                                {
                                    composedScanResult.addSubResult(symbol, scanResult);
                                    break;
                                }
                            }
                        }
                        scanResults.add(composedScanResult);
                    }
                    
                }
            }
        }

        for(ComposedScanResult r: scanResults)
        {
            resultBuffer.register(r);
        }
    }

    private String getPatternFromList(List<String> options)
    {
        options.sort(Collections.reverseOrder());
        
        return options.stream().collect(Collectors.joining("|"));
    }
    
    @Override
    public String toString()
    {
        return getScannableProperty().getCanonicalName() + " -> " + getScannerDependencies().stream().map(p -> p.getCanonicalName()).collect(Collectors.toList());
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ScannerFeature.TYPE.getName(), ScannerType.COMPOSED_PATTERN.getName())
            .addFeature(ScannerFeature.PROPERTY.getName(), getScannableProperty().getCanonicalName())
            .addFeature(ScannerFeature.PATTERN_ARRAY.getName(), Arrays.asList(patterns))
            .addFeature(ScannerFeature.SYMBOLS_INFO.getName(), getSymbolPropertyInfo()
                .stream()
                .map(spi -> spi.buildFeatureMap().getFeatures())
                .collect(Collectors.toCollection(ArrayList::new)))
            .addFeature(ScannerFeature.PATTERN_WRAP.getName(), patternWrap);
    }
}
