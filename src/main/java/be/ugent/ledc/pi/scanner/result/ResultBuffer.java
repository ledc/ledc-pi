package be.ugent.ledc.pi.scanner.result;

import be.ugent.ledc.pi.property.Property;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ResultBuffer
{
    private final List<ScanResult> scanResults;

    public ResultBuffer()
    {
        scanResults = new ArrayList<>();
    }
    
    public synchronized void register(ScanResult propertyScanResult)
    {
        scanResults.add(propertyScanResult);
    }
    
    public void clear()
    {
        scanResults.clear();
    }

    public List<ScanResult> getScanResultsForProperty(Property property)
    {
        return scanResults
            .stream()
            .filter(r -> r.getProperty()
                .getCanonicalName()
                .equals(property.getCanonicalName()))
            .collect(Collectors.toList());
    }
    
    public List<ScanResult> getScanResults()
    {
        return scanResults;
    }
    
    public void clean()
    {
        //Clean results
        List<ScanResult> copy = new ArrayList<>(scanResults);
        
        scanResults.removeIf(candidateForRemoval -> copy.stream().anyMatch(other -> candidateForRemoval.subsumedBy(other)));
    }
}
