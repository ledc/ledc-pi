package be.ugent.ledc.pi.scanner.execution;

import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.PropertyScanner;
import java.util.Arrays;
import java.util.List;

public class ScannerExecutor implements IScannerExecutor
{
    private final List<Property> requiredProperties;
    
    private final List<PropertyScanner> scanners;
    
    public ScannerExecutor(Property ... requiredProperties) throws DependencyGraphException, PropertyParseException
    {
        this.requiredProperties = Arrays.asList(requiredProperties);
        this.scanners = Registry
            .getInstance()
            .createDependencyGraph(requiredProperties)
            .toList();
    }
    
    public ScannerExecutor(List<Property> requiredProperties) throws DependencyGraphException, PropertyParseException
    {
        this(requiredProperties.toArray(new Property[requiredProperties.size()]));
    }

    @Override
    public ResultBuffer execute(String text) throws PropertyScanException
    {   
        ResultBuffer resultBuffer = new ResultBuffer();

        for(PropertyScanner scanner: scanners)
        {
            scanner.scan(text, resultBuffer);
        }
        
        //Remove subsumed results of the same property
        resultBuffer.clean();
        
        return resultBuffer;

    }
    
    public List<Property> getRequiredProperties()
    {
        return requiredProperties;
    }
    
    public int numberOfScanners()
    {
        return this.scanners.size();
    }
}
