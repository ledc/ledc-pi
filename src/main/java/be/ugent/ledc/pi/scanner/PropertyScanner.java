package be.ugent.ledc.pi.scanner;

import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.property.Property;
import java.util.ArrayList;
import java.util.List;
import be.ugent.ledc.pi.measure.MeasureDependency;

/**
 * A PropertyScanner is a tool to find instances of properties in a given text.
 * 
 * The main idea is to first some way of roughly searching the text for candidate instances and
 * then to verify these candidates with the quality measure that is available for the target
 * property.
 * 
 * @author abronsel
 */
public abstract class PropertyScanner implements Persistable, MeasureDependency
{
    private Property scannableProperty;

    public PropertyScanner(Property scannableProperty)
    {
        this.scannableProperty = scannableProperty;
    }

    public PropertyScanner(){}

    /**
     * Scans the text for instances of the target property and stores these instances
     * in a result buffer. The result buffer is used to pass results when scanners
     * are combined in a hierarchy of composed scanners.
     * @param text
     * @param resultBuffer
     * @throws PropertyScanException 
     */
    public abstract void scan(String text, ResultBuffer resultBuffer) throws PropertyScanException;

    public Property getScannableProperty()
    {
        return scannableProperty;
    }

    public void setScannableProperty(Property scannableProperty)
    {
        this.scannableProperty = scannableProperty;
    }

    public List<Property> getScannerDependencies()
    {
        return new ArrayList<>();
    }

    @Override
    public List<Property> getMeasureDependencies()
    {
        return new ArrayList<>();
    }
}