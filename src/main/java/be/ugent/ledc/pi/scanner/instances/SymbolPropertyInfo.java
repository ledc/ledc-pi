package be.ugent.ledc.pi.scanner.instances;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.property.Property;
import java.util.List;
import java.util.stream.Collectors;

public class SymbolPropertyInfo implements Persistable
{
    private final String symbol;
    
    private final List<Property> properties;
    
    private final boolean required;
    
    public static final String SYMBOL = "Symbol";
    public static final String PROPERTIES = "Properties";
    public static final String REQUIRED = "Required";

    public SymbolPropertyInfo(String symbol, List<Property> properties, boolean required)
    {
        this.symbol = symbol;
        this.properties = properties;
        this.required = required;
    }
    public SymbolPropertyInfo(String symbol, List<Property> properties)
    {
        this(symbol, properties, true);
    }

    public String getSymbol()
    {
        return symbol;
    }

    public List<Property> getProperties()
    {
        return properties;
    }

    public boolean isRequired()
    {
        return required;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(SYMBOL, symbol)
            .addFeature(PROPERTIES, properties.stream().map(p->p.getCanonicalName()).collect(Collectors.toList()))
            .addFeature(REQUIRED, required);
    }
}
