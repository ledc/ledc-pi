package be.ugent.ledc.pi.scanner.instances;

import be.ugent.ledc.pi.property.Property;
import java.util.List;

public class ComposedPatternScannerFactory
{
    public static ComposedPatternScanner create(Property property, List<SymbolPropertyInfo> symbolPropertyInfo)
    {
        return create(property, symbolPropertyInfo, true);
    }
    
    public static ComposedPatternScanner create(Property property, List<SymbolPropertyInfo> symbolPropertyInfo, boolean patternWrapping)
    {
        String pattern = "";
        
        for(int i=0; i<symbolPropertyInfo.size(); i++)
        {
            SymbolPropertyInfo spi = symbolPropertyInfo.get(i);
            
            pattern += spi.getSymbol() + (i==symbolPropertyInfo.size()-1 ? "" : "\\s*");
        }
        
        return new ComposedPatternScanner(symbolPropertyInfo, property, patternWrapping, pattern);
    }
}
