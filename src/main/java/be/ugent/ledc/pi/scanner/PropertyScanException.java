package be.ugent.ledc.pi.scanner;

import be.ugent.ledc.pi.PiException;

public class PropertyScanException extends PiException
{
    public PropertyScanException(String message)
    {
        super(message);
    }

    public PropertyScanException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PropertyScanException(Throwable cause)
    {
        super(cause);
    }
}
