package be.ugent.ledc.pi.scanner;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.scanner.instances.ComposedPatternScanner;
import be.ugent.ledc.pi.scanner.instances.MeasureScanner;
import be.ugent.ledc.pi.scanner.instances.SymbolPropertyInfo;
import java.util.ArrayList;
import java.util.List;

public class ScannerFactory
{
    public static PropertyScanner createPropertyScanner(FeatureMap featureMap) throws ParseException
    {
        if(featureMap.getString(ScannerFeature.TYPE.getName()) == null)
            throw new ParseException("Cannot determine the type of scanner. Scanners must include a " + ScannerFeature.TYPE.getName() + ".");
        
        //Get the scanner type
        ScannerType scannerType = ScannerType.parse(featureMap.getString(ScannerFeature.TYPE.getName()));
        
        if(scannerType.getRequiredFeatures().stream().anyMatch(feature -> !featureMap.getFeatures().containsKey(feature.getName())))
            throw new ParseException("Could not reconstruct scanner type " + scannerType.getName() + ". Required fields are missing...");
        
        //Any scanner always has a property
        Property property = Property.parseProperty(featureMap.getString(ScannerFeature.PROPERTY.getName()));
        
        switch(scannerType)
        {
            case MEASURE:
                return new MeasureScanner(
                    property,
                    featureMap.getString(ScannerFeature.PATTERN.getName())
                );
            case COMPOSED_PATTERN:
                return new ComposedPatternScanner(
                    parseSymbolInfoList(featureMap.getList(ScannerFeature.SYMBOLS_INFO.getName())),
                    property,
                    featureMap.getBoolean(ScannerFeature.PATTERN_WRAP.getName()),    
                    parsePatternArray(featureMap.getList(ScannerFeature.PATTERN_ARRAY.getName())));                        
        }
        
        throw new ParseException("Could not parse scanner for unknown reason...");
    }
    
    private static List<SymbolPropertyInfo> parseSymbolInfoList(List list) throws ParseException
    {
        List<SymbolPropertyInfo> spiList = new ArrayList<>();
        
        for(Object o: list)
        {
            spiList.add(parseSymbolInfo((FeatureMap)o));
        }
        return spiList;
    }
    
    private static SymbolPropertyInfo parseSymbolInfo(FeatureMap fm) throws ParseException
    {
        if(fm.getString(SymbolPropertyInfo.SYMBOL) == null)
            throw new ParseException(ScannerFeature.SYMBOLS_INFO.getName() + " must declare a symbol in the key '" + SymbolPropertyInfo.SYMBOL + "'.");
        
        if(fm.getList(SymbolPropertyInfo.PROPERTIES) == null)
            throw new ParseException(ScannerFeature.SYMBOLS_INFO.getName() + " must declare an array of properties in the key '" + SymbolPropertyInfo.PROPERTIES + "'.");
        
        if(fm.getBoolean(SymbolPropertyInfo.REQUIRED) == null)
            throw new ParseException(ScannerFeature.SYMBOLS_INFO.getName() + " must declare whether the symbol is required in the key '" + SymbolPropertyInfo.REQUIRED + "'.");
        
        return new SymbolPropertyInfo(
            fm.getString(SymbolPropertyInfo.SYMBOL),
                parseProperties(fm.getList(SymbolPropertyInfo.PROPERTIES)),
            fm.getBoolean(SymbolPropertyInfo.REQUIRED));
        
    }

    private static String[] parsePatternArray(List list)
    {
        String[] patterns = new String[list.size()];
        
        for(int i=0;i<list.size(); i++)
        {
            patterns[i] = (String)list.get(i);
        }
        return patterns;
    }
    
    private static List<Property> parseProperties(List list) throws PropertyParseException
    {
        List<Property> properties = new ArrayList<>();
        
        for(int i=0;i<list.size(); i++)
        {
            properties.add(Property.parseProperty((String)list.get(i)));
        }
        return properties;
    }
}
