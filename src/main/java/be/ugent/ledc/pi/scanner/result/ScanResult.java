package be.ugent.ledc.pi.scanner.result;

import be.ugent.ledc.pi.property.Property;

public class ScanResult
{
    private Property property;
    private String instance;
    private int startIndex;
    private int endIndex;

    public ScanResult(Property property, String instance, int startIndex, int endIndex)
    {
        this.property = property;
        this.instance = instance;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public String getInstance()
    {
        return instance;
    }

    public void setInstance(String instance)
    {
        this.instance = instance;
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public int getEndIndex()
    {
        return endIndex;
    }

    public void setEndIndex(int endIndex)
    {
        this.endIndex = endIndex;
    }
    
    public boolean subsumedBy(ScanResult other)
    {
        return (other.getStartIndex() <= startIndex && other.getEndIndex() > endIndex) || (other.getStartIndex() < startIndex && other.getEndIndex() >= endIndex);
    }
    
    public boolean subsumes(ScanResult other)
    {
        return other.subsumedBy(this);
    }

    public Property getProperty()
    {
        return property;
    }

    public void setProperty(Property property)
    {
        this.property = property;
    }

    @Override
    public String toString()
    {
        return "ScanResult{" + "property=" + property + ", instance=" + instance + ", startIndex=" + startIndex + ", endIndex=" + endIndex + '}';
    }    
}
