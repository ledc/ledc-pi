package be.ugent.ledc.pi.scanner.instances;

import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.scanner.PropertyScanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A Composed scanner searches for properties that are composed of some other properties.
 * To search, a high-level, regex-like pattern is used, where symbols can be used
 * as placeholders for a set of properties that are allowed in that position of the pattern.
 * @author abronsel
 */
public abstract class ComposedScanner extends PropertyScanner
{
    private final List<SymbolPropertyInfo> symbolPropertyInfo;

    public ComposedScanner(List<SymbolPropertyInfo> symbolPropertyInfo, Property scannableProperty)
    {
        super(scannableProperty);
        this.symbolPropertyInfo = symbolPropertyInfo;
    }
    
    public ComposedScanner(Property scannableProperty)
    {
        this(new ArrayList<>(), scannableProperty);
    }

    public ComposedScanner()
    {
        this(null);
    }

    @Override
    public List<Property> getScannerDependencies()
    {
        return symbolPropertyInfo
            .stream()
            .flatMap(spi -> spi.getProperties().stream())
            .collect(Collectors.toList());
    }

    public List<Property> getAllPropertiesForSymbol(String symbol)
    {
        return symbolPropertyInfo
            .stream()
            .filter(spi -> spi.getSymbol() != null && spi.getSymbol().equals(symbol))
            .flatMap(spi -> spi.getProperties().stream()).collect(Collectors.toList());
    }
    
    public boolean isRequired(String symbol)
    {
        return symbolPropertyInfo
            .stream()
            .anyMatch(spi -> spi.getSymbol() != null && spi.getSymbol().equals(symbol) && spi.isRequired());
    }
    
    public Set<String> getSymbols()
    {
        return symbolPropertyInfo.stream().map(spi -> spi.getSymbol()).collect(Collectors.toSet());
    }

    public List<SymbolPropertyInfo> getSymbolPropertyInfo()
    {
        return symbolPropertyInfo;
    }
}
