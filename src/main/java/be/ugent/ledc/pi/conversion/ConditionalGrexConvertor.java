package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.conversion.ConversionException;
import be.ugent.ledc.pi.conversion.ConversionFeature;
import be.ugent.ledc.pi.conversion.ConversionType;
import be.ugent.ledc.pi.conversion.Convertor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ConditionalGrexConvertor implements Persistable, Convertor<String, String>
{
    private final List<ConditionalConversionStep> conversionSteps;

    public ConditionalGrexConvertor(List<ConditionalConversionStep> conversionSteps)
    {
        this.conversionSteps = conversionSteps;
    }
    
    public ConditionalGrexConvertor()
    {
        this(new ArrayList<>());
    }

    public List<ConditionalConversionStep> getConversionSteps()
    {
        return conversionSteps;
    }

    public void addConditionalConversionStep(ConditionalConversionStep step)
    {
        this.conversionSteps.add(step);
    }
    
    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.conversionSteps);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ConditionalGrexConvertor other = (ConditionalGrexConvertor) obj;
        if (!Objects.equals(this.conversionSteps, other.conversionSteps))
        {
            return false;
        }
        return true;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(
                ConversionFeature.TYPE.getName(),
                ConversionType.GREX_CONDITIONAL.getName())
            .addFeature(
                ConversionFeature.CONVERSION_STEPS.getName(),
                conversionSteps
                    .stream()
                    .map(cs -> cs.buildFeatureMap().getFeatures())
                    .collect(Collectors.toList())
            );
    }

    @Override
    public String convert(String input) throws ConversionException
    {
        for(ConditionalConversionStep ccs: conversionSteps)
        {
            if(ccs.test(input))
                return ccs.convert(input);
        }
        return null;
    }
    
}
