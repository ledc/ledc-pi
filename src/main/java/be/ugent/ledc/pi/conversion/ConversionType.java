package be.ugent.ledc.pi.conversion;

import be.ugent.ledc.core.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ConversionType
{
    GREX_STRING("GrexString", ConversionFeature.TYPE, ConversionFeature.GREX, ConversionFeature.PATTERN),
    GREX_INTEGER("GrexInteger", ConversionFeature.TYPE, ConversionFeature.GREX, ConversionFeature.PATTERN),
    GREX_DOUBLE("GrexDouble", ConversionFeature.TYPE, ConversionFeature.GREX, ConversionFeature.PATTERN),
    GREX_CONDITIONAL("GrexConditional", ConversionFeature.TYPE, ConversionFeature.CONVERSION_STEPS),
    PROPERTY_TRANSFORM("PropertyTransform", ConversionFeature.TYPE, ConversionFeature.INPUT, ConversionFeature.OUTPUT, ConversionFeature.EMBEDDED),
    LINEAR("Linear", ConversionFeature.TYPE, ConversionFeature.A, ConversionFeature.B);

    private final String name;
    
    private final Set<ConversionFeature> requiredFeatures;
    private final Set<ConversionFeature> optionalFeatures;
    
    private ConversionType(String name, Set<ConversionFeature> requiredFeatures, Set<ConversionFeature> optionalFeatures)
    {
        this.name = name;
        this.requiredFeatures = requiredFeatures;
        this.optionalFeatures = optionalFeatures;
    }
    
    private ConversionType(String name, ConversionFeature... requiredFeatures)
    {
        this(name, Stream.of(requiredFeatures).collect(Collectors.toSet()), new HashSet<>());
    }

    public String getName()
    {
        return name;
    }
    
    public static ConversionType parse(String name) throws ParseException
    {
        if(Stream.of(values()).noneMatch(pt -> pt.getName().equals(name)))
            throw new ParseException("Unknown conversion type.");
        
        return Stream.of(values()).filter(pt -> pt.getName().equals(name)).findFirst().get();
    }

    public Set<ConversionFeature> getRequiredFeatures()
    {
        return requiredFeatures;
    }

    public Set<ConversionFeature> getOptionalFeatures()
    {
        return optionalFeatures;
    }
}
