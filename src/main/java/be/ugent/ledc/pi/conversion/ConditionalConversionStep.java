package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.conversion.ConversionFeature;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import java.util.Objects;

/**
 * A conditional conversion step combines a single regex pattern with a grex formula and a grex used for conversion.
 * @author abronsel
 */
public class ConditionalConversionStep implements Persistable
{
    private final String pattern;
    
    private final Grex condition;
    
    private final Grex conversion;

    public ConditionalConversionStep(String pattern, Grex condition, Grex conversion)
    {
        this.pattern = pattern;
        this.condition = condition;
        this.conversion = conversion;
    }

    public boolean test(String data)
    {
        try
        {
            Boolean b =  condition.resolveAsBoolean(pattern, data);
            
            return b == null ? false : b;
        }
        catch (InterpretationException ex) {
            return false;
        }
    }
    
    public String convert(String data)
    {
        try
        {
            return conversion.resolveAsString(pattern, data);
        }
        catch (InterpretationException ex)
        {
            return null;
        }
    }

    public String getPattern() {
        return pattern;
    }

    public Grex getCondition() {
        return condition;
    }

    public Grex getConversion() {
        return conversion;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.pattern);
        hash = 79 * hash + Objects.hashCode(this.condition);
        hash = 79 * hash + Objects.hashCode(this.conversion);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final ConditionalConversionStep other = (ConditionalConversionStep) obj;
        if (!Objects.equals(this.pattern, other.pattern))
        {
            return false;
        }
        if (!Objects.equals(this.condition, other.condition))
        {
            return false;
        }
        if (!Objects.equals(this.conversion, other.conversion))
        {
            return false;
        }
        return true;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(
                ConversionFeature.PATTERN.getName(),
                pattern)
            .addFeature(
                ConversionFeature.CONDITION.getName(),
                condition.getExpression())
            .addFeature(
                ConversionFeature.GREX.getName(),
                conversion.getExpression());
    }  
}
