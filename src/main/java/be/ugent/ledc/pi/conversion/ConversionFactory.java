package be.ugent.ledc.pi.conversion;

import be.ugent.ledc.pi.conversion.grex.ConditionalConversionStep;
import be.ugent.ledc.pi.conversion.grex.ConditionalGrexConvertor;
import be.ugent.ledc.pi.conversion.grex.GrexStringConvertor;
import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.property.Property;
import java.util.List;

public class ConversionFactory
{
    public static PropertyTransform createPropertyTransform(FeatureMap featureMap) throws LedcException
    {
        if(featureMap.getString(ConversionFeature.TYPE.getName()) == null)
            throw new ParseException("Cannot determine the type of convertor. Convertors must include a " + ConversionFeature.TYPE.getName() + ".");
        
        //Get the conversion type
        ConversionType conversionType = ConversionType.parse(featureMap.getString(ConversionFeature.TYPE.getName()));
        
        if(conversionType.getRequiredFeatures().stream().anyMatch(feature -> !featureMap.getFeatures().containsKey(feature.getName())))
            throw new ParseException("Could not reconstruct predicate type " + conversionType.getName() + ". Required fields are missing...");
        
        switch(conversionType)
        {
            case PROPERTY_TRANSFORM:
                return new PropertyTransform(
                    Property.parseProperty(featureMap.getString(ConversionFeature.INPUT.getName())),
                    Property.parseProperty(featureMap.getString(ConversionFeature.OUTPUT.getName())),
                    ConversionFactory.createConvertor(featureMap.getFeatureMap(ConversionFeature.EMBEDDED.getName())));
           
        }
        
        throw new ParseException("Could not parse predicate for unknown reason...");
    }
    
    public static Convertor createConvertor(FeatureMap featureMap) throws ParseException
    {
        if(featureMap.getString(ConversionFeature.TYPE.getName()) == null)
            throw new ParseException("Cannot determine the type of convertor. Convertors must include a " + ConversionFeature.TYPE.getName() + ".");
        
        //Get the conversion type
        ConversionType conversionType = ConversionType.parse(featureMap.getString(ConversionFeature.TYPE.getName()));
        
        if(conversionType.getRequiredFeatures().stream().anyMatch(feature -> !featureMap.getFeatures().containsKey(feature.getName())))
            throw new ParseException("Could not reconstruct predicate type " + conversionType.getName() + ". Required fields are missing...");
        
        switch(conversionType)
        {
            case LINEAR:
                return new LinearConvertor(
                    featureMap.getDouble(ConversionFeature.A.getName()),
                    featureMap.getDouble(ConversionFeature.B.getName()));
            case GREX_STRING:
                return new GrexStringConvertor(
                    new Grex(featureMap.getString(ConversionFeature.GREX.getName())),
                    featureMap.getString(ConversionFeature.PATTERN.getName()));
            case GREX_CONDITIONAL:
                List<FeatureMap> gcm = featureMap.getList(ConversionFeature.CONVERSION_STEPS.getName());
                
                ConditionalGrexConvertor cgc = new ConditionalGrexConvertor();
                
                for(FeatureMap fm: gcm)
                {   
                    cgc.addConditionalConversionStep(
                        new ConditionalConversionStep(
                            fm.getString(ConversionFeature.PATTERN.getName()),
                            new Grex(fm.getString(ConversionFeature.CONDITION.getName())),
                            new Grex(fm.getString(ConversionFeature.GREX.getName()))));
                }
                
                return cgc;  
        }
        
        throw new ParseException("Could not parse convertor for unknown reason...");
    }
}
