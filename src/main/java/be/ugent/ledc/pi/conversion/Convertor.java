package be.ugent.ledc.pi.conversion;

import be.ugent.ledc.core.config.Persistable;

/**
 * A convertor is the generic interface to do inference in the framework of properties.
 * The basic goal of a convertor is to take a data value of type I and convert it to
 * a data value of type O.
 * @author abronsel
 * @param <I> Data type of the input data
 * @param <O> Data type of the output data
 */
public interface Convertor<I, O> extends Persistable
{  
    public O convert(I input) throws ConversionException;
}
