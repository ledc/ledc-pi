package be.ugent.ledc.pi.conversion;

public class ConversionException extends RuntimeException
{
    public ConversionException(String string) {
        super(string);
    }

    public ConversionException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ConversionException(Throwable thrwbl) {
        super(thrwbl);
    }
}
