package be.ugent.ledc.pi.conversion;

import be.ugent.ledc.core.config.FeatureMap;

public class LinearConvertor implements Convertor<Double,Double>
{
    private final double a;
    private final double b;
    
    public LinearConvertor(double a, double b)
    {
        this.a = a;
        this.b = b;
    }

    @Override
    public Double convert(Double input) throws ConversionException
    {
        if(input != null)
            return a*input + b;
        
        return null;
    }

    public double getA()
    {
        return a;
    }

    public double getB()
    {
        return b;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ConversionFeature.TYPE.getName(), ConversionType.LINEAR.getName())
            .addFeature(ConversionFeature.A.getName(), a)
            .addFeature(ConversionFeature.B.getName(), b);
    }
}
