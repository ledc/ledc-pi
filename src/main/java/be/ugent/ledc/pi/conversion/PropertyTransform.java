package be.ugent.ledc.pi.conversion;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.validation.ValidationScheme;
import java.util.function.Function;

/**
 * A PropertyTransform is an implementation of a {@link Convertor} that restrict involved data values to be validate instances of
 a given {@link be.ugent.telin.datatypes.property.Property}. Validation is understood here as being of sufficient quality
 * in terms of the {@link be.ugent.ledc.pi.measure.Measure} that is registered for a {@link be.ugent.telin.datatypes.property.Property}.
 * 
 * The conversion is done by an embeddedConvertor.
 * @author abronsel
 */
public class PropertyTransform implements Persistable, Function<String,String>
{
    private final Property inputProperty;
    
    private final Property outputProperty;
    
    private final Convertor<String, String> embeddedConvertor;

    public PropertyTransform(Property inputProperty, Property outputProperty, Convertor<String, String> embeddedConvertor)
    {
        this.inputProperty = inputProperty;
        this.outputProperty = outputProperty;
        this.embeddedConvertor = embeddedConvertor;
    }

    public Property getInputProperty()
    {
        return inputProperty;
    }

    public Property getOutputProperty()
    {
        return outputProperty;
    }

    public Convertor<String, String> getEmbeddedConvertor()
    {
        return embeddedConvertor;
    }

    public String transform(String input) throws ConversionException
    {
        ValidationScheme vScheme = Registry
            .getInstance()
            .getValidationScheme(inputProperty);
        
        if(vScheme == null)
            throw new ConversionException("No validation scheme for "
                + inputProperty.getCanonicalName());
        
        if(vScheme.getMeasure() == null)
            throw new ConversionException("Validation scheme for "
                + inputProperty.getCanonicalName()
                + " has no measure");
        
        if(vScheme.getConvertor() == null)
            throw new ConversionException("Validation scheme for "
                + inputProperty.getCanonicalName()
                + " has no convertor");
        
        if(!vScheme.validate(input))
            throw new ConversionException("Could not convert " + input
                + ". Cause: insufficient quality for property "
                + inputProperty.getCanonicalName());
        
        return embeddedConvertor.convert(vScheme.getConvertor().convert(input));
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ConversionFeature.TYPE.getName(), ConversionType.PROPERTY_TRANSFORM.getName())
            .addFeature(ConversionFeature.INPUT.getName(), getInputProperty().getCanonicalName())
            .addFeature(ConversionFeature.OUTPUT.getName(), getOutputProperty().getCanonicalName())
            .addFeature(ConversionFeature.EMBEDDED.getName(), getEmbeddedConvertor().buildFeatureMap().getFeatures());
    }

    @Override
    public String apply(String t)
    {
        return transform(t);
    }
}
