package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.conversion.ConversionFeature;
import be.ugent.ledc.pi.conversion.ConversionType;
import be.ugent.ledc.pi.grex.Grex;

public class GrexStringConvertor extends GrexConvertor<String>
{
    public GrexStringConvertor(Grex expression, String pattern)
    {
        super(expression, pattern, (s) -> s);
    }   
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ConversionFeature.TYPE.getName(), ConversionType.GREX_STRING.getName())
            .addFeature(ConversionFeature.PATTERN.getName(), getPattern())
            .addFeature(ConversionFeature.GREX.getName(), getGrex().toString());
    }
}
