package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.conversion.ConversionFeature;
import be.ugent.ledc.pi.conversion.ConversionType;
import be.ugent.ledc.pi.grex.Grex;

public class GrexDoubleConvertor extends GrexConvertor<Double>
{
    public GrexDoubleConvertor(Grex expression, String pattern)
    {
        super(expression, pattern, (s) -> Double.parseDouble(s));
    }   
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ConversionFeature.TYPE.getName(), ConversionType.GREX_DOUBLE.getName())
            .addFeature(ConversionFeature.PATTERN.getName(), getPattern())
            .addFeature(ConversionFeature.GREX.getName(), getGrex().toString());
    }
}

