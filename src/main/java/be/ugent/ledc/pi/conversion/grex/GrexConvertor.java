package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.pi.conversion.ConversionException;
import be.ugent.ledc.pi.conversion.Convertor;
import be.ugent.ledc.pi.grex.PatternManager;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.GrexException;
import be.ugent.ledc.pi.grex.InterpretationException;
import java.util.function.Function;

import java.util.regex.Matcher;

public abstract class GrexConvertor<D> implements Convertor<String, D>
{
    private final Grex grex;
    
    private String pattern;
    
    private final Function<String, D> typeConvertor;

    public GrexConvertor(Grex grex, String pattern, Function<String, D> typeConvertor)
    {
        setPattern(pattern);
        this.grex = grex;
        this.typeConvertor = typeConvertor;
    }
    
    @Override
    public D convert(String givenValue) throws ConversionException
    {
        PatternManager m = PatternManager.getInstance();
        
        //Get the matcher
        Matcher matcher = m.getMatcher(pattern, givenValue);
        
        try
        {
            if(matcher.matches())
            {
                return typeConvertor.apply(grex.resolveAsString(pattern, givenValue));
            }
            
            return null;
        }
        catch (GrexException | InterpretationException ex)
        {
            throw new ConversionException(ex);
        }
    }

    public Grex getGrex()
    {
        return grex;
    }
    public String getPattern()
    {
        return pattern;
    }

    public final void setPattern(String pattern)
    {
        this.pattern = pattern;
        PatternManager.getInstance().registerPattern(pattern);
    }
}
