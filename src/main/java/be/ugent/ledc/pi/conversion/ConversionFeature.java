package be.ugent.ledc.pi.conversion;

public enum ConversionFeature
{
    TYPE("Type"),
    PATTERN("Pattern"),
    GREX("Grex"),
    INPUT("InputProperty"),
    OUTPUT("OutputProperty"),
    EMBEDDED("Convertor"),
    A("A"),
    B("B"),
    CONDITION_MAP("ConditionMap"),
    CONDITION("Condition"),
    CONVERSION_STEPS("ConversionSteps"),
    VALID_AT("ValidAt")
    ;

    private final String name;
    
    private ConversionFeature(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
