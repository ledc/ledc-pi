package be.ugent.ledc.pi.conversion.grex;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.conversion.ConversionFeature;
import be.ugent.ledc.pi.conversion.ConversionType;
import be.ugent.ledc.pi.grex.Grex;

public class GrexIntegerConvertor extends GrexConvertor<Long>
{
    public GrexIntegerConvertor(Grex expression, String pattern)
    {
        super(expression, pattern, (s) -> Long.parseLong(s));
    }   
    
    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(ConversionFeature.TYPE.getName(), ConversionType.GREX_INTEGER.getName())
            .addFeature(ConversionFeature.PATTERN.getName(), getPattern())
            .addFeature(ConversionFeature.GREX.getName(), getGrex().toString());
    }
}
