package be.ugent.ledc.pi;

import be.ugent.ledc.core.LedcException;

public class PiException extends LedcException
{
    public PiException(String message)
    {
        super(message);
    }

    public PiException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PiException(Throwable cause)
    {
        super(cause);
    }

    public PiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
