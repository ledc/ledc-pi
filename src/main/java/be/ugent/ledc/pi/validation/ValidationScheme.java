package be.ugent.ledc.pi.validation;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.conversion.Convertor;
import be.ugent.ledc.pi.measure.Measure;
import be.ugent.ledc.pi.property.Property;
import java.net.URI;

/**
 * A ValidationScheme is a system for validation of persistent identifiers that is robust
 against variants of the identifier instances.
 * @author abronsel
 */
public class ValidationScheme implements Persistable
{
    public static final String MEASURE_FEATURE      = "Measure";
    public static final String CONVERSION_FEATURE   = "Convertor";
    
    public static final String SOURCE_FEATURE       = "Source";
    public static final String PROPERTY_FEATURE     = "Property";
    public static final String THRESHOLD_FEATURE    = "SufficiencyThreshold";
    
    /**
     * The source URI where information to compose this ValidationScheme was found.
     */
    private final URI source;
    
    /**
     * The {@link be.ugent.ledc.pi.property.Property} for which this ValidationScheme applies.
     */
    private final Property property;
        
    /**
     * The level of quality that instances must achieve in order to be considered
     * a valid instance of the property. By default, this is equal to the maximum level.
     */
    private final int sufficiencyThreshold;
    
    /**
     * The measure in the validation scheme
     */
    private final Measure<String> measure;
    
    /**
     * The convertor in the validation scheme
     */
    private final Convertor<String,String> convertor;

    public ValidationScheme(Property property, Measure<String> measure, Convertor<String, String> convertor, URI source, int sufficiencyThreshold) {
        this.source = source;
        this.property = property;
        this.sufficiencyThreshold = sufficiencyThreshold;
        this.measure = measure;
        this.convertor = convertor;
    }
    
    public ValidationScheme(Property property, Measure<String> measure, Convertor<String, String> convertor, URI source) {
        this(
            property,
            measure,
            convertor,
            source,
            measure.getScaleSize()
        );
    }

    public URI getSource()
    {
        return source;
    }

    public Property getProperty()
    {
        return property;
    }

    public int getSufficiencyThreshold()
    {
        return sufficiencyThreshold;
    }

    public Measure<String> getMeasure()
    {
        return measure;
    }

    public Convertor<String, String> getConvertor()
    {
        return convertor;
    }
    
    /**
     * Verifies if the data can be considered a validate instance of the assigned property.
     * @param data The data for which validity is checked.
     * @return Returns true if the measured quality is greater then or equal to
     * the sufficiency threshold (i.e., measure(data) >= sufficiencyThreshold)
     */
    public boolean validate(String data)
    {
        return this.measure.measure(data) >= sufficiencyThreshold;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        FeatureMap featureMap = new FeatureMap()
            .addFeature(PROPERTY_FEATURE, property.getCanonicalName())
            .addFeature(MEASURE_FEATURE, measure.buildFeatureMap().getFeatures())
            .addFeature(CONVERSION_FEATURE, convertor.buildFeatureMap().getFeatures())
            .addFeature(SOURCE_FEATURE, source.toString())
            .addFeature(THRESHOLD_FEATURE, sufficiencyThreshold);
         
        return featureMap;
    }
}
