package be.ugent.ledc.pi.validation;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.conversion.ConversionFactory;
import be.ugent.ledc.pi.conversion.Convertor;
import be.ugent.ledc.pi.measure.Measure;
import be.ugent.ledc.pi.measure.MeasureFactory;
import be.ugent.ledc.pi.property.Property;
import java.net.URI;
import java.net.URISyntaxException;

public class ValidationSchemeFactory
{
    public static ValidationScheme createValidationScheme(FeatureMap fm) throws ParseException, URISyntaxException
    {
        if(fm.getString(ValidationScheme.PROPERTY_FEATURE) == null)
            throw new ParseException("ValidationSchema must declare a Property in the key " + ValidationScheme.PROPERTY_FEATURE + ".");
        
        if(fm.getString(ValidationScheme.SOURCE_FEATURE) == null)
            throw new ParseException("ValidationSchema must declare a source in the key " + ValidationScheme.SOURCE_FEATURE + ".");

        if(fm.getFeatureMap(ValidationScheme.MEASURE_FEATURE) == null)
            throw new ParseException("ValidationSchema must contain a measure in the key " + ValidationScheme.MEASURE_FEATURE + ".");
        
        if(fm.getFeatureMap(ValidationScheme.CONVERSION_FEATURE) == null)
            throw new ParseException("ValidationSchema must contain a convertor in the key " + ValidationScheme.CONVERSION_FEATURE + ".");

        //Read the property
        Property property = Property.parseProperty(fm.getString(ValidationScheme.PROPERTY_FEATURE));
        
        //Read the measure
        Measure measure = MeasureFactory.createMeasure(fm.getFeatureMap(ValidationScheme.MEASURE_FEATURE));
        
        //Read the convertor
        Convertor convertor = ConversionFactory.createConvertor(fm.getFeatureMap(ValidationScheme.CONVERSION_FEATURE));
        
        //Read the source
        URI source = new URI(fm.getString(ValidationScheme.SOURCE_FEATURE));
        
        int threshold = fm.getInteger(ValidationScheme.THRESHOLD_FEATURE) == null
                ? measure.getScaleSize()
                : fm.getInteger(ValidationScheme.THRESHOLD_FEATURE);

        
        return new ValidationScheme(
            property,
            measure,
            convertor,
            source,
            threshold
        );
    }
}
