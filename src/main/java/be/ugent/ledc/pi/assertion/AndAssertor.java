package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.property.PropertyParseException;
import java.util.Set;
import java.util.stream.Collectors;

public class AndAssertor extends AggregateAssertor
{
    public AndAssertor(Set<AbstractAssertor> components, String attribute)
    {
        super(components, attribute);
    }

    @Override
    public DataObject clean(DataObject o) throws PiException
    {
        if(!canClean())
            return o;
        
        for(AbstractAssertor aa: getComponents())
        {
            o = aa.clean(o);
        }
        
        return o;
    }

    @Override
    public boolean test(DataObject o)
    {
        for(AbstractAssertor aa: getComponents())
        {
            if(!aa.test(o))
                return false;
        }
        
        return true;
    }

    @Override
    public Integer quality(DataObject o)
    {
        if(super.scale() == 1)
            return valid(o) ? 1 : 0;
        else
        {
            Integer quality = super.scale();
            
            for(AbstractAssertor aa: getComponents())
            {
                quality = Integer.min(quality, aa.quality(o));
            }
            return quality;
        }
    }

    @Override
    public boolean canClean()
    {
        return getComponents().stream().allMatch(aa -> aa.canClean());
    }

    @Override
    public boolean canValidate()
    {
        return getComponents().stream().allMatch(aa -> aa.canValidate());
    }

    @Override
    public String explain(DataObject o) throws PiException
    {
        if(super.scale() == 1)
            return valid(o)
                ? "Valid"
                : "Invalid value for " +  getAttribute();
        else
        {
            Integer quality = super.scale();
            String message = "Valid";
            for(AbstractAssertor aa: getComponents())
            {
                Integer q = aa.quality(o);
                if(q < quality)
                {
                    message = aa.explain(o);
                    quality = q;
                }
            }
            return message;
        }
    }
    
    @Override
    public String toString()
    {
        return getComponents()
            .stream()
            .map(c -> c.toString())
            .collect(Collectors.joining(" and "));
    }

    @Override
    public AbstractAssertor renameTo(String newAttribute) throws DependencyGraphException, PropertyParseException
    {
        return new AndAssertor(getComponents(), newAttribute);
    }
}
