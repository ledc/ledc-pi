package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.validation.ValidationScheme;

/**
 * This assertion claims that the given attribute should be a valid instance of the asserted property.
 * @author abronsel
 */
public class InstanceAssertor extends AbstractAssertor
{
    public static final String OPERATOR = "instanceof";
    
    private final ValidationScheme validationScheme;
    
    public InstanceAssertor(String attribute, Property property)
    {
        super(attribute, property);
        
        this.validationScheme = Registry
            .getInstance()
            .getValidationScheme(property);
    }
    
    @Override
    /**
     * Returns true if cleaning is possible for this assertion.
     */
    public boolean canClean()
    {
        return validationScheme.getConvertor() != null;
    }
    
    @Override
    public DataObject clean(DataObject o) throws PiException
    {
        //Look for a Property standardizer
        if(validationScheme.getConvertor() == null)
            return o;
        
        if(o.get(getAttribute()) == null)
            return o;
        
        //If available get it and convert
        String converted = validationScheme
            .getConvertor()
            .convert(o.get(getAttribute()).toString());
        
        return new DataObject()
            .concat(o)
            .set(getAttribute(), converted);
    }
    
    @Override
    /**
     * Returns true if validation is possible for this assertion.
     */
    public boolean canValidate()
    {
        return validationScheme.getMeasure() != null;
    }

    @Override
    public boolean test(DataObject o)
    {
        if(validationScheme.getMeasure() == null)
            return true;
        
        Object value = o.get(getAttribute());
        
        return validationScheme.validate(value == null
            ?  null
            : value.toString());
    }

    @Override
    public String toString()
    {
        return getAttribute()
            + " "
            + OPERATOR 
            + " "
            + getProperty().getCanonicalName();
    }
    
    @Override
    public Integer quality(DataObject o)
    {        
        if(validationScheme.getMeasure() == null)
            return 0;
            
        Object value = o.get(getAttribute());
        
        return validationScheme.getMeasure().measure(value == null ?  null : value.toString());
    }
    
    @Override
    public int scale()
    {
        if(validationScheme.getMeasure() == null)
            return 0;
        
        return validationScheme.getMeasure().getScaleSize();
    }

    @Override
    public String explain(DataObject o) throws PiException
    {
        return validationScheme.getMeasure() == null
            ? "Can't explain: no measure found"
            : validationScheme.getMeasure().explain(o.getString(getAttribute()));
    }
    
    @Override
    public AbstractAssertor renameTo(String newAttribute) throws DependencyGraphException, PropertyParseException
    {
        return new InstanceAssertor(newAttribute, getProperty());
    }
}
