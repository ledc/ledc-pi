package be.ugent.ledc.pi.assertion;

import java.util.Objects;
import java.util.Set;

public abstract class AggregateAssertor extends AbstractAssertor
{
    private final Set<AbstractAssertor> components;

    public AggregateAssertor(Set<AbstractAssertor> components, String attribute)
    {
        super(attribute, null);
        this.components = components;
    }

    @Override
    public int scale()
    {
        return components.stream().map(c -> c.scale()).distinct().count() == 1
            ? components
                .stream()
                .map(c -> c.scale())
                .findFirst()
                .get()
            : 1;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.components);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final AggregateAssertor other = (AggregateAssertor) obj;
        if (!Objects.equals(this.components, other.components))
        {
            return false;
        }
        return true;
    }

    public Set<AbstractAssertor> getComponents()
    {
        return components;
    }
}
