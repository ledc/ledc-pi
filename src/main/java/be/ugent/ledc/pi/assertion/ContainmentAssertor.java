package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.execution.ScannerExecutor;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.validation.ValidationScheme;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ContainmentAssertor extends AbstractAssertor
{
    public static final String OPERATOR         = "contains";
    public static final String SINGLE_OPTION    = "single";
    public static final String MULTI_OPTION     = "multiple";

    private final ScannerExecutor executor;
    
    private final boolean singleInstance;
    
     private final ValidationScheme validationScheme;
    
    public ContainmentAssertor(String attribute, Property property, boolean singleInstance) throws DependencyGraphException, PropertyParseException
    {
        super(attribute, property);

        this.validationScheme = Registry.getInstance().getValidationScheme(property);
        this.executor = new ScannerExecutor(property);
        this.singleInstance = singleInstance;
    }
    
    public ContainmentAssertor(String attribute, Property property) throws DependencyGraphException, PropertyParseException
    {
        this(attribute, property, true);
    }

    @Override
    public DataObject clean(DataObject o) throws PiException
    {
        //We get scan results from text
        ResultBuffer results = executor.execute((String)o.get(getAttribute()));
        
        List<?> processed = results
            .getScanResults()
            .stream()
            .filter(sr -> sr.getProperty().equals(super.getProperty()))
            .map(sr -> validationScheme.getConvertor() == null 
                    ? sr.getInstance()
                    : validationScheme.getConvertor().convert(sr.getInstance())
            )
            .filter(v -> v != null)
            .collect(Collectors.toList());

        String newName = getProperty()
            .getName()
            .concat("_in_").concat(getAttribute());
        
        DataObject cleaned = new DataObject().concat(o);
        
        if(singleInstance)
            cleaned.set(newName, processed.isEmpty() ? null : processed.get(0));
        else
        {
            DataObject subObject = new DataObject();
            
            IntStream
            .range(0, processed.size())
            .boxed()
            .forEach(i -> subObject.set("instance_" + i, processed.get(i)));
            
            cleaned.setDataObject(
                    newName,
                    subObject
            );
        }
            
        return cleaned;
    }

    @Override
    public boolean test(DataObject o)
    {
        if(o.get(getAttribute()) == null)
            return false;
        
        try {
            long count = executor
                    .execute(new DataObject()
                            .concat(o)
                            .asString(getAttribute())
                            .getString(getAttribute())
                    )
                    .getScanResults()
                    .stream()
                    .filter(sr -> sr.getProperty().equals(super.getProperty()))
                    .filter(sr -> sr.getInstance() != null)
                    .filter(sr -> !sr.getInstance().isEmpty())
                    .count();
            
            return singleInstance ? count == 1l : count > 0l;
        }
        catch (PropertyScanException ex)
        {
            Logger.getLogger(ContainmentAssertor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }

    @Override
    public String toString()
    {
        return getAttribute()
            + " "
            + OPERATOR
            + " "    
            + (singleInstance ? "single " : "multiple ")
            + getProperty().getCanonicalName();
    }

    @Override
    public Integer quality(DataObject o)
    {
        return test(o) ? 1 : 0;
    }
    
    @Override
    public int scale()
    {
        return 1;
    }
    
    @Override
    public boolean canClean()
    {
        return this.validationScheme.getConvertor() != null;
    }

    @Override
    public boolean canValidate()
    {
        return true;
    }

    @Override
    public String explain(DataObject o) throws PiException
    {
        if(!valid(o))
        {
            return singleInstance
                ? "No unique valid instance found for property " + getProperty()
                : "No valid instances found for property " + getProperty();
        }
        
        return "Valid";
            
    }

    @Override
    public AbstractAssertor renameTo(String newAttribute) throws DependencyGraphException, PropertyParseException
    {
        return new ContainmentAssertor(newAttribute, getProperty(), singleInstance);
    }
}
