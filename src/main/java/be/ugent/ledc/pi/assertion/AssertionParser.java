package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AssertionParser
{
    public static AbstractAssertor parse(String line) throws PiException, PropertyParseException, DependencyGraphException
    {
        if(line.contains(InstanceAssertor.OPERATOR))
        {
            String[] parts = line.split(" " + InstanceAssertor.OPERATOR + " ");
            
            if(parts.length != 2)
                throw new PiException("Could not parse instance assertion from line " + line);
            
            String attribute = parts[0].trim();
            Property p = Property.parseProperty(parts[1]);
            
            return new InstanceAssertor(attribute, p);
        }
        
        if(line.contains(ContainmentAssertor.OPERATOR))
        {
            String[] parts = line.split(" " + ContainmentAssertor.OPERATOR + " ");
            
            if(parts.length != 2)
                throw new PiException("Could not parse containment assertion from line " + line);
            
            String attribute = parts[0].trim();
            
            if(parts[1].trim().startsWith(ContainmentAssertor.SINGLE_OPTION))
            {
                Property p = Property.parseProperty(parts[1]
                    .trim()
                    .replaceFirst(ContainmentAssertor.SINGLE_OPTION, "")
                    .trim());
                
                return new ContainmentAssertor(attribute, p, true);
            }
            if(parts[1].trim().startsWith(ContainmentAssertor.MULTI_OPTION))
            {
                Property p = Property.parseProperty(parts[1]
                    .trim()
                    .replaceFirst(ContainmentAssertor.MULTI_OPTION, "")
                    .trim());
                
                return new ContainmentAssertor(attribute, p, false);
            }
        }
        
        throw new PiException("Could not parse assertion from line " + line);
            
    }
    
    public static PropertyAssertions parse(List<String> lines) throws PiException, PropertyParseException, DependencyGraphException
    {
        Set<AbstractAssertor> assertions = new HashSet<>();
        
        for(String line: lines)
        {
            assertions.add(parse(line));
        }
        return new PropertyAssertions(assertions);
    }
}
