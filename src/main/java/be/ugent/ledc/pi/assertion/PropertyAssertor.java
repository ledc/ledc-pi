package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.rules.Rule;
import be.ugent.ledc.pi.PiException;

public interface PropertyAssertor extends Rule<DataObject>
{
    public DataObject clean(DataObject o) throws PiException;
    
    default public boolean valid(DataObject o)
    {
        return test(o);
    }
    
    public int scale();
    
    public Integer quality(DataObject o);
    
    public boolean canClean();
    
    public boolean canValidate();
    
    public String explain(DataObject o) throws PiException;
    
    public String getAttribute();
}
