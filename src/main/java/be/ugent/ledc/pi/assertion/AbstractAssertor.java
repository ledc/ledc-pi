package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import java.util.Objects;
import java.util.Set;

public abstract class AbstractAssertor implements PropertyAssertor
{
    private final String attribute;
    
    private final Property property;

    public AbstractAssertor(String attribute, Property property)
    {
        this.attribute = attribute;
        this.property = property;
    }

    public abstract AbstractAssertor renameTo(String newAttribute) throws DependencyGraphException, PropertyParseException;
    
    @Override
    public String getAttribute()
    {
        return attribute;
    }

    public Property getProperty()
    {
        return property;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.attribute);
        hash = 47 * hash + Objects.hashCode(this.property);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final AbstractAssertor other = (AbstractAssertor) obj;
        if (!Objects.equals(this.attribute, other.attribute))
        {
            return false;
        }
        if (!Objects.equals(this.property, other.property))
        {
            return false;
        }
        return true;
    }

    @Override
    public Set<String> getInvolvedAttributes() {
        return SetOperations.set(attribute);
    }
    
}
