package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import java.util.Set;

public class PropertyAssertions extends RuleSet<DataObject, AbstractAssertor>
{

    public PropertyAssertions(Set<AbstractAssertor> rules) {
        super(rules);
    }

    public PropertyAssertions(AbstractAssertor... rules) {
        super(rules);
    }
    
}
