package be.ugent.ledc.pi.assertion;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.property.PropertyParseException;
import java.util.Set;
import java.util.stream.Collectors;

public class OrAssertor extends AggregateAssertor
{
    public OrAssertor(Set<AbstractAssertor> components, String attribute)
    {
        super(components, attribute);
    }

    @Override
    public DataObject clean(DataObject o) throws PiException
    {
        for(AbstractAssertor aa: getComponents())
        {
            if(aa.canClean() && aa.valid(o))
            {
                o = aa.clean(o);
            }
        }
        
        return o;
    }

    @Override
    public boolean test(DataObject o)
    {
        for(AbstractAssertor aa: getComponents())
        {
            if(aa.test(o))
                return true;
        }
        
        return false;
    }

    @Override
    public Integer quality(DataObject o)
    {
        if(super.scale() == 1)
            return valid(o) ? 1 : 0;
        else
        {
            Integer quality = 0;
            
            for(AbstractAssertor aa: getComponents())
            {
                quality = Integer.max(quality, aa.quality(o));
            }
            return quality;
        }
    }

    @Override
    public boolean canClean()
    {
        return getComponents().stream().anyMatch(aa -> aa.canClean());
    }

    @Override
    public boolean canValidate()
    {
        return getComponents().stream().anyMatch(aa -> aa.canValidate());
    }

    @Override
    public String explain(DataObject o) throws PiException
    {
        if(super.scale() == 1)
            return valid(o)
                ? "Valid"
                : "Invalid value for " +  getAttribute();
        else
        {
            Integer quality = 0;
            String message = "Invalid value for " +  getAttribute();
            for(AbstractAssertor aa: getComponents())
            {
                Integer q = aa.quality(o);
                if(q > quality)
                {
                    message = aa.explain(o);
                    quality = q;
                }
            }
            return message;
        }
    }

    @Override
    public String toString()
    {
        return getComponents()
            .stream()
            .map(c -> c.toString())
            .collect(Collectors.joining(" or "));
    }

    @Override
    public AbstractAssertor renameTo(String newAttribute) throws DependencyGraphException, PropertyParseException
    {
        return new OrAssertor(getComponents(), newAttribute);
    }
}
