package be.ugent.ledc.pi.measure;

import be.ugent.ledc.pi.property.Property;
import java.util.List;

public interface MeasureDependency
{
    public List<Property> getMeasureDependencies();
}
