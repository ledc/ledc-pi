package be.ugent.ledc.pi.measure.predicates;

public enum PredicateFeature
{
    TYPE("Type"),
    PATTERN("Pattern"),
    GREX("Grex"),
    COMPONENTS("Components"),
    PROPERTY("Property"),
    DURING("During"),
    EXPLANATION("Explanation");
    
    private final String name;
    
    private PredicateFeature(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
}
