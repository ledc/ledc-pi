package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.validation.ValidationScheme;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A special type of GrexPredicate where the Grex is not directly evaluated to 
 * a Boolean, but to a string. The resulting string value is then tested to be
 * a valid instance of another property by means of a dedicated measure for
 * that property.
 * @author abronsel
 */
public class InstancePredicate extends GrexPredicate
{
    private final Property propertyToTest;
    
    public InstancePredicate(Grex grex, String pattern, Property propertyToTest)
    {
        super(grex, pattern);
        this.propertyToTest = propertyToTest;
    }

    public InstancePredicate(Grex grex, String pattern, Property propertyToTest, String explanation)
    {
        super(grex, pattern, explanation);
        this.propertyToTest = propertyToTest;
    }

    @Override
    public boolean test(String data)
    {
        if(data == null)
            return false;

        try
        {
            String instanceToTest = getGrex().resolveAsString(getPattern(), data);
            
            //Retrieve the quality measure for the property
            Registry reg = Registry.getInstance();

            ValidationScheme scheme = reg.getValidationScheme(propertyToTest);
                
            return scheme.validate(instanceToTest);
        }
        catch(InterpretationException | NumberFormatException ex)
        {
            return false;
        }
    }

    @Override
    public String explain(String data)
    {
        if(data == null)
            return "Null value";
        
        try
        {
            boolean test = test(data);
            if(test)
                return "Ok";
            else if(getExplanation() != null && !getExplanation().isEmpty())
                return getExplanation();
            else
                return  "Not a valid instance of " + propertyToTest;
        }
        catch(NumberFormatException ex)
        {
            return "Grex '" + getGrex().toString() + "' induces a NumberFormat exception.";
        }
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return super
            .buildFeatureMap()
            .addFeature(PredicateFeature.TYPE.getName(), PredicateType.INSTANCE.getName())
            .addFeature(PredicateFeature.PROPERTY.getName(), propertyToTest.getCanonicalName());
    }

    @Override
    public List<Property> getMeasureDependencies()
    {
        return Stream
            .of(propertyToTest)
            .collect(Collectors.toList());
    }
}
