package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import be.ugent.ledc.pi.grex.PatternManager;

/**
 * A GrexPredicate is a the cornerstone of ledc-pi. It performs a regex match test
 * and an additional test in terms of groups identified in the regex pattern.
 * @author abronsel
 */
public class GrexPredicate implements Predicate<String>
{   
    private final Grex grex;
    
    /**
     * The regex pattern that to test String values against.
     */
    private String pattern;
    
    /**
     * A human readable explanation of the pattern
     */
    private String explanation;

    public GrexPredicate(Grex grex, String pattern)
    {
        this(grex, pattern, null);
    }

    public GrexPredicate(Grex grex, String pattern, String explanation)
    {
        setPattern(pattern);
        this.explanation = explanation;
        this.grex = grex;
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(PredicateFeature.TYPE.getName(), PredicateType.GREX.getName())
            .addFeature(PredicateFeature.PATTERN.getName(), getPattern())
            .addFeature(PredicateFeature.GREX.getName(), grex.toString())
            .addFeatureWhenNotEmpty(PredicateFeature.EXPLANATION.getName(), getExplanation());
    }

    public String getPattern()
    {
        return pattern;
    }

    public final void setPattern(String pattern)
    {
        //Set the pattern
        this.pattern = pattern;
        
        //Register the regex Pattern
        PatternManager.getInstance().registerPattern(pattern);
    }

    public String getExplanation()
    {
        return explanation;
    }

    public void setExplanation(String explanation)
    {
        this.explanation = explanation;
    }
    
    @Override
    public String explain(String data)
    {
        if(data == null)
            return "Null value";
        
        try
        {
            Boolean test = grex.resolveAsBoolean(getPattern(), data);
            
            if(test)
                return "Ok";
            else if(getExplanation() != null && !getExplanation().isEmpty())
                return getExplanation();
            else
                return  "Failure to satisfy group expression " + grex.toString();
        }
        catch(InterpretationException ex)
        {
            return "Grex '" + grex.toString() + "' cannot be resolved.";
        }
        catch(NumberFormatException ex)
        {
            return "Grex '" + grex.toString() + "' induces a NumberFormat exception.";
        }
    }

    @Override
    public boolean test(String data)
    {
        if(data == null)
            return false;

        try
        {
            Boolean test = grex.resolveAsBoolean(getPattern(), data);
            return test != null && test;
        }
        catch(InterpretationException | NumberFormatException ex)
        {
            return false;
        }
    }
    
    public Grex getGrex()
    {
        return grex;
    }
}
