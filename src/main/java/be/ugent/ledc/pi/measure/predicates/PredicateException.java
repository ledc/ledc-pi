package be.ugent.ledc.pi.measure.predicates;

public class PredicateException extends RuntimeException
{
    public PredicateException(String message)
    {
        super(message);
    }

    public PredicateException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public PredicateException(Throwable cause)
    {
        super(cause);
    }
}