package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;

/**
 * A predicate that tests if data is not NULL.
 * This predicate will return true if the tested data is not NULL.
 * NotNullPredicates are useful as a first test in a {@link be.ugent.telin.propagent.measures.QualityMeasure}.
 * @author abronsel
 * @param <T> The type of data on which this predicate can be applied.
 */
public class NotNullPredicate<T> implements Predicate<T>
{
    public NotNullPredicate(){}
    
    @Override
    /**
     * Returns true if data is not NULL.
     */
    public boolean test(T data)
    {
        return data != null;
    }

    @Override
    public String explain(T data)
    {
        return test(data) ? "Ok" : "Null value";
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap().addFeature(PredicateFeature.TYPE.getName(), PredicateType.NOT_NULL.getName());
    }
}
