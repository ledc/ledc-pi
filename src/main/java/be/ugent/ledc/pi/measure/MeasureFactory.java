package be.ugent.ledc.pi.measure;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.measure.predicates.PredicateFactory;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MeasureFactory
{
    public static Measure createMeasure(FeatureMap fm) throws ParseException, URISyntaxException
    {
        if(fm.getList(Measure.PREDICATES_FEATURE) == null)
            throw new ParseException("QualityMeasure must declare a list of predicates in the key " + Measure.PREDICATES_FEATURE + ".");
        
        List list = fm.getList(Measure.PREDICATES_FEATURE);
        
        List<Predicate> predicates = new ArrayList<>();
        
        //Parse predicates
        for(Object o: list)
        {
            predicates.add(PredicateFactory.createPredicate((FeatureMap) o));
        }

        return new Measure(predicates);
    }
}
