package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum PredicateType
{
    NOT_NULL("NotNull", PredicateFeature.TYPE),
    PATTERN("Pattern",
        Stream.of(PredicateFeature.TYPE, PredicateFeature.PATTERN).collect(Collectors.toSet()),
        Stream.of(PredicateFeature.EXPLANATION).collect(Collectors.toSet())),
    DISJUNCTIVE("Or",
        Stream.of(PredicateFeature.TYPE, PredicateFeature.COMPONENTS).collect(Collectors.toSet()),    
        Stream.of(PredicateFeature.EXPLANATION).collect(Collectors.toSet())),
    CONJUNCTIVE("And",
        Stream.of(PredicateFeature.TYPE, PredicateFeature.COMPONENTS).collect(Collectors.toSet()),    
        Stream.of(PredicateFeature.EXPLANATION).collect(Collectors.toSet())),
    GREX("Grex",
        Stream.of(PredicateFeature.TYPE, PredicateFeature.PATTERN, PredicateFeature.GREX).collect(Collectors.toSet()),
        Stream.of(PredicateFeature.EXPLANATION).collect(Collectors.toSet())),
    INSTANCE("Instance",
        Stream.of(PredicateFeature.TYPE, PredicateFeature.PATTERN, PredicateFeature.GREX, PredicateFeature.PROPERTY).collect(Collectors.toSet()),
        Stream.of(PredicateFeature.EXPLANATION).collect(Collectors.toSet()));
    
    private final String name;
    
    private final Set<PredicateFeature> requiredFeatures;
    private final Set<PredicateFeature> optionalFeatures;
    
    private PredicateType(String name, Set<PredicateFeature> requiredFeatures, Set<PredicateFeature> optionalFeatures)
    {
        this.name = name;
        this.requiredFeatures = requiredFeatures;
        this.optionalFeatures = optionalFeatures;
    }
    
    private PredicateType(String name, PredicateFeature... requiredFeatures)
    {
        this(name, Stream.of(requiredFeatures).collect(Collectors.toSet()), new HashSet<>());
    }

    public String getName()
    {
        return name;
    }
    
    public static PredicateType parse(String name) throws ParseException
    {
        if(Stream.of(values()).noneMatch(pt -> pt.getName().equals(name)))
            throw new ParseException("Unknown predicate type.");
        
        return Stream.of(values()).filter(pt -> pt.getName().equals(name)).findFirst().get();
    }

    public Set<PredicateFeature> getRequiredFeatures()
    {
        return requiredFeatures;
    }

    public Set<PredicateFeature> getOptionalFeatures()
    {
        return optionalFeatures;
    }
}
