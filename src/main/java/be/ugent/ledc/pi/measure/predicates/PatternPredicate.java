package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.Grex;

/**
 * A predicate that applies on a {@link java.lang.String} and tests if the given String value matches a regex pattern.
 * This predicate will return true if the tested data matches the regex encoded in this predicate.
 * @author abronsel
 */
public class PatternPredicate extends GrexPredicate
{

    public PatternPredicate(String pattern) {
        super(new Grex("true"), pattern);
    }

    public PatternPredicate(String pattern, String explanation) {
        super(new Grex("true"), pattern, explanation);
    }
    
    @Override
    public String explain(String data)
    {
        if(test(data))
            return "Ok";
        
        if(getExplanation() != null && !getExplanation().isEmpty())
            return getExplanation();
        
        return "Failure to match pattern " + getPattern();
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(PredicateFeature.TYPE.getName(), PredicateType.PATTERN.getName())
            .addFeature(PredicateFeature.PATTERN.getName(), getPattern())
            .addFeatureWhenNotEmpty(PredicateFeature.EXPLANATION.getName(), getExplanation());
    }
}
