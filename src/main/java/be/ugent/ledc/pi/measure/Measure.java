package be.ugent.ledc.pi.measure;

import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.measure.predicates.Predicate;
import be.ugent.ledc.pi.property.Property;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * A QualityMeasure implements a simple measurement procedure that induces an ordinal scale for measurement.
 * Based on the ideas coined in the paper "A measure-theoretic foundation for data quality",
 * it assumes a total order on a set of predicates.
 * 
 * Predicates will be checked in the given order and as more test succeed, quality will be higher.
 * The main type of predicate is based on group expressions, which are formulas
 * written on top of a regular expression.
 * @author abronsel
 * @param <T> The type of data on which this measure is applied.
 */
public class Measure<T> implements Persistable, MeasureDependency
{
    public static final String PREDICATES_FEATURE = "Predicates";
    /**
     * The ordered sequence of predicates used by this measure to determine the quality of data.
     */
    private final List<Predicate<T>> predicates;

    /**
     * Main constructor for QualityMeasure.
     * @param predicates The list of predicates on which this measure relies. Predicates are executed in the given order.
     */
    public Measure(List<Predicate<T>> predicates)
    {
        this.predicates = predicates;
    }
    
    /**
     * Returns the maximum level of quality that can be obtained by this QualityMeasure.
     * This number is equal to the number of predicates used by this measure.
     * @return 
     */
    public int getScaleSize()
    {
        return predicates.size();
    }

    /**
     * Measures the quality of data by evaluating all predicates in order.
     * The level of quality is the index of the first predicate the fails or,
     * if all predicates succeed, the number of predicates.
     * 
     * Note: although this method returns an integer, the measurement procedure
     * yields an ordinal scale and arithmetic on the returned levels is to be avoided.
     * @param data The data for which quality is measured.
     * @return The measured level of quality on an ordinal scale from 0 to the number of predicates.
     */
    public int measure(T data)
    {
        for(int i=0; i<predicates.size();i++)
        {
            if(!predicates.get(i).test(data))
                return i;
        }
        
        return predicates.size();
    }
    
    /**
     * Provides a textual description of the state of this data. If predicates fail,
     * the description will be a description of the failure of the first failed predicate.
     * @param data The data for which the quality level must be explained.
     * @return A textual description of the quality of this data.
     */
    public String explain(T data)
    {
        int quality = measure(data);
        
        if(quality == getScaleSize())
            return "All predicates are satisfied!";
        
        return predicates.get(quality).explain(data);     
    }
    
    public List<Predicate<T>> getPredicates()
    {
        return new ArrayList<>(predicates);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.predicates);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Measure<?> other = (Measure<?>) obj;
        return Objects.equals(this.predicates, other.predicates);
    }


    @Override
    public FeatureMap buildFeatureMap()
    {
        FeatureMap featureMap = new FeatureMap()
            .addFeature(
                PREDICATES_FEATURE,
                predicates
                    .stream()
                    .map(p -> p.buildFeatureMap().getFeatures())
                    .collect(Collectors.toCollection(ArrayList::new))
            );
         
        return featureMap;
    }

    @Override
    public List<Property> getMeasureDependencies()
    {
        return predicates
            .stream()
            .flatMap(p -> p
                .getMeasureDependencies()
                .stream())
            .collect(Collectors.toList());
    }
}
