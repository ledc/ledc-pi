package be.ugent.ledc.pi.measure.predicates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * An aggregate predicate represents a Boolean connective of a set of other predicates.
 * @author abronsel
 * @param <T> The type of data on which this predicate can be applied.
 */
public abstract class AggregatePredicate<T> implements Predicate<T>
{
    private List<Predicate<T>> components;
    
    /**
     * A human readable explanation of the pattern
     */
    private String explanation;

    public AggregatePredicate(List<Predicate<T>> components)
    {
        this.components = components;
    }
    
    public AggregatePredicate(Predicate<T> ... components)
    {
        this.components = new ArrayList<>(Arrays.asList(components));
    }

    public AggregatePredicate(){}

    public List<Predicate<T>> getComponents()
    {
        return components;
    }

    public void setComponents(List<Predicate<T>> components)
    {
        this.components = components;
    }
    
    public String getExplanation()
    {
        return explanation;
    }

    public void setExplanation(String explanation)
    {
        this.explanation = explanation;
    }
}
