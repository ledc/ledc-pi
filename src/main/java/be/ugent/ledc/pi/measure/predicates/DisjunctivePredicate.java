package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An aggregate predicate that represents a Boolean disjunction (OR) of a set of given predicates.
 * The test of this predicate will be true if at least one component predicate yield true.
 * @author abronsel
 * @param <T> The type of data on which this predicate can be applied.
 */
public class DisjunctivePredicate<T> extends AggregatePredicate<T>
{
    public DisjunctivePredicate(List<Predicate<T>> components)
    {
        super(components);
    }

    public DisjunctivePredicate(Predicate<T>... components)
    {
        super(components);
    }

    public DisjunctivePredicate(){}
    
    @Override
    /**
     * Returns true if any of the component predicates return true.
     */
    public boolean test(T data)
    {
        for(Predicate<T> p: getComponents())
        {
            if(p.test(data))
                return true;
        } 
        return false;
    }

    @Override
    public String explain(T data)
    {
        if(test(data))
            return "Ok";
        
        if(getExplanation() != null && !getExplanation().isEmpty())
            return getExplanation();
        
        return "None of the conditions in this disjunction are satisfied.";
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(PredicateFeature.TYPE.getName(), PredicateType.DISJUNCTIVE.getName())
            .addFeature(
                PredicateFeature.COMPONENTS.getName(),
                getComponents()
                    .stream()
                    .map(c -> c.buildFeatureMap().getFeatures())
                    .collect(Collectors.toCollection(ArrayList::new)))
            .addFeatureWhenNotEmpty(PredicateFeature.EXPLANATION.getName(), getExplanation());
    }

}
