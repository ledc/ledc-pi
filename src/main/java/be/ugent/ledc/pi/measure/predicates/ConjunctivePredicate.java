package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.FeatureMap;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An aggregate predicate that represents a Boolean conjunction (AND) of a set of given predicates.
 * The test of this predicate will be true if all component predicates yield true.
 * @author abronsel
 * @param <T> The type of data on which this predicate can be applied.
 */
public class ConjunctivePredicate<T> extends AggregatePredicate<T>
{
    public ConjunctivePredicate(List<Predicate<T>> components)
    {
        super(components);
    }

    public ConjunctivePredicate(Predicate<T>... components)
    {
        super(components);
    }

    public ConjunctivePredicate(){}
    
    @Override
    /**
     * Returns true if all component predicates return true.
     */
    public boolean test(T data)
    {
        return getComponents().stream().allMatch(p -> p.test(data));
    }
    
    @Override
    public String explain(T data)
    {
        if(test(data))
            return "Ok";
        
        if(getExplanation() != null && !getExplanation().isEmpty())
            return getExplanation();
        
        return "Some condition in this conjunction is not satisfied.";
    }

    @Override
    public FeatureMap buildFeatureMap()
    {
        return new FeatureMap()
            .addFeature(PredicateFeature.TYPE.getName(), PredicateType.CONJUNCTIVE.getName())
            .addFeature(
                PredicateFeature.COMPONENTS.getName(),
                getComponents()
                    .stream()
                    .map(c -> c.buildFeatureMap().getFeatures())
                    .collect(Collectors.toCollection(ArrayList::new)))
            .addFeatureWhenNotEmpty(PredicateFeature.EXPLANATION.getName(), getExplanation());
    }
}