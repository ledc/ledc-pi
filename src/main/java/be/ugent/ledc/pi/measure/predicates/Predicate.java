package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.config.Persistable;
import be.ugent.ledc.pi.property.Property;
import java.util.ArrayList;
import java.util.List;
import be.ugent.ledc.pi.measure.MeasureDependency;

/**
 * A predicate poses a basic test for a data item. It is the basic building block from which quality measures are built.
 * @author abronsel
 * @param <T> The type of data on which this predicate can be applied.
 */
public interface Predicate<T> extends java.util.function.Predicate<T>, Persistable, MeasureDependency
{
    public String explain(T data);

    @Override
    public default List<Property> getMeasureDependencies()
    {
        return new ArrayList<>();
    }
}

