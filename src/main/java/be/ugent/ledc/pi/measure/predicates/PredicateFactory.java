package be.ugent.ledc.pi.measure.predicates;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.property.Property;
import java.util.ArrayList;
import java.util.List;

public class PredicateFactory
{
    public static Predicate createPredicate(FeatureMap featureMap) throws ParseException
    {
        if(featureMap.getString(PredicateFeature.TYPE.getName()) == null)
            throw new ParseException("Cannot determine the type of predicate. Predicates must include a " + PredicateFeature.TYPE.getName() + ".");
        
        //Get the predicate type
        PredicateType predicateType = PredicateType.parse(featureMap.getString(PredicateFeature.TYPE.getName()));
        
        if(predicateType.getRequiredFeatures().stream().anyMatch(feature -> !featureMap.getFeatures().containsKey(feature.getName())))
            throw new ParseException("Could not reconstruct predicate type " + predicateType.getName() + ". Required fields are missing...");
        
        switch(predicateType)
        {
            case NOT_NULL:
                return new NotNullPredicate<>();
            case PATTERN:
                PatternPredicate patternPredicate = new PatternPredicate(featureMap.getString(PredicateFeature.PATTERN.getName()));
                if(featureMap.getString(PredicateFeature.EXPLANATION.getName()) != null)
                    patternPredicate.setExplanation(featureMap.getString(PredicateFeature.EXPLANATION.getName()));
                return patternPredicate;
            case GREX:
                GrexPredicate grexPredicate = new GrexPredicate(
                    new Grex(featureMap.getString(PredicateFeature.GREX.getName())),
                    featureMap.getString(PredicateFeature.PATTERN.getName()));
                if(featureMap.getString(PredicateFeature.EXPLANATION.getName()) != null)
                    grexPredicate.setExplanation(featureMap.getString(PredicateFeature.EXPLANATION.getName()));
                return grexPredicate;
            case INSTANCE:
                InstancePredicate instancePredicate = new InstancePredicate(
                    new Grex(featureMap.getString(PredicateFeature.GREX.getName())),
                    featureMap.getString(PredicateFeature.PATTERN.getName()),
                    Property.parseProperty(featureMap.getString(PredicateFeature.PROPERTY.getName()))    
                );
                if(featureMap.getString(PredicateFeature.EXPLANATION.getName()) != null)
                    instancePredicate.setExplanation(featureMap.getString(PredicateFeature.EXPLANATION.getName()));
                return instancePredicate;
            case CONJUNCTIVE:
                List conjunctiveObjectList = featureMap.getList(PredicateFeature.COMPONENTS.getName());
                List<Predicate> conjuncts = new ArrayList<>();
                for(Object o: conjunctiveObjectList)
                {
                    conjuncts.add(PredicateFactory.createPredicate((FeatureMap)o));
                }
                ConjunctivePredicate cp = new ConjunctivePredicate(conjuncts);
                
                if(featureMap.getString(PredicateFeature.EXPLANATION.getName()) != null)
                    cp.setExplanation(featureMap.getString(PredicateFeature.EXPLANATION.getName()));
                
                return cp;
            case DISJUNCTIVE:
                List disjunctiveObjectList = featureMap.getList(PredicateFeature.COMPONENTS.getName());
                List<Predicate> disjuncts = new ArrayList<>();
                for(Object o: disjunctiveObjectList)
                {
                    disjuncts.add(PredicateFactory.createPredicate((FeatureMap)o));
                }
                
                DisjunctivePredicate dp = new DisjunctivePredicate(disjuncts);
                
                if(featureMap.getString(PredicateFeature.EXPLANATION.getName()) != null)
                    dp.setExplanation(featureMap.getString(PredicateFeature.EXPLANATION.getName()));        
                
                return dp;
        }
        
        throw new ParseException("Could not parse predicate for unknown reason...");
    }
}
