package be.ugent.ledc.pi.test.operators;

import be.ugent.ledc.pi.grex.PostfixParser;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BranchTest
{
    public BranchTest(){}

    @Test
    public void testBranchAnd()
    {
        assertEquals("true", PostfixParser.getInstance().resolve("::int 10 branch& 5 >= 20 <="));
        assertEquals("true", PostfixParser.getInstance().resolve("::int 10 branch& 5 >= 5 + 15 ="));
    }
    
    @Test
    public void testBranchOr()
    {
        assertEquals("true", PostfixParser.getInstance().resolve("::int 10 branch| 2 20 between 5 50 between"));
        assertEquals("false", PostfixParser.getInstance().resolve("::int 10 branch| 2 8 between 20 50 between"));
    }
}
