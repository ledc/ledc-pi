package be.ugent.ledc.pi.test.operators;

import be.ugent.ledc.pi.grex.PostfixParser;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * The purpose of these tests is to verify that the default operators are still working as intended
 * @author abronsel
 */
public class IntegerOperatorTest
{
    public IntegerOperatorTest(){}

    @Test
    public void testIntegerAddition()
    {       
        assertEquals("323", PostfixParser.getInstance().resolve("::int 123 200 +"));
    }
    
    @Test
    public void testIntegerSubtraction()
    {        
        assertEquals("-77", PostfixParser.getInstance().resolve("::int 123 200 -"));
    }
    
    @Test
    public void testIntegerMultiplication()
    {        
        assertEquals("45", PostfixParser.getInstance().resolve("::int 5 9 *"));
    }
    
    @Test
    public void testIntegerDivision()
    {        
        assertEquals("11", PostfixParser.getInstance().resolve("::int 121 11 /"));
    }
    
    @Test
    public void testIntegerModulo()
    {        
        assertEquals("3", PostfixParser.getInstance().resolve("::int 11 4 %"));
    }
    
    @Test
    public void testIntegerAbsoluteValue()
    {        
        assertEquals("153", PostfixParser.getInstance().resolve("::int -153 abs"));
    }
    
    @Test
    public void testIntegerFaculty()
    {        
        assertEquals("24", PostfixParser.getInstance().resolve("::int 4 !"));
    }
    
    @Test
    public void testIntegerPower()
    {        
        assertEquals("256", PostfixParser.getInstance().resolve("::int 2 8 ^"));
    }
    
    @Test
    public void testIntegerBitshiftRight()
    {        
        assertEquals("5", PostfixParser.getInstance().resolve("::int 20 2 >>"));
    }
    
    @Test
    public void testIntegerBitshiftLeft()
    {        
        assertEquals("80", PostfixParser.getInstance().resolve("::int 20 2 <<"));
    }
    
    @Test
    public void testIntegerBitwiseAnd()
    {        
        assertEquals("4", PostfixParser.getInstance().resolve("::int 20 5 &"));
    }
    
    @Test
    public void testIntegerBitwiseOr()
    {        
        assertEquals("21", PostfixParser.getInstance().resolve("::int 20 5 |"));
    }
    
    @Test
    public void testIntegerBitwiseNot()
    {        
        assertEquals("-21", PostfixParser.getInstance().resolve("::int 20 ~"));
    }
    
    @Test
    public void testIntegerDihedral5()
    {        
        assertEquals("2", PostfixParser.getInstance().resolve("::int 3 4 D5"));
        assertEquals("9", PostfixParser.getInstance().resolve("::int 7 3 D5"));
        assertEquals("7", PostfixParser.getInstance().resolve("::int 4 8 D5"));
    }
    
    @Test
    public void testIntegerWeightedSumWithRepeatedPattern()
    {        
        assertEquals("22", PostfixParser.getInstance().resolve("::int 156 1,3 w+"));
        assertEquals("56", PostfixParser.getInstance().resolve("::int 1469 3,5,1 w+"));
    }
    
    @Test
    public void testIntegerWeightedSumWithPowerWeights()
    {        
        assertEquals("210", PostfixParser.getInstance().resolve("::int 1469 1->2^n w+"));
        assertEquals("105", PostfixParser.getInstance().resolve("::int 1469 0->2^n w+"));
        assertEquals("90", PostfixParser.getInstance().resolve("::int 1469 2^n<-1 w+"));
        assertEquals("45", PostfixParser.getInstance().resolve("::int 1469 2^n<-0 w+"));
    }
    
    @Test
    public void testIntegerWeightedSumWithLinearWeights()
    {        
        assertEquals("103", PostfixParser.getInstance().resolve("::int 1469 3->n w+"));
        assertEquals("17", PostfixParser.getInstance().resolve("::int 1469 3<-n w+"));
        assertEquals("77", PostfixParser.getInstance().resolve("::int 1469 n<-3 w+"));
        assertEquals("43", PostfixParser.getInstance().resolve("::int 1469 n->3 w+"));
    }
    
    @Test
    public void testIntegerLuhnPermutation()
    {        
        assertEquals("8", PostfixParser.getInstance().resolve("::int 4 luhn"));
        assertEquals("9", PostfixParser.getInstance().resolve("::int 9 luhn"));
        assertEquals("1", PostfixParser.getInstance().resolve("::int 5 luhn"));
    }
    
    @Test
    public void testIntegerVerhoeffPermutation()
    {        
        assertEquals("9", PostfixParser.getInstance().resolve("::int 8 verhoeff"));
        assertEquals("1", PostfixParser.getInstance().resolve("::int 0 verhoeff"));
        assertEquals("3", PostfixParser.getInstance().resolve("::int 6 verhoeff"));
    }
    
    @Test
    public void testIntegerVerhoeffPowerPermutation()
    {        
        assertEquals("2", PostfixParser.getInstance().resolve("::int 8 3 verhoeff*"));
        assertEquals("5", PostfixParser.getInstance().resolve("::int 0 2 verhoeff*"));
        assertEquals("3", PostfixParser.getInstance().resolve("::int 6 5 verhoeff*"));
    }
    
    @Test
    public void testIntegerSmallerThan()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("::int 2 2 <"));
    }
    
    @Test
    public void testIntegerSmallerThanOrEqualTo()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("::int 2 2 <="));
    }
    
    @Test
    public void testIntegerEqual()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("::int 2 2 ="));
    }
    
    @Test
    public void testIntegerNotEqual()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("::int 2 2 !="));
    }
    
    @Test
    public void testIntegerGreaterThanOrEqualTo()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("::int 2 2 >="));
    }
    
    @Test
    public void testIntegerGreaterThan()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("::int 2 2 >"));
    }
    
    @Test
    public void testIntegerBetween()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("::int 2 2 5 between"));
        assertEquals("false", PostfixParser.getInstance().resolve("::int 1 2 5 between"));
    }
    
    @Test
    public void testIntegerBetweenX()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("::int 2 2 5 betweenx"));
        assertEquals("true", PostfixParser.getInstance().resolve("::int 3 2 5 betweenx"));
    }
}
