package be.ugent.ledc.pi.test.operators;

import be.ugent.ledc.pi.grex.GrexParseException;
import be.ugent.ledc.pi.grex.PostfixParser;
import org.junit.Test;
import static org.junit.Assert.*;

public class StringOperatorTest
{
    public StringOperatorTest(){}

    @Test
    public void testStringConcat()
    {        
        assertEquals("HiThere", PostfixParser.getInstance().resolve("Hi There ||"));
    }
    
    @Test
    public void testStringTrim()
    {        
        assertEquals("Hello", PostfixParser.getInstance().resolve("\" Hello \" trim"));
    }
    
    @Test
    public void testCasing()
    {        
        assertEquals("hello", PostfixParser.getInstance().resolve("Hello lowercase"));
        assertEquals("HELLO", PostfixParser.getInstance().resolve("Hello uppercase"));
    }
    
    @Test
    public void testSpaceCompress()
    {        
        assertEquals(
            " hi my name is Jimmy ",
            PostfixParser
                .getInstance()
                .resolve("\"  hi my   name is   Jimmy  \" spacecomp"));
        
    }
    
    @Test
    public void testReverse()
    {        
        assertEquals("olleH", PostfixParser.getInstance().resolve("Hello reverse"));
    }
    
    @Test
    public void testCaesarCoding()
    {        
        assertEquals("123", PostfixParser.getInstance().resolve("abc cc:a->1"));
        assertEquals("012", PostfixParser.getInstance().resolve("abc cc:a->0"));
        assertEquals("101112", PostfixParser.getInstance().resolve("abc cc:a->10"));
    }
    
    @Test
    public void testKeep()
    {        
        assertEquals("24", PostfixParser.getInstance().resolve("a2,bc4 0..9 keep"));
        assertEquals("abc", PostfixParser.getInstance().resolve("a2bc,4 letters keep"));
    }
    
    @Test
    public void testSubstring()
    {        
        assertEquals("el", PostfixParser.getInstance().resolve("Hello 1 3 substring"));
    }
    
    @Test
    public void testContains()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("\"Hi I'm Antoon\" toon contains"));
    }
    
    @Test
    public void testEquality()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("\" Antoon\" Antoon ="));
        assertEquals("true", PostfixParser.getInstance().resolve("\"Antoon\" Antoon ="));
    }
    
    @Test
    public void testInequality()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("\" Antoon\" Antoon !="));
        assertEquals("false", PostfixParser.getInstance().resolve("\"Antoon\" Antoon !="));
    }
    
    @Test
    public void testBefore()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello ello <"));
    }
    
    @Test
    public void testBeforeOrEquals()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello ello <="));
    }
    
    @Test
    public void testAfter()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello ello >"));
    }
    
    @Test
    public void testAfterOrEquals()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello ello >="));
    }
    
    @Test
    public void testShorter()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello ello shorter"));
    }
    
    @Test
    public void testShorterOrEqualLength()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello ello shortereq"));
    }
    
    @Test
    public void testLonger()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello ello longer"));
    }
    
    @Test
    public void testLongerOrEqualLength()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello ello longereq"));
    }
    
    @Test
    public void testEqualLength()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello ello eqlong"));
        assertEquals("true", PostfixParser.getInstance().resolve("eleven twelve eqlong"));
    }
    
    @Test
    public void testRegexMatch()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello he.* ~="));
        assertEquals("false", PostfixParser.getInstance().resolve("hallo he.* ~="));
    }
    
    @Test
    public void testStartsWith()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello hel startswith"));
    }
    
    @Test
    public void testNotStartsWith()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello hel notstartswith"));
    }
    
    @Test
    public void testEndsWith()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("hello llo endswith"));
    }
    
    @Test
    public void testNotEndsWith()
    {        
        assertEquals("false", PostfixParser.getInstance().resolve("hello llo notendswith"));
    }
    
    @Test
    public void testHttpStatus()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("http://www.google.com 200 httpstatus"));
    }
    
    @Test
    public void testDateTime()
    {        
        assertEquals("true", PostfixParser.getInstance().resolve("10/10/2012 dd/MM/yyyy isdatetime"));
        assertEquals("false", PostfixParser.getInstance().resolve("10/15/2012 dd/MM/yyyy isdatetime"));
    }
    
    @Test
    public void testCrc32()
    {
        assertEquals("2323174389", PostfixParser.getInstance().resolve("2016000869 crc32"));
        assertEquals("1667334449", PostfixParser.getInstance().resolve("2018003446 crc32"));
    }
    
    @Test
    public void testCrockFord32Decode()
    {
        assertEquals("102513264", PostfixParser.getInstance().resolve("031rekg crockford32decode"));
    }
    
    @Test
    public void testConcatAll()
    {
        assertEquals("10.10.10-11.20", PostfixParser.getInstance().resolve("10 . 10 . 10 - 11 . 20 concat"));
    }
    
    @Test
    public void textExtractInteger1()
    {
        assertEquals("10", PostfixParser.getInstance().resolve("hello 10 concat exint"));
    }
    
    @Test
    public void textExtractInteger2()
    {
        assertEquals("10", PostfixParser.getInstance().resolve("hello 10 bye concat exint"));
    }
    
    @Test
    public void textExtractInteger3()
    {
        assertEquals("1020", PostfixParser.getInstance().resolve("hello 10 20 concat exint"));
    }
    
    @Test
    public void textExtractInteger4()
    {
        assertEquals("", PostfixParser.getInstance().resolve("hello 10 friend 20 concat exint"));
    }
    
    @Test
    public void textExtractInteger5()
    {
        assertEquals("10", PostfixParser.getInstance().resolve("hello 0010 concat exint"));
    }
    
    @Test
    public void textExtractDecimal1()
    {
        assertEquals("10.44", PostfixParser.getInstance().resolve("hi 10.439 concat .{2} exdec"));
    }
    
    @Test
    public void textExtractDecimal2()
    {
        assertEquals("10", PostfixParser.getInstance().resolve("hi 10.439 concat .{0} exdec"));
    }
    
    @Test
    public void textExtractDecimal3()
    {
        assertEquals("-10.4", PostfixParser.getInstance().resolve("hi -10.439 buddy concat .{1} exdec"));
    }
    
    @Test
    public void textExtractDecimal4()
    {
        assertEquals("-10,4", PostfixParser.getInstance().resolve("hi -10,439 buddy concat ,{1} exdec"));
    }
    
    @Test
    public void textExtractDecimal5()
    {
        assertEquals("-10.4", PostfixParser.getInstance().resolve("hi -10,439 buddy concat .{1} exdec"));
    }
    
    @Test
    public void textExtractDecimal6()
    {
        assertEquals("-10.00", PostfixParser.getInstance().resolve("hi -10buddy concat .{2} exdec"));
    }
    
    @Test
    public void textExtractDecimal7()
    {
        assertEquals("0.214", PostfixParser.getInstance().resolve("hi .2139 buddy concat .{3} exdec"));
    }
    
    @Test
    public void textExtractDecimal8()
    {
        assertEquals("12", PostfixParser.getInstance().resolve("12.0 ounces concat .{0} exdec"));
    }
    
    @Test
    public void textExtractDecimal9()
    {
        assertEquals("10.5", PostfixParser.getInstance().resolve("hi 10,439 buddy concat .{1}c exdec"));
    }
    
    @Test
    public void textExtractDecimal10()
    {
        assertEquals("-10.5", PostfixParser.getInstance().resolve("hi -10,439 buddy concat .{1}f exdec"));
    }
    
    @Test
    public void textExtractDecimal11()
    {
        assertEquals("10439.5", PostfixParser.getInstance().resolve("10,439.46 .{1}r exdec"));
    }
    
    @Test
    public void textExtractDecimal12()
    {
        assertEquals("10439.0", PostfixParser.getInstance().resolve("10439 .{1}r exdec"));
    }
    
    @Test
    public void textExtractDecimal13()
    {
        assertEquals("10439", PostfixParser.getInstance().resolve("10439 .{3}r exdectrim"));
    }
    
    @Test
    public void textExtractDecimalTrim1()
    {
        assertEquals("-10.52", PostfixParser.getInstance().resolve("hi -10,520 buddy concat .{3}f exdectrim"));
    }
    
    @Test
    public void textExtractDecimalTrim2()
    {
        assertEquals("-10.521", PostfixParser.getInstance().resolve("hi -10,521 buddy concat .{3}f exdectrim"));
    }
    
    @Test
    public void textExtractDecimalTrim3()
    {
        assertEquals("10.5", PostfixParser.getInstance().resolve("hi 10,50003 buddy concat .{3}f exdectrim"));
    }
    
    @Test
    public void testLeftPadding()
    {
        assertEquals("09", PostfixParser.getInstance().resolve("9 0 2 lpad"));
    }
    
    @Test
    public void testLeftPadding2()
    {
        assertEquals("15", PostfixParser.getInstance().resolve("15 0 2 lpad"));
    }
    
    @Test
    public void testRightPadding()
    {
        assertEquals("50000", PostfixParser.getInstance().resolve("5 00 4 rpad"));
    }
    
    @Test
    public void testSplit1()
    {
        assertEquals("7896 5329", PostfixParser
            .getInstance()
            .resolve("78965329 4,4 \" \" split"));
    }
    
    @Test
    public void testSplit2()
    {
        assertEquals("78-9653-29", PostfixParser
            .getInstance()
            .resolve("78965329 2,4,2 - split"));
    }
    
    @Test
    public void testSplit3()
    {
        assertEquals("7896-5329-", PostfixParser
            .getInstance()
            .resolve("78965329 4,4,0 - split"));
    }
    
    @Test(expected = GrexParseException.class)
    public void testSplit7()
    {
        PostfixParser
            .getInstance()
            .resolve("78965329 4,5 - split");
    }

}
