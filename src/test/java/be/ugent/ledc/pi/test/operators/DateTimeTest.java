package be.ugent.ledc.pi.test.operators;

import be.ugent.ledc.pi.grex.PostfixParser;
import java.time.LocalDateTime;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class DateTimeTest
{
    @Test
    public void nowTest()
    {       
        String resolve = PostfixParser.getInstance().resolve("::datetime now");
        
        LocalDateTime.parse(resolve);
    }
    
    @Test
    public void yearTest()
    {       
        String year = PostfixParser
                .getInstance()
                .resolve("::datetime now ::int year");
        
        assertEquals(LocalDateTime.now().getYear(), Integer.parseInt(year));
    }
    
    @Test
    public void beforeTest1()
    {       
        String test = PostfixParser
                .getInstance()
                .resolve("::datetime now \"26/10/2000 10:00:00\" \"dd/MM/yyyy HH:mm:ss\" parse after");
        
        assertTrue(Boolean.parseBoolean(test));
    }
    
    @Test
    public void extractTest1()
    {       
        String year = PostfixParser
                .getInstance()
                .resolve("::int 2007-12-03T10:15:30 year");
        
        assertEquals("2007", year);
    }
    
    @Test
    public void extractTest2()
    {       
        String month = PostfixParser
                .getInstance()
                .resolve("::int 2007-12-03T10:15:30 month");
        
        assertEquals("12", month);
    }
    @Test
    public void extractTest3()
    {       
        String d = PostfixParser
                .getInstance()
                .resolve("::int 2007-12-03T10:15:30 dayofyear");
        
        assertEquals("337", d);
    }
    
    @Test
    public void extractTest4()
    {       
        String d = PostfixParser
                .getInstance()
                .resolve("2007-12-03T10:15:30 dayofweek");
        
        assertEquals("MONDAY", d);
    }
    
    @Test
    public void extractTest5()
    {       
        String d = PostfixParser
                .getInstance()
                .resolve("2007-12-03T10:15:30 month");
        
        assertEquals("DECEMBER", d);
    }
}
