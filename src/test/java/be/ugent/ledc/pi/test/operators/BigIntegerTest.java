package be.ugent.ledc.pi.test.operators;

import be.ugent.ledc.pi.grex.PostfixParser;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class BigIntegerTest
{
    @Test
    public void bigIntegerModuloTest()
    {
        assertEquals("0", PostfixParser
            .getInstance()
            .resolve("::bigint 20041010050500013402606 97 %"));
    }
    
    @Test
    public void bigIntegerModuloEqualityTest()
    {
        assertTrue(Boolean.parseBoolean(PostfixParser
            .getInstance()
            .resolve("::bigint 20041010050500013402606 97 % 0 =")));
    }
}
