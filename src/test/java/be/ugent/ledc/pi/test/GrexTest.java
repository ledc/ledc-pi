package be.ugent.ledc.pi.test;

import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.grex.InterpretationException;
import be.ugent.ledc.pi.measure.predicates.GrexPredicate;
import org.junit.Assert;
import org.junit.Test;

public class GrexTest
{
    @Test
    public void testIssnCheckSumGrex()
    {
        GrexPredicate p = new GrexPredicate(
            new Grex("::int 11 @1@2 8<-n w+ 11 % - @3 ="),
            "(\\d{4})\\-?(\\d{3})(\\d|X)");

        Assert.assertTrue(p.test("1063-6706"));
        Assert.assertTrue(p.test("0167-9236"));
    }
    
    @Test
    public void grexConstantTest() throws InterpretationException
    {
        Grex g = new Grex("\"Hello friend\"");

        String result = g.resolveAsString( "^.*$", "Does not matter...");
        
        Assert.assertEquals("Hello friend", result);
    }
    
    @Test
    public void grexTwoConstantsTest() throws InterpretationException
    {
        Grex g = new Grex("@0");

        String result = g.resolveAsString( "^.*$", "Hello friend");
        
        Assert.assertEquals("Hello friend", result);
    }
    
    @Test
    public void grexQuoteTest1() throws InterpretationException
    {
        Grex g = new Grex("@2");

        String result = g.resolveAsString(
            "<ArticleId IdType=\"(.+)\">(.+)</ArticleId>",
            "<ArticleId IdType=\"pii\">M.D L\"</ArticleId>"
        );

        Assert.assertEquals("M.D L\"", result);
    }
    
    @Test
    public void grexQuoteTest2() throws InterpretationException
    {
        Grex g = new Grex("\"@2\"");

        String result = g.resolveAsString(
            "<ArticleId IdType=\"(.+)\">(.+)</ArticleId>",
            "<ArticleId IdType=\"pii\">M.D L\"</ArticleId>"
        );

        Assert.assertEquals("@2", result);
    }
    
    @Test
    public void grexQuoteTest3() throws InterpretationException
    {
        Grex g = new Grex("\"\"\"");

        String result = g.resolveAsString(
            ".+",
            "Not much to say..."
        );

        Assert.assertEquals("\"", result);
    }
    
    @Test
    public void booleanAndTest1() throws InterpretationException
    {
        Grex g = new Grex("::int @1 @2 < @3 @4 >= ::bool and");

        Assert.assertTrue(g.resolveAsBoolean("(\\d)(\\d)(\\d)(\\d)","1295"));
    }
    
    @Test
    public void booleanAndTest2() throws InterpretationException
    {
        Grex g = new Grex("::int @1 @2 < @4 @3 >= ::bool and");

        Assert.assertFalse(g.resolveAsBoolean("(\\d)(\\d)(\\d)(\\d)","1295"));
    }
    
    @Test
    public void booleanAndNotTest() throws InterpretationException
    {
        Grex g = new Grex("@1 @2 < @4 @3 >= ::bool and not");

        Assert.assertTrue(g.resolveAsBoolean("(\\d)(\\d)(\\d)(\\d)","1295"));
    }
    
    @Test
    public void booleanOrTest() throws InterpretationException
    {
        Grex g = new Grex("::int @1 @2 < @4 @3 >= ::bool or");

        Assert.assertTrue(g.resolveAsBoolean("(\\d)(\\d)(\\d)(\\d)","1295"));
    }
    
    @Test
    public void mkVarTest() throws InterpretationException
    {
        Grex g = new Grex("::int @1 @2 + mkvar @3 10 >= @3 39 <= ::bool and");

        Assert.assertTrue(g.resolveAsBoolean("(\\d+)\\s?\\+\\s?(\\d+)","13 + 26"));
    }
}
