package be.ugent.ledc.pi.test;

import be.ugent.ledc.core.datastructures.DependencyGraphException;
import be.ugent.ledc.pi.PiException;
import be.ugent.ledc.pi.conversion.Convertor;
import be.ugent.ledc.pi.conversion.grex.GrexStringConvertor;
import be.ugent.ledc.pi.grex.Grex;
import be.ugent.ledc.pi.measure.Measure;
import be.ugent.ledc.pi.measure.predicates.ConjunctivePredicate;
import be.ugent.ledc.pi.measure.predicates.DisjunctivePredicate;
import be.ugent.ledc.pi.measure.predicates.GrexPredicate;
import be.ugent.ledc.pi.measure.predicates.NotNullPredicate;
import be.ugent.ledc.pi.measure.predicates.PatternPredicate;
import be.ugent.ledc.pi.measure.predicates.Predicate;
import be.ugent.ledc.pi.property.Property;
import be.ugent.ledc.pi.property.PropertyParseException;
import be.ugent.ledc.pi.registry.Registry;
import be.ugent.ledc.pi.scanner.PropertyScanException;
import be.ugent.ledc.pi.scanner.execution.ScannerExecutor;
import be.ugent.ledc.pi.scanner.instances.MeasureScanner;
import be.ugent.ledc.pi.scanner.result.ResultBuffer;
import be.ugent.ledc.pi.validation.ValidationScheme;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class IssnTest
{
    public IssnTest(){}
    
    @BeforeClass
    public static void setUpClass() throws PropertyParseException, URISyntaxException, PiException
    {
        List<Predicate<String>> predicates = new ArrayList<>();
        
        String p = "(\\d{4})\\-?(\\d{3})(\\d|xX)";
        
        predicates.add(new NotNullPredicate<>());
        predicates.add(new PatternPredicate(p));
        predicates.add(new DisjunctivePredicate<>(
            new GrexPredicate(new Grex("::int 11 @1@2 8<-n w+ 11 % - @3 ="), p),
            new ConjunctivePredicate(
                new GrexPredicate(new Grex("::int 11 @1@2 8<-n w+ 11 % - 10 ="), p),
                new GrexPredicate(new Grex("@3 uppercase X ="), p)
            ),
            new ConjunctivePredicate(
                new GrexPredicate(new Grex("::int @1@2 8<-n w+ 11 % 0 ="), p),
                new GrexPredicate(new Grex("@3 0 ="), p)
            )
        ));
        predicates.add(new PatternPredicate("(\\d{4})\\-(\\d{3})(\\d|X)"));
        
        Property issn = Property.parseProperty("ledc.ugent.be/identifiers#issn2:*");

        //Register a quality masure for ISSN numbers
        Measure<String> measure = new Measure<>(predicates);
        
        //Register a standardizer for ISSN numbers
        Convertor<String,String> convertor = new GrexStringConvertor(new Grex("@1 - @2 @3 concat uppercase"), p); 
        
        Registry
            .getInstance()
            .register(
                new ValidationScheme(
                    issn,
                    measure,
                    convertor,
                    new URI("https://nl.wikipedia.org/wiki/International_Standard_Serial_Number"),
                    3));
        
        Registry.getInstance().register(new MeasureScanner(issn, p));
    }   
    
    @Test
    public void nullTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure(null);
        
        Assert.assertEquals(0, measurement);
    }
    
    @Test
    public void notNullTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure("1063-6705dqfq");
        
        Assert.assertEquals(1, measurement);
    }
    
    @Test
    public void patternTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure("1063-6705");
        
        Assert.assertEquals(2, measurement);
    }
    
    @Test
    public void checkDigitTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure("10636706");
        
        Assert.assertEquals(3, measurement);
    }
    
    @Test
    public void checkDigitZeroConditionTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure("13260200");
        
        Assert.assertEquals(3, measurement);
    }
    
    @Test
    public void perfectionTest() throws PropertyParseException
    {
        int measurement = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getMeasure()
            .measure("1063-6706");
        
        Assert.assertEquals(4, measurement);
    }
    
    @Test
    public void validityTest() throws PropertyParseException
    {
        boolean isValid = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .validate("10636706");
        
        Assert.assertEquals(true, isValid);
    }
    
    @Test
    public void conversionTest() throws PropertyParseException
    {
        String converted = Registry
            .getInstance()
            .getValidationScheme(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"))
            .getConvertor()
            .convert("10636706");
        
        Assert.assertEquals("1063-6706", converted);
    }
    
    @Test
    public void scanTest() throws PropertyParseException, DependencyGraphException, PropertyScanException
    {
        String text = "The article entitled 'A measure-theoretic foundation for data quality' has been assigned the ISSN number 1063-6706."
                + "The number 1063-6705 is clearly wrong!";
        
        //Build an executor to search for the desired properties.
        ScannerExecutor executor = new ScannerExecutor(Property.parseProperty("ledc.ugent.be/identifiers#issn2:*"));

        //Execute the scan and store results.
        ResultBuffer buffer =  executor.execute(text);
   
        Assert.assertEquals(1, buffer.getScanResults().size());
        Assert.assertEquals("1063-6706", buffer.getScanResults().get(0).getInstance());
    }
}
